﻿using HealingPebblesMobile.View.Pages;
using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace HealingPebblesMobile
{
    public partial class App : Application
    {
        public static int ScreenWidth;
        public static int ScreenHeight;
        public static int RealScreenWidth;
        public static int RealScreenHeight;

        public App()
        {
            InitializeComponent();

            MainPage = new NavigationPage(new LoginPage());
        }

        protected override void OnStart()
        {
        }

        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
        }
    }
}
