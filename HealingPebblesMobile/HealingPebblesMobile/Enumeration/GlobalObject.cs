﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HealingPebblesMobile.Enumeration
{
    public static class GlobalObject
    {
        //ServerState icon
        public static readonly string BadConnectIcon = "ConnectBadState.png";
        public static readonly string GoodConnectIcon = "ConnectGootState.png";
        //BluetoothState icon
        public static readonly string FireFlyNotFoundIcon = "NoFireFly.png";
        public static readonly string FireFlyFullCharageIcon = "FullFireFly.png";
        public static readonly string FireFlyNotFullCharageIcon = "NotFullFireFly.png";
        public static readonly string FireFlyMiddleCharageIcon = "MiddleFireFly.png";
        public static readonly string FireFlyLowCharageIcon = "LowFireFly.png";

        //Цветовая индикация
        public static readonly string GoodStateHEX = "#4AAB3F";
        public static readonly string So_SoStateHEX = "#F0BA00";
        public static readonly string BadStateHEX = "#FF6552";
    }
}
