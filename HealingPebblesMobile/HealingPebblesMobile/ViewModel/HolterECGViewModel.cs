﻿using HealingPebblesMobile.Model.ConclusionPart;
using HealingPebblesMobile.Classes;
using HealingPebblesMobile.Model.BluetoothModel.CustomEventArgsClasses;
using HealingPebblesMobile.Model.BluetoothPart;
using HealingPebblesMobile.Model.BluetoothPart.CustomEventArgsClasses;
using HealingPebblesMobile.Model.BluetoothPart.DataConvert;
using HealingPebblesMobile.Model.ServerPart;
using SkiaSharp;
using SkiaSharp.Views.Forms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;
using HealingPebblesMobile.Enumeration;
using HealingPebblesMobile.Model.ConclusionPart.ConclusionEventArgs;
using HealingPebblesMobile.Model.ConclusionPart.HeartRateVariability;
using HealingPebblesMobile.Model.ConclusionPart.Diagnosis;

namespace HealingPebblesMobile.ViewModel
{
    public class HolterECGViewModel : INotifyPropertyChanged, IDisposable
    {
        //ServerState icon
        private readonly string BadConnectIcon = "ConnectBadState.png";
        private readonly string GoodConnectIcon = "ConnectGootState.png";

        //Цветовая индикация
        static readonly string GoodStateHEX = "#4AAB3F";
        static readonly string So_SoStateHEX = "#F0BA00";
        static readonly string BadStateHEX = "#FF6552";

        //ConclusionModel ConclusionModel { get; set; } = null;

        FireFly FireFly;
        ServerWorker ServerWorker;

        //Флаги контроля
        private bool IsProcessValueInit { get; set; } = false;
        private bool IsInfoCame { get; set; } = false;
        public bool IsLoadingAborted { get; set; } = false;
        private SKCanvasView ECGCanvas { get; set; } = null;

        SkiaPaintController SkiaPaintController;

        public event EventHandler<SignalReadyEventArgs> PackageCameEvent;
        protected virtual void OnPackageCame(SignalReadyEventArgs e)
        {
            EventHandler<SignalReadyEventArgs> handler = PackageCameEvent;
            if (handler != null)
            {
                handler(this, e);
            }
        }


        SignalHandler SignalHandler;
        ConclusionModel ConclusionModel;

        public HolterECGViewModel()
        {
            SignalHandler = new SignalHandler(1);
            FireFly = FireFly.getInstance();
            //FireFly.BluetoothConnectController.ChangeBluetoothStateEvent += BluetoothStateChangeConnect;
            this.ServerWorker = ServerWorker.getInstance();
            this.FireFly.PackageCameEvent += NewPartReady;
            this.ConclusionModel = new ConclusionModel(this, 500, new DateTime(1994, 7, 29, 0, 0, 0));
            this.ConclusionModel.Weight = 78;
            this.ConclusionModel.Growth = 168;
            this.ConclusionModel.PulseAnalyzer.DiagnosisUpdate += this.PulseUpdate;
            this.ConclusionModel.RithmConclusionReadyEvent += this.RithmStyleChange;
            this.ConclusionModel.BodyMassIndexChange += this.BodyMassIndexChange;
            this.ConclusionModel.SystemTensionIndexController.TensionIndexChangeEvent += TensionIndexChange;
            this.ConclusionModel.AdaptationLevel.AdaptationLevelChangeEvent += AdaptationLevelChange;
            this.ConclusionModel.VegetativeBalanceController.VegetativeBalanceIndexChangeEvent += VegetativeBalanceIndexChange;
            this.ConclusionModel.ExtrasystoleConclusionReadyEvent += ExtrasistoleReaction;
            this.ConclusionModel.PauseConclusionReadyEvent += PauseReaction;
        }

        

        void NewPartReady(object sender, FireFlyPackageEventArgs e)
        {
            //Отправка данных на сервер
            //string res = (this.ServerWorker.SendPackage(this.ServerWorker.CovertData(e.Package.GetSignal()))).GetAwaiter().GetResult();
            //Отправка данных на отрисовку
            List<short[]> list = new List<short[]>();
            list.Add(e.Package.GetSignal());
            List<short[]> a = this.SignalHandler.GetProcessedSignal(list);
            SignalReadyEventArgs signalReadyEventArgs = new SignalReadyEventArgs() { Data = a[0] };
            this.OnPackageCame(signalReadyEventArgs);
        }




        public void SetCanvas(SKCanvasView ecgCanvas)
        {
            ECGCanvas = ecgCanvas;
            this.SkiaPaintController = new SkiaPaintController(ECGCanvas, this, "", 500);
        }

        private void PauseReaction(object sender, PauseEventArgs e)
        {
            this.PauseDiscription = e.PauseDiscription;
            switch (e.ProblemRank)
            {
                case 0:
                    this.PauseRank = 5;
                    this.SmallPauseDiscription = "Нет отклонений";
                    this.ColorPause = Color.FromHex(GoodStateHEX);
                    break;
                case 1:
                    this.PauseRank = 5;
                    this.SmallPauseDiscription = "Небольшие отклонения";
                    this.ColorPause = Color.FromHex(GoodStateHEX);
                    break;
                case 2:
                    this.ColorPause = Color.FromHex(So_SoStateHEX);
                    this.SmallPauseDiscription = "Небольшие проблемы";
                    this.PauseRank = 4;
                    this.PauseDiscription += Environment.NewLine;
                    this.PauseDiscription += Environment.NewLine;
                    this.PauseDiscription += "Двухсекундных пауз: " + e.CountFromTwoToThreeSecondsPause.ToString();
                    this.PauseDiscription += Environment.NewLine;
                    this.PauseDiscription += "Трёхсекундных пауз: " + e.CountMoreThreeSecondsPause.ToString();
                    break;
                default:
                    this.PauseRank = 3;
                    this.ColorPause = Color.FromHex(BadStateHEX);
                    this.SmallPauseDiscription = "Опасность";
                    this.PauseDiscription += Environment.NewLine;
                    this.PauseDiscription += Environment.NewLine;
                    this.PauseDiscription += "Двухсекундных пауз: " + e.CountFromTwoToThreeSecondsPause.ToString();
                    this.PauseDiscription += Environment.NewLine;
                    this.PauseDiscription += "Трёхсекундных пауз: " + e.CountMoreThreeSecondsPause.ToString();
                    break;
            }
        }


        //Регион, отвечающий за изменение GUI связанного с ВСР
        #region

        private void TensionIndexChange(object sender, IndexChangeOptionsEventArgs e)
        {
            if (!isTensionIndexInit)
            {
                this.TensionIndexMin = ((SystemTensionIndexController)(sender)).LeftBorderControl;
                this.TensionIndexMax = ((SystemTensionIndexController)(sender)).RightBorderControl;
                isTensionIndexInit = true;
            }
            this.TensionIndex = e.ValuePercent;
            switch (e.HeartRateVariabilityIndex)
            {
                case HeartRateVariabilityIndex.Good:
                    this.TensionIndexDiscription = "На данный момент вы хорошо справляетесь с физическими и психологическими нагрузками";
                    break;
                case HeartRateVariabilityIndex.NotVeryGood:
                    this.TensionIndexDiscription = "Компенсированный дистресс. Вы успешно справляетесь с нагрузками, способены к ним адаптироваться, однако ценой больших энергозатрат";
                    break;
                case HeartRateVariabilityIndex.Warning:
                    this.TensionIndexDiscription = "Некомпенсированный дистресс. Организм с трудом адаптируется к физическим и эмоциональным нагрузкам. Рекомендуется отдых и корректировака нагрузки";
                    break;
                case HeartRateVariabilityIndex.Danger:
                    this.TensionIndexDiscription = "Состояние коротковременного функционального расстройства. Стресс-реализующие системы организма оказывают повреждающий эффкект на человека";
                    break;
                case HeartRateVariabilityIndex.Critical:
                    this.TensionIndexDiscription = "Критическое состояние. Организм истощён. Сердце работает на износ со значительным напряжением. Велика вероятность развития инфаркта";
                    break;
                default:
                    this.TensionIndexDiscription = "";
                    break;
            }
        }
        private void AdaptationLevelChange(object sender, IndexChangeOptionsEventArgs e)
        {
            if (!isAdaptationLevelInit)
            {
                this.AdaptationLevelIndexMin = ((AdaptationLevel)(sender)).LeftBorderControl;
                this.AdaptationLevelIndexMax = ((AdaptationLevel)(sender)).RightBorderControl;
                isAdaptationLevelInit = true;
            }
            this.AdaptationLevelIndex = e.ValuePercent;
            switch (e.HeartRateVariabilityIndex)
            {
                case HeartRateVariabilityIndex.Good:
                    this.AdaptationLevelIndexDiscription = "Норма. Организм достаточно мобилизован и находится в тонусе";
                    break;
                case HeartRateVariabilityIndex.NotVeryGood:
                    this.AdaptationLevelIndexDiscription = "Избыток жизненных сил. Организм избыточно расслаблен и готов к плавному повышению нагрузки";
                    break;
                case HeartRateVariabilityIndex.Warning:
                    this.AdaptationLevelIndexDiscription = "Сложная адаптация. Наблюдается высокая цена адаптации, связанная с течением основного заболевания или напряжённого периода жизни";
                    break;
                case HeartRateVariabilityIndex.Danger:
                    this.AdaptationLevelIndexDiscription = "Застой жизненной энергии. Дизрегуляционная патология возникшая на фоне инертности систем мобилизации энергетики";
                    break;
                case HeartRateVariabilityIndex.Critical:
                    this.AdaptationLevelIndexDiscription = "Критическое низкий уровень адаптации. Крайне высокая цена адаптации на фоне истощения энергетики, указывает на возможность кризиса";
                    break;
                default:
                    this.AdaptationLevelIndexDiscription = "";
                    break;
            }
        }
        private void VegetativeBalanceIndexChange(object sender, IndexChangeOptionsEventArgs e)
        {
            if (!isVegetativeBalanceInit)
            {
                this.VegetativeBalanceIndexMin = ((VegetativeBalanceController)(sender)).LeftBorderControl;
                this.VegetativeBalanceIndexMax = ((VegetativeBalanceController)(sender)).RightBorderControl;
                isVegetativeBalanceInit = true;
            }
            this.VegetativeBalanceIndex = e.ValuePercent;
            switch (e.HeartRateVariabilityIndex)
            {
                case HeartRateVariabilityIndex.Good:
                    this.VegetativeBalanceIndexDiscription = "Состояние нормы. Вегетативный баланс находится в равновесии: симпатическая и парасимпатическая системы работают исправно в комплексе";
                    break;
                case HeartRateVariabilityIndex.NotVeryGood:
                    this.VegetativeBalanceIndexDiscription = "Расслабление. Наблюдается незначительное преобладание парасимпатического отдела - расслабление организма";
                    break;
                case HeartRateVariabilityIndex.Warning:
                    this.VegetativeBalanceIndexDiscription = "Состояние напряжения. Домината симпатического отдела нервной системы. Рекомендуется отдых";
                    break;
                case HeartRateVariabilityIndex.Danger:
                    this.VegetativeBalanceIndexDiscription = "Состояние инертности. Вы слишком расслаблены. Наблюдается выраженная инертность и пассивность, явная доминанта парасимпатического отдела";
                    break;
                case HeartRateVariabilityIndex.Critical:
                    this.VegetativeBalanceIndexDiscription = "Состояние дисбаланса. Наблюдается крайняя степень напряжения симпатического отела на фоне истощения энергии";
                    break;
                default:
                    this.VegetativeBalanceIndexDiscription = "";
                    break;
            }
        }
        

        private bool isTensionIndexInit = false;
        private bool isAdaptationLevelInit = false;
        private bool isVegetativeBalanceInit = false;

        ushort tensionIndex = 0;
        public ushort TensionIndex
        {
            get { return tensionIndex; }
            set
            {
                if (tensionIndex != value)
                {
                    tensionIndex = value;
                    OnPropertyChanged("TensionIndex");
                }
            }
        }
        ushort tensionIndexMax = 100;
        public ushort TensionIndexMax
        {
            get { return tensionIndexMax; }
            set
            {
                if (tensionIndexMax != value)
                {
                    tensionIndexMax = value;
                    OnPropertyChanged("TensionIndexMax");
                }
            }
        }
        ushort tensionIndexMin = 0;
        public ushort TensionIndexMin
        {
            get { return tensionIndexMin; }
            set
            {
                if (tensionIndexMin != value)
                {
                    tensionIndexMin = value;
                    OnPropertyChanged("TensionIndexMin");
                }
            }
        }
        string tensionIndexDiscription = "Процесс расчёта уровня стресса...";
        public string TensionIndexDiscription
        {
            get { return tensionIndexDiscription; }
            set
            {
                if (tensionIndexDiscription != value)
                {
                    tensionIndexDiscription = value;
                    OnPropertyChanged("TensionIndexDiscription");
                }
            }
        }


        ushort adaptationLevelIndex = 0;
        public ushort AdaptationLevelIndex
        {
            get { return adaptationLevelIndex; }
            set
            {
                if (adaptationLevelIndex != value)
                {
                    adaptationLevelIndex = value;
                    OnPropertyChanged("AdaptationLevelIndex");
                }
            }
        }
        ushort adaptationLevelIndexMax = 100;
        public ushort AdaptationLevelIndexMax
        {
            get { return adaptationLevelIndexMax; }
            set
            {
                if (adaptationLevelIndexMax != value)
                {
                    adaptationLevelIndexMax = value;
                    OnPropertyChanged("AdaptationLevelIndexMax");
                }
            }
        }
        ushort adaptationLevelIndexMin = 0;
        public ushort AdaptationLevelIndexMin
        {
            get { return adaptationLevelIndexMin; }
            set
            {
                if (adaptationLevelIndexMin != value)
                {
                    adaptationLevelIndexMin = value;
                    OnPropertyChanged("AdaptationLevelIndexMin");
                }
            }
        }
        string adaptationLevelDiscription = "Процесс расчёта уровня адаптации...";
        public string AdaptationLevelIndexDiscription
        {
            get { return adaptationLevelDiscription; }
            set
            {
                if (adaptationLevelDiscription != value)
                {
                    adaptationLevelDiscription = value;
                    OnPropertyChanged("AdaptationLevelIndexDiscription");
                }
            }
        }
        ushort vegetativeBalanceIndex = 0;
        public ushort VegetativeBalanceIndex
        {
            get { return vegetativeBalanceIndex; }
            set
            {
                if (vegetativeBalanceIndex != value)
                {
                    vegetativeBalanceIndex = value;
                    OnPropertyChanged("VegetativeBalanceIndex");
                }
            }
        }
        ushort vegetativeBalanceIndexMax = 100;
        public ushort VegetativeBalanceIndexMax
        {
            get { return vegetativeBalanceIndexMax; }
            set
            {
                if (vegetativeBalanceIndexMax != value)
                {
                    vegetativeBalanceIndexMax = value;
                    OnPropertyChanged("VegetativeBalanceIndexMax");
                }
            }
        }
        ushort vegetativeBalanceIndexMin = 0;
        public ushort VegetativeBalanceIndexMin
        {
            get { return vegetativeBalanceIndexMin; }
            set
            {
                if (vegetativeBalanceIndexMin != value)
                {
                    vegetativeBalanceIndexMin = value;
                    OnPropertyChanged("VegetativeBalanceIndexMin");
                }
            }
        }
        string vegetativeBalanceIndexDiscription = "Процесс расчёта индекса равновесия нервной системы...";
        public string VegetativeBalanceIndexDiscription
        {
            get { return vegetativeBalanceIndexDiscription; }
            set
            {
                if (vegetativeBalanceIndexDiscription != value)
                {
                    vegetativeBalanceIndexDiscription = value;
                    OnPropertyChanged("VegetativeBalanceIndexDiscription");
                }
            }
        }



        string fireFlyPackageNumber = "--";
        public string FireFlyPackageNumber
        {
            get { return fireFlyPackageNumber; }
            set
            {
                if (fireFlyPackageNumber != value)
                {
                    fireFlyPackageNumber = value;
                    OnPropertyChanged("FireFlyPackageNumber");
                }
            }
        }
        string fireFlyMaxPackageNumber = "--";
        public string FireFlyMaxPackageNumber
        {
            get { return fireFlyMaxPackageNumber; }
            set
            {
                if (fireFlyMaxPackageNumber != value)
                {
                    fireFlyMaxPackageNumber = value;
                    OnPropertyChanged("FireFlyMaxPackageNumber");
                }
            }
        }
        //Либо Bl либо sd
        string fireFlyReceiveState = "--";
        public string FireFlyReceiveState
        {
            get { return fireFlyReceiveState; }
            set
            {
                if (fireFlyReceiveState != value)
                {
                    fireFlyReceiveState = value;
                    OnPropertyChanged("FireFlyReceiveState");
                }
            }
        }
        //+ создан, - нет
        string serverFileState = "-";
        public string ServerFileState
        {
            get { return serverFileState; }
            set
            {
                if (serverFileState != value)
                {
                    serverFileState = value;
                    OnPropertyChanged("ServerFileState");
                }
            }
        }
        string currentServerPart = "--";
        public string CurrentServerPart
        {
            get { return currentServerPart; }
            set
            {
                if (currentServerPart != value)
                {
                    currentServerPart = value;
                    OnPropertyChanged("CurrentServerPart");
                }
            }
        }
        string serverFileClose = "no";
        public string ServerFileClose
        {
            get { return serverFileClose; }
            set
            {
                if (serverFileClose != value)
                {
                    serverFileClose = value;
                    OnPropertyChanged("ServerFileClose");
                }
            }
        }

        string signalListCount = "--";
        public string SignalListCount
        {
            get { return signalListCount; }
            set
            {
                if (signalListCount != value)
                {
                    signalListCount = value;
                    OnPropertyChanged("SignalListCount");
                }
            }
        }


        string bluetoothDeviceStateIcon = "NoFireFly.png";
        public string BluetoothDeviceStateIcon
        {
            get { return bluetoothDeviceStateIcon; }
            set
            {
                if (bluetoothDeviceStateIcon != value)
                {
                    bluetoothDeviceStateIcon = value;
                    OnPropertyChanged("BluetoothDeviceStateIcon");
                }
            }
        }

        string bluetoothDeviceState = "--";
        public string BluetoothDeviceState
        {
            get { return bluetoothDeviceState + "%"; }
            set
            {
                if (bluetoothDeviceState != value)
                {
                    bluetoothDeviceState = value;
                    OnPropertyChanged("BluetoothDeviceState");
                }
            }
        }

        void ConnectionStateChange(object sender, EventArgs e)
        {
            bool isConnected = (sender as ServerWorker).IsInternetConnection;
            if (isConnected)
            {
                this.ServerStateIcon = GoodConnectIcon;
            }
            else
                this.ServerStateIcon = BadConnectIcon;
        }

        string serverStateIcon = "ConnectBadState.png";
        public string ServerStateIcon
        {
            get { return serverStateIcon; }
            set
            {
                if (serverStateIcon != value)
                {
                    serverStateIcon = value;
                    OnPropertyChanged("ServerStateIcon");
                }
            }
        }

        //Пульс
        #region
        
        void PulseUpdate(object sender, DiagnosisDiscription e)
        {
            if (e != null)
            {
                this.PulseTitle = e.DiagnosisTitle;
                this.PulseSmallDisc = e.SmallDiagnosisDiscription;
                this.ColorPulse = CalculateColor(e.DiagnosisRank);
                this.PulseDiscription = e.DiagnosDiscription + Environment.NewLine + e.SpecificDiagnosisDiscription + Environment.NewLine
                    + "Рекомендации: " + e.DiagnosisRecommendation;
            }
            else
            {
                this.PulseTitle = "--";
                this.PulseSmallDisc = "";
                this.ColorPulse = CalculateColor(DiagnosisRank.Normal);
                this.PulseDiscription = "Возникли проблемы при определении пульса. " + Environment.NewLine
                    + "Рекомендации: отрегулируйте положение мобильного кардиографа и проверьте контакт электродов с телом или устройством. ";
            }

        }
        

        private string pulseTitle = "--";
        public string PulseTitle
        {
            get { return pulseTitle; }
            set
            {
                if (pulseTitle != value)
                {
                    pulseTitle = value;
                    OnPropertyChanged("PulseTitle");
                }
            }
        }
        private string pulseSmallDisc = "Расчёт...";
        public string PulseSmallDisc
        {
            get { return pulseSmallDisc; }
            set
            {
                if (pulseSmallDisc != value)
                {
                    pulseSmallDisc = value;
                    OnPropertyChanged("PulseSmallDisc");
                }
            }
        }

        private string pulseDiscription = "Расчёт...";
        public string PulseDiscription
        {
            get { return pulseDiscription; }
            set
            {
                if (pulseDiscription != value)
                {
                    pulseDiscription = value;
                    OnPropertyChanged("PulseDiscription");
                }
            }
        }


        private Color colorPulse = Color.FromHex(GoodStateHEX);
        public Color ColorPulse
        {
            get { return colorPulse; }
            set
            {
                if (colorPulse != value)
                {
                    colorPulse = value;
                    OnPropertyChanged("ColorPulse");
                }
            }
        }

        private byte PulseRank = 5;

        
        private Color CalculateColor(DiagnosisRank diagnosisRank)
        {
            Color resultColor = Color.FromHex(GoodStateHEX); ;
            switch (diagnosisRank)
            {
                case DiagnosisRank.VeryGood:
                    resultColor = Color.FromHex(GoodStateHEX);
                    PulseRank = 5;
                    break;
                case DiagnosisRank.Good:
                    resultColor = Color.FromHex(GoodStateHEX);
                    PulseRank = 5;
                    break;
                case DiagnosisRank.Normal:
                    resultColor = Color.FromHex(So_SoStateHEX);
                    PulseRank = 5;
                    break;
                case DiagnosisRank.Warning:
                    resultColor = Color.FromHex(So_SoStateHEX);
                    PulseRank = 4;
                    break;
                case DiagnosisRank.Danger:
                    resultColor = Color.FromHex(BadStateHEX);
                    PulseRank = 3;
                    break;
                default:
                    break;
            }
            return resultColor;
        }
        

        private int widhtPulseFrame = 100;
        public int WidhtPulseFrame
        {
            get { return widhtPulseFrame; }
            set
            {
                if (widhtPulseFrame != value)
                {
                    widhtPulseFrame = value;
                    OnPropertyChanged("WidhtPulseFrame");
                }
            }
        }
        private int heightPulseFrame = 100;
        public int HeightPulseFrame
        {
            get { return heightPulseFrame; }
            set
            {
                if (heightPulseFrame != value)
                {
                    heightPulseFrame = value;
                    OnPropertyChanged("HeightPulseFrame");
                }
            }
        }
        #endregion

        //Ритмы
        #region 
        private byte RithmRank = 5;
        private string rithmDiscription = "Нет отклонений";
        public string RithmDiscription
        {
            get { return rithmDiscription; }
            set
            {
                if (rithmDiscription != value)
                {
                    rithmDiscription = value;
                    OnPropertyChanged("RithmDiscription");
                }
            }
        }
        private string rithmTitleDiscription = "Нет отклонений";
        public string RithmTitleDiscription
        {
            get { return rithmTitleDiscription; }
            set
            {
                if (rithmTitleDiscription != value)
                {
                    rithmTitleDiscription = value;
                    OnPropertyChanged("RithmTitleDiscription");
                }
            }
        }
        private Color colorRithm = Color.FromHex(GoodStateHEX);
        public Color ColorRithm
        {
            get { return colorRithm; }
            set
            {
                if (colorRithm != value)
                {
                    colorRithm = value;
                    OnPropertyChanged("ColorRithm");
                }
            }
        }
        private int widhtRithmFrame = 100;
        public int WidhtRithmFrame
        {
            get { return widhtRithmFrame; }
            set
            {
                if (widhtRithmFrame != value)
                {
                    widhtRithmFrame = value;
                    OnPropertyChanged("WidhtRithmFrame");
                }
            }
        }
        private int heightRithmFrame = 100;
        public int HeightRithmFrame
        {
            get { return heightRithmFrame; }
            set
            {
                if (heightRithmFrame != value)
                {
                    heightRithmFrame = value;
                    OnPropertyChanged("HeightRithmFrame");
                }
            }
        }

        void RithmStyleChange(object sender, RithmEventArgs e)
        {
            this.RithmDiscription = e.RithmDiscription;
            this.RithmTitleDiscription = e.RithmSmallDiscription;
            if (e.ProblemRank == 0)
            {
                this.RithmRank = 4;
                this.ColorRithm = Color.FromHex(GoodStateHEX);
            }  
            if (e.ProblemRank == 1)
            {
                this.RithmRank = 4;
                this.ColorRithm = Color.FromHex(So_SoStateHEX);
            }
            if (e.ProblemRank >= 2)
            {
                this.RithmRank = 3;
                this.ColorRithm = Color.FromHex(BadStateHEX);
            }
        }


        #endregion
        

        //Экстрасистолия
        #region 

        private byte ExtrasistoleRank = 5;
        private uint CountVentricularExtrasistole = 0;
        private uint CountAtrialExtrasistole = 0;

        private string smallExtrasystoleDiscription = "Нет отклонений";
        public string SmallExtrasystoleDiscription
        {
            get { return smallExtrasystoleDiscription; }
            set
            {
                if (smallExtrasystoleDiscription != value)
                {
                    smallExtrasystoleDiscription = value;
                    OnPropertyChanged("SmallExtrasystoleDiscription");
                }
            }
        }

        private string extrasystoleDiscription = "Нет отклонений";
        public string ExtrasystoleDiscription
        {
            get { return extrasystoleDiscription; }
            set
            {
                if (extrasystoleDiscription != value)
                {
                    extrasystoleDiscription = value;
                    OnPropertyChanged("ExtrasystoleDiscription");
                }
            }
        }
        private Color colorExtrasystole = Color.FromHex(GoodStateHEX);
        public Color ColorExtrasystole
        {
            get { return colorExtrasystole; }
            set
            {
                if (colorExtrasystole != value)
                {
                    colorExtrasystole = value;
                    OnPropertyChanged("ColorExtrasystole");
                }
            }
        }
        private int widhtExtrasystoleFrame = 100;
        public int WidhtExtrasystoleFrame
        {
            get { return widhtExtrasystoleFrame; }
            set
            {
                if (widhtExtrasystoleFrame != value)
                {
                    widhtExtrasystoleFrame = value;
                    OnPropertyChanged("WidhtExtrasystoleFrame");
                }
            }
        }
        private int heightExtrasystoleFrame = 100;
        public int HeightExtrasystoleFrame
        {
            get { return heightExtrasystoleFrame; }
            set
            {
                if (heightExtrasystoleFrame != value)
                {
                    heightExtrasystoleFrame = value;
                    OnPropertyChanged("HeightExtrasystoleFrame");
                }
            }
        }

        
        private void ExtrasistoleReaction(object sender, ExtrasystoleEventArgs e)
        {
            this.ExtrasystoleDiscription = e.ExtrasystoleDiscription;
            switch (e.ProblemRank)
            {
                case 0:
                    this.ExtrasistoleRank = 5;
                    this.SmallExtrasystoleDiscription = "Нет отклонений";
                    this.ColorExtrasystole = Color.FromHex(GoodStateHEX);
                    break;
                case 1:
                    this.ExtrasistoleRank = 5;
                    this.SmallExtrasystoleDiscription = "Небольшие отклонения";
                    this.ColorExtrasystole = Color.FromHex(GoodStateHEX);
                    break;
                case 2:
                    this.ColorExtrasystole = Color.FromHex(So_SoStateHEX);
                    this.SmallExtrasystoleDiscription = "Опасно";
                    this.ExtrasistoleRank = 4;
                    this.ExtrasystoleDiscription += Environment.NewLine;
                    this.ExtrasystoleDiscription += Environment.NewLine;
                    this.ExtrasystoleDiscription += "Предсердных: " + e.AtrialPrematureBeatsCount.ToString();
                    this.ExtrasystoleDiscription += Environment.NewLine;
                    this.ExtrasystoleDiscription += "Желудочковых: " + e.VentricularExtrasystoleCount.ToString();
                    break;
            }
        }
        


        #endregion

        //Паузы
        #region 
        private byte PauseRank = 5;
        private string pauseDiscription = "Нет отклонений";
        public string PauseDiscription
        {
            get { return pauseDiscription; }
            set
            {
                if (pauseDiscription != value)
                {
                    pauseDiscription = value;
                    OnPropertyChanged("PauseDiscription");
                }
            }
        }
        private string smallPauseDiscription = "Нет отклонений";
        public string SmallPauseDiscription
        {
            get { return smallPauseDiscription; }
            set
            {
                if (smallPauseDiscription != value)
                {
                    smallPauseDiscription = value;
                    OnPropertyChanged("SmallPauseDiscription");
                }
            }
        }
        private Color colorPause = Color.FromHex(GoodStateHEX);
        public Color ColorPause
        {
            get { return colorPause; }
            set
            {
                if (colorPause != value)
                {
                    colorPause = value;
                    OnPropertyChanged("ColorPause");
                }
            }
        }
        private int widhtPauseFrame = 100;
        public int WidhtPauseFrame
        {
            get { return widhtPauseFrame; }
            set
            {
                if (widhtPauseFrame != value)
                {
                    widhtPauseFrame = value;
                    OnPropertyChanged("WidhtPauseFrame");
                }
            }
        }
        private int heightPauseFrame = 100;
        public int HeightPauseFrame
        {
            get { return heightPauseFrame; }
            set
            {
                if (heightPauseFrame != value)
                {
                    heightPauseFrame = value;
                    OnPropertyChanged("HeightPauseFrame");
                }
            }
        }
        #endregion

        //ИМТ
        #region 
        private string bmiTitle = "Отклонений нет";
        public string BMITitle
        {
            get { return bmiTitle; }
            set
            {
                if (bmiTitle != value)
                {
                    bmiTitle = value;
                    OnPropertyChanged("BMITitle");
                }
            }
        }
        private string bmiDiscription = "";
        public string BMIDiscription
        {
            get { return bmiDiscription; }
            set
            {
                if (bmiDiscription != value)
                {
                    bmiDiscription = value;
                    OnPropertyChanged("BMIDiscription");
                }
            }
        }
        private Color colorBMI = Color.FromHex(GoodStateHEX);
        public Color ColorBMI
        {
            get { return colorBMI; }
            set
            {
                if (colorBMI != value)
                {
                    colorBMI = value;
                    OnPropertyChanged("ColorBMI");
                }
            }
        }
        private int widhtBMIFrame = 100;
        public int WidhtBMIFrame
        {
            get { return widhtBMIFrame; }
            set
            {
                if (widhtBMIFrame != value)
                {
                    widhtBMIFrame = value;
                    OnPropertyChanged("WidhtBMIFrame");
                }
            }
        }
        private int heightBMIFrame = 100;
        public int HeightBMIFrame
        {
            get { return heightBMIFrame; }
            set
            {
                if (heightBMIFrame != value)
                {
                    heightBMIFrame = value;
                    OnPropertyChanged("HeightBMIFrame");
                }
            }
        }

        void BodyMassIndexChange(object sender, BodyMassIndexEventArgs e)
        {
            this.ColorBMI = e.BodyMassIndexColor;
            this.BMITitle = e.BodyMassIndexTitleDiscription;
            if (e.BodyMassIndexRating == 2)
                this.BMIDiscription = "Отлично. У вас наблюдается " + e.BodyMassIndexDiscription;
            else
                this.BMIDiscription = "Возможные риски и патологии: " + e.BodyMassIndexDiscription;
        }

        #endregion

        private byte heightFramePart = 3;
        
        public void UdateFramesSize(double width, double height)
        {
            this.WidhtPulseFrame = (int)(4 * width / 11);
            this.HeightPulseFrame = (int)(height / heightFramePart);
            this.WidhtRithmFrame = (int)(6 * width / 11);
            this.HeightRithmFrame = (int)(height / heightFramePart);

            this.WidhtExtrasystoleFrame = (int)(width);
            this.HeightExtrasystoleFrame = (int)(height / heightFramePart);

            this.WidhtBMIFrame = 6 * (int)(width / 11);
            this.HeightBMIFrame = (int)(height / heightFramePart);
            this.WidhtPauseFrame = 4 * (int)(width / 11);
            this.HeightPauseFrame = (int)(height / heightFramePart);
        }
        

        #endregion

        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }

        public async void Dispose()
        {
            
        }
    }
}
