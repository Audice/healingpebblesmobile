﻿using HealingPebblesMobile.Model.BluetoothPart;
using HealingPebblesMobile.View.Pages;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace HealingPebblesMobile.ViewModel
{
    public class ModeSelectViewModel : INotifyPropertyChanged
    {
        public INavigation Navigation { get; set; }

        public ModeSelectViewModel()
        {
        }

        public ICommand RunECGMonitoring => new Command(async() => {
            await Navigation.PushAsync(new HolterModePage());
        });

        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
