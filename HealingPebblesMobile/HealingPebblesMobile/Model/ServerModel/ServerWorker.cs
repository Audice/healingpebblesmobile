﻿using Plugin.Connectivity;
using Plugin.Connectivity.Abstractions;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace HealingPebblesMobile.Model.ServerPart
{
    /// <summary>
    /// Класс, отвечающий за отправку и приём информации 
    /// </summary>
    public class ServerWorker
    {
        private static ServerWorker ServerWorkerInstance;


        bool isInternetConnection = false;
        /// <summary>
        /// Свойство, характеризующее текущее состояние подключение к сети интернет
        /// </summary>
        public bool IsInternetConnection
        {
            get { return isInternetConnection; }
            private set
            {
                isInternetConnection = value;
                ChangeConnectionState?.Invoke(this, EventArgs.Empty);
            }
        }
        public EventHandler<EventArgs> ChangeConnectionState;

        public string URLServer { get; private set; } = "http://192.168.43.182:8000";

        /// <summary>
        /// Объект отправщик soap пакетов
        /// </summary>
        private HttpClient MessageSender;

        protected ServerWorker()
        {
            MessageSender = new HttpClient();
            MessageSender.DefaultRequestHeaders.Add("SOAPAction", URLServer);

            this.IsInternetConnection = CrossConnectivity.Current.IsConnected;

            CrossConnectivity.Current.ConnectivityChanged += ConnectivityChanged;
        }

        /// <summary>
        /// Реакция на изменение состояния подключения к сети интернет
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void ConnectivityChanged(object sender, ConnectivityChangedEventArgs e)
        {
            this.IsInternetConnection = e.IsConnected;
        }

        public static ServerWorker getInstance()
        {
            if (ServerWorkerInstance == null)
            {
                ServerWorkerInstance = new ServerWorker();
            }
            return ServerWorkerInstance;
        }

        public byte[] CovertData(short[] data)
        {
            string sss = "";
            for (int i = 0; i < data.Length; i++)
                sss += data[i].ToString() + Environment.NewLine;
            byte[] bytesArray = new byte[data.Length * sizeof(Int16)];
            Buffer.BlockCopy(data, 0, bytesArray, 0, bytesArray.Length);
            return bytesArray;
        }


        /// <summary>
        /// Задача отправки soap пакета на сервер
        /// </summary>
        /// <param name="soapPackage">Текстовое представление SOAP пакета</param>
        /// <returns>Ответ от сервера в виде строки</returns>
        public async Task<string> SendPackage(byte[] soapPackage)
        {
            if (MessageSender == null || soapPackage == null) return null;
            var soapResponse = "";
            try
            {
                var content = new ByteArrayContent(soapPackage);
                var response = await MessageSender.PostAsync(new Uri(URLServer), content);
                soapResponse = await response.Content.ReadAsStringAsync();
                string returnNode = GetReturnNode(soapResponse, "return");
                return returnNode;
            }
            catch (Exception e)
            {
                return null;
            }
        }





        /// <summary>
        /// Поиск значения в узле NodeName в ответе от сервера
        /// </summary>
        /// <param name="Response">Ответ от сервера</param>
        /// <param name="NodeName">Целевой узел</param>
        /// <returns></returns>
        private string GetReturnNode(string Response, string NodeName)
        {
            string resultString = "";
            try
            {
                var xmlReader = XmlReader.Create(new StringReader(Response));

                do xmlReader.Read();
                while (!xmlReader.Name.Contains(NodeName) && !xmlReader.Name.Equals(""));

                xmlReader.Read();
                resultString = xmlReader.Value;
            }
            catch (Exception ex)
            {
                string ExceptionMessage = ex.Message;
                resultString = null;
            }
            if (resultString != null && resultString.Length == 0) resultString = null;

            return resultString;
        }


    }
}
