﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace HealingPebblesMobile.Model.BluetoothPart
{
    public delegate void ChangeStateBluetoothConnectHandler();
    public interface IBluetoothController
    {
        event ChangeStateBluetoothConnectHandler ChangeStateBluetoothConnectEvent;

        /// <summary>
        /// Функция поиска доступных bluetooth устройств. 
        /// </summary>
        /// <param name="deviceName">Имя девайса (FireFly или устройство обработки ЭЭГ)</param>
        /// <returns>Пара значений - название устройства \ его MAC адресс</returns>
        List<Tuple<string, string>> GetListDevices(string deviceName);
        /// <summary>
        /// Проверка наличия bluetooth модуля
        /// </summary>
        /// <returns>True - модуль есть и готов к работе, False - модуль отсутствуе или сломан</returns>
        bool CheckBluetoothModule();
        /// <summary>
        /// Функция создания подключения к устройству по имеющемуся MAC-адресу устройства
        /// </summary>
        /// <param name="MacAdres">Адрес bluetooth устройства</param>
        /// <returns>Идентификатор успешности подключения: >= 0 - подключенике успешно, иначе - ошибка на одном из этапов соединения</returns>
        Task<int> ConnectBluetooth(string MacAdres);
        /// <summary>
        /// Отправка команды устройству. Команда представлена в виде массива byte
        /// </summary>
        /// <param name="command">Массив байт, являющийся командой bluetooth устройству</param>
        /// <returns>Код результативности операции отправки команды</returns>
        Task<int> SendCommand(byte[] command);
        /// <summary>
        /// Отправка проверочной команнды для подтверждения активного статуса потоков
        /// </summary>
        /// <param name="command">Байтовый массив, представляющий комманду</param>
        /// <returns>Код готовности потоков чтения\записи: 0 - готов, < 0 - есть проблемы</returns>
        Task<int> SendCheckableCommand(byte[] command);
        /// <summary>
        /// Функция получения данных от Bluetooth устройства
        /// </summary>
        /// <returns>Массив байт, получаемый из Bluetooth стэка</returns>
        Task<byte[]> ReadData();
        /// <summary>
        /// Метод проверки состояния Bluetooth сокета
        /// </summary>
        /// <returns>true - соединение присутствует</returns>
        bool GetBluetoothConnectState();
        /// <summary>
        /// Уничтожение текущего Bluetooth соединения
        /// </summary>
        /// <returns>true - уничтожение успешно</returns>
        bool CloseCurrentBluetoothConnect();
        /// <summary>
        /// Разрыв Bluetooth соединения, без уничтожения экземпляоа соединения
        /// </summary>
        /// <returns>true - bluetooth соединение разорвано</returns>
        bool BreakBluetoothConnect();
    }
}
