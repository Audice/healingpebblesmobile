﻿using HealingPebblesMobile.Model.BluetoothPart.CustomEventArgsClasses;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace HealingPebblesMobile.Model.BluetoothPart
{
    public class BluetoothConnectServicer
    {
        /// <summary>
        /// Событие, возникающее при появлении новой порции данных от Bluetooth. 
        /// Используется изменённый EventHandler для передачи массива byte.
        /// </summary>
        public event EventHandler<DataReadyEventArgs> RawDataReadyEvent;
        /// <summary>
        /// Метод инициирующий событие готовности новой порции данных для подписчиков
        /// </summary>
        /// <param name="e">Кастомный EventArgs, для передачи массива сырых данных подписчикам</param>
        protected virtual void OnRawDataReady(DataReadyEventArgs e)
        {
            EventHandler<DataReadyEventArgs> handler = RawDataReadyEvent;
            if (handler != null)
            {
                handler(this, e);
            }
        }
        
        bool isBluetoothConnect = false;
        /// <summary>
        /// Свойство, характеризующее состояние соединения с Bluetooth девайсом
        /// </summary>
        public bool IsBluetoothConnect
        {
            get { return isBluetoothConnect; }
            private set
            {
                if (isBluetoothConnect != value)
                {
                    isBluetoothConnect = value;
                    ChangeBluetoothStateEvent?.Invoke(this, EventArgs.Empty);
                }
            }
        }

        private bool FirstBluetoothFlag = false;
        private bool SecondBluetoothFlag = false;


        /// <summary>
        /// Кастомный делегат для события смены состояния Bluetooth подключения
        /// </summary>
        public delegate void ChangeBluetoothStateHandler();
        /// <summary>
        /// Событие, оповещающее подписчиков о смене состояния Bluetooth соединения.
        /// </summary>
        public EventHandler ChangeBluetoothStateEvent; //Возможно нужно использовать EventHandler...
        /// <summary>
        /// Имя Bluetooth устройств, к которому производится подключение
        /// </summary>
        private string DeviceName;
        /// <summary>
        /// Идентификатор Bluetooth устройства, представленный в виде MAC-адреса
        /// </summary>
        public string DeviceMAC { get; private set; }
        //Базовый команды, представленные в виде байтовых массивов
        /// <summary>
        /// Проверка каналов I\O устройства, поиск устройства
        /// </summary>
        readonly byte[] FindCommand = new byte[] { 70, 73, 78, 68, 13, 10 };
        /// <summary>
        /// Команда, активирующая бесконечную передачу данных ЭКГ
        /// </summary>
        readonly byte[] GETECGCommand = new byte[] { 71, 69, 84, 32, 69, 67, 71, 13, 10 };
        /// <summary>
        /// Команда, позволяющая получить состояние батареи Bluetooth устройства
        /// </summary>
        readonly byte[] GetBatteryStateCommand = new byte[] { 71, 69, 84, 32, 66, 65, 84, 84, 13, 10 };

        readonly byte[] GetNFileCommand = new byte[] { 71, 69, 84, 32, 70, 73, 76, 69, 32 };

        readonly byte[] SetChannelsCommand = new byte[] { 83, 69, 84, 32, 67, 72, 32};

        readonly byte[] GetDirInfoCommand = new byte[] { 68, 73, 82, 32, 63, 13, 10 };

        readonly byte[] ClearDirCommand = new byte[] { 68, 73, 82, 32, 67, 76, 82, 13, 10 };

        readonly byte[] SetSampleFrequencyCommand = new byte[] { 83, 69, 84, 32, 83, 80, 83, 32 };

        readonly byte[] DirectorySettingCommand = new byte[] { 68, 73, 82, 32, 83, 69, 84, 32 };

        readonly byte[] TimeSettingCommand = new byte[] { 83, 69, 84, 32, 84, 73, 77, 69, 32 };

        /// <summary>
        /// Флаг наличия Bluetooth модуля
        /// </summary>
        public bool HasBluetoothModuleError
        {
            get; private set;
        } = false;

        /// <summary>
        /// Флаг валидности Bluetooth девайса
        /// </summary>
        public bool HasCurrentBluetoothDevice
        {
            get; private set;
        } = false;


        public BluetoothConnectServicer(string nameNativeDevice, string MAC_Adress = null)
        {
            this.DeviceName = nameNativeDevice;
            this.DeviceMAC = MAC_Adress;
            //DependencyService.Get<IBluetoothController>().ChangeStateBluetoothConnectEvent += CheckBluetoothState;
            this.ChangeBluetoothStateEvent += ActionBluetoothConnectChange;
            this.ChangeBluetoothStateEvent += ControlReadProcess;
            if (this.DeviceMAC != null && this.DeviceMAC.Length == 17)
            {
                this.Connect();
                BatteryStateTask().ContinueWith(RunnerBatteryStateTask);
            }
        }

        public void UpdateDevicesMAC(string newMAC)
        {
            if (!(newMAC != null && newMAC.Length == 17) || !(DependencyService.Get<IBluetoothController>().CheckBluetoothModule()))
                throw new ArgumentException();

            var devicesList = DependencyService.Get<IBluetoothController>().GetListDevices(this.DeviceName);
            if (devicesList != null && devicesList.Count > 0 && (devicesList.Find(x => x.Item2 == newMAC) != null))
            {
                if (this.DeviceMAC == null)
                {
                    this.DeviceMAC = newMAC;
                    this.Connect();
                    BatteryStateTask().ContinueWith(RunnerBatteryStateTask);
                }
                else
                {
                    if (!this.IsConnectStart)
                    {
                        //Разрываем предыдущее соединение и запускаем новое с новым MAC адресом
                        this.DeviceMAC = newMAC;
                        this.CloseBluetooth();
                        this.ResetConnectFlags();
                    }
                }
            }
            //Иначе не делаем ничего
        }


        /// <summary>
        /// Активное подключение к Bluetooth устройству. При неудачном подключении повторяет попытки
        /// </summary>
        /// <returns></returns>
        public Task Connect()
        {
            return Task.Run(async () =>
            {
                this.IsConnectStart = true;

                if (!this.HasCurrentBluetoothDevice) RegisterBluetoothDevice(this.DeviceMAC);

                if (this.HasCurrentBluetoothDevice)
                {
                    while (!this.IsBluetoothConnect)
                    {
                        //Пока соединение не установлено или не активно продолжаем попытки соединения
                        int connectResult = await DependencyService.Get<IBluetoothController>().ConnectBluetooth(DeviceMAC);
                        await Task.Delay(1000);
                        if (connectResult >= 0)
                        {
                            this.IsBluetoothConnect = true;
                            this.FirstBluetoothFlag = true;
                        }
                    }
                }
                this.IsConnectStart = false;
            });
        }



        /// <summary>
        /// Метод-подписчик на событие отключения потоков ввода\вывода.
        /// Запускает процедуру постоянного опроса потока данных
        /// </summary>
        private void ControlReadProcess(object sender, EventArgs e)
        {
            if (this.IsBluetoothConnect)
            {
                ReadTask().ContinueWith(RunnerReadTask);
            }
        }



        /// <summary>
        /// Задача, опрашивающая поток данных на наличие новых данных
        /// </summary>
        /// <returns></returns>
        async Task ReadTask()
        {
            if (this.IsBluetoothConnect)
            {
                byte[] array = await DependencyService.Get<IBluetoothController>().ReadData();
                if (array != null)
                {
                    CountReadByte = array.Length.ToString(); //Debug
                    DataReadyEventArgs args = new DataReadyEventArgs();
                    args.RawData = array;
                    this.OnRawDataReady(args);
                }
            }
        }
        /// <summary>
        /// Метод, запускающий опрашивающую поток данных задачу, используюя ContinueWith
        /// </summary>
        /// <param name="t"></param>
        void RunnerReadTask(Task t)
        {
            if (this.IsBluetoothConnect)
            {
                ReadTask().ContinueWith(RunnerReadTask);
            }
        }


        /// <summary>
        /// Задача, опрашивающая поток данных на наличие новых данных
        /// </summary>
        /// <returns></returns>
        async Task BatteryStateTask()
        {
            if (this.IsBluetoothConnect)
            {
                await Task.Delay(2000);
                BattaryCommandSend();
            }
            await Task.Delay(20000);
        }
        /// <summary>
        /// Метод, запускающий опрашивающую поток данных задачу, используюя ContinueWith
        /// </summary>
        /// <param name="t"></param>
        void RunnerBatteryStateTask(Task t)
        {
            BatteryStateTask().ContinueWith(RunnerBatteryStateTask);
        }

        private void ActionBluetoothConnectChange(object sender, EventArgs e)
        {
            if (!this.IsBluetoothConnect)
            {
                //Произошёл тролл..... дисконнект. Делаем повторное подключение и отключаем потоки
                if (!IsConnectStart)
                {
                    ResetConnectFlags(); //Так как Bluetooth отключен, то и потоки отключены 
                    Connect();
                }
            }
        }

        private void ResetConnectFlags()
        {
            this.IsBluetoothConnect = false;
            this.FirstBluetoothFlag = false;
            this.SecondBluetoothFlag = false;
        }
        /// <summary>
        /// Проверка потоков ввода\вывода Bluetooth устройства через механизм DependencyService
        /// </summary>
        /// <returns>true если потоки активны и работают, иначе false</returns>
        private async Task<bool> CheckIOChannel()
        {
            if (this.FirstBluetoothFlag && (await DependencyService.Get<IBluetoothController>().SendCheckableCommand(FindCommand)) >= 0)
            {
                return true;
            }
            return false;
        }

        public bool IsConnectStart
        {
            get; private set;
        } = false;


        /// <summary>
        /// Регистрация MAC адреса устройства в классе с последующей проверкой
        /// </summary>
        /// <param name="MAC_Adress"></param>
        private void RegisterBluetoothDevice(string MAC_Adress)
        {
            //17 символов - стандартная длина MAC адреса
            if (MAC_Adress == null || MAC_Adress.Length != 17)
            {
                this.HasCurrentBluetoothDevice = false;
            }
            else
            {
                if (DependencyService.Get<IBluetoothController>().CheckBluetoothModule())
                {
                    var devicesList = DependencyService.Get<IBluetoothController>().GetListDevices(this.DeviceName);
                    if (devicesList.Count > 0 && (devicesList.Find(x => x.Item2 == MAC_Adress) != null))
                    {
                        this.HasCurrentBluetoothDevice = true;
                    }
                    else
                    {
                        this.HasCurrentBluetoothDevice = false; //Девайса нет в списке
                        //this.IsBluetoothStreamsReady = false;
                    }
                }
                else
                {
                    this.HasBluetoothModuleError = true;
                    this.HasCurrentBluetoothDevice = false;
                    //this.IsBluetoothStreamsReady = false;
                }
            }
        }

        public bool IsECGStreamRun
        {
            get; private set;
        } = false;
        public async void GET_ECGSend()
        {
            if (IsECGStreamRun) return;
            IsECGStreamRun = true;
            this.IsBluetoothConnect = await DependencyService.Get<IBluetoothController>().SendCommand(GETECGCommand) >= 0 ? true : false;
            IsECGStreamRun = false;
        }

        public async void BattaryCommandSend()
        {
            if (this.IsBluetoothConnect)
                this.IsBluetoothConnect = await DependencyService.Get<IBluetoothController>().SendCommand(GetBatteryStateCommand) >= 0 ? true : false;
        }

        public async void GetConcretFile(int minutesNumber)
        {
            if (minutesNumber >= 0)
            {
                byte[] numberFile = Encoding.ASCII.GetBytes(minutesNumber.ToString());
                List<byte> command = new List<byte>();
                command.AddRange(GetNFileCommand); command.AddRange(numberFile); command.AddRange(new byte[] { 13, 10 });
                this.IsBluetoothConnect = await DependencyService.Get<IBluetoothController>().SendCommand(command.ToArray()) >= 0 ? true : false;
            }
            else
            {
                return;
            }
        }

        public async void SetCountChannel(int countChannels)
        {
            List<byte> command = new List<byte>();
            command.AddRange(this.SetChannelsCommand);
            int localCountChannels = countChannels <= 2 && countChannels >= 1 ? countChannels : 1;

            string strChannel = countChannels.ToString();

            command.Add((byte)strChannel[0]);
            command.Add(13); command.Add(10);
            this.IsBluetoothConnect = await DependencyService.Get<IBluetoothController>().SendCommand(command.ToArray()) >= 0 ? true : false;
        }

        public async void SetSampleFrequency(int sampleFrequency)
        {
            List<byte> command = new List<byte>();
            command.AddRange(this.SetSampleFrequencyCommand);
            switch (sampleFrequency)
            {
                case 500: command.AddRange(new byte[] { 48, 53, 48, 48 });  break;
                case 1000: command.AddRange(new byte[] { 49, 48, 48, 48 }); break;
                case 2000: command.AddRange(new byte[] { 50, 48, 48, 48 }); break;
                case 4000: command.AddRange(new byte[] { 52, 48, 48, 48 }); break;
                case 8000: command.AddRange(new byte[] { 56, 48, 48, 48 }); break;
                default: command.AddRange(new byte[] { 48, 53, 48, 48 }); break;
            }
            command.Add(13); command.Add(10);
            this.IsBluetoothConnect = await DependencyService.Get<IBluetoothController>().SendCommand(command.ToArray()) >= 0 ? true : false;
        }

        public async void SetDirectory(short directoryNumber)
        {
            List<byte> command = new List<byte>();
            command.AddRange(this.DirectorySettingCommand);
            string strDirectory = directoryNumber.ToString();
            for (int i=0; i < strDirectory.Length; i++)
            {
                command.Add((byte)strDirectory[i]);
            }
            command.Add(13); command.Add(10);
            this.IsBluetoothConnect = await DependencyService.Get<IBluetoothController>().SendCommand(command.ToArray()) >= 0 ? true : false;
        }

        public async void GetDirInfo()
        {
            this.IsBluetoothConnect = await DependencyService.Get<IBluetoothController>().SendCommand(this.GetDirInfoCommand) >= 0 ? true : false;
        }

        public async void ClearDir()
        {
            this.IsBluetoothConnect = await DependencyService.Get<IBluetoothController>().SendCommand(this.ClearDirCommand) >= 0 ? true : false;
        }


        /// <summary>
        /// DEBUG!!!
        /// </summary>
        private string countReadByte = "0";
        public string CountReadByte
        {
            get { return countReadByte; }
            set {
                countReadByte = value;
                ChangeCountReadByteEvent?.Invoke();
            }
        }
        public delegate void ChangeCountReadByteHandler();
        public event ChangeCountReadByteHandler ChangeCountReadByteEvent;

        //Закрыть соединение
        public void CloseBluetooth()
        {
            DependencyService.Get<IBluetoothController>().CloseCurrentBluetoothConnect();
        }
    }
}
