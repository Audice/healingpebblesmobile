﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HealingPebblesMobile.Model.BluetoothPart.CustomEventArgsClasses
{
    public class ConvertedSignalEventArgs: EventArgs
    {
        public uint PackageNumber { get; set; }
        public List<short[]> Signals { get; set; }
    }
}
