﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HealingPebblesMobile.Model.BluetoothPart.CustomEventArgsClasses
{
    public class DataReadyEventArgs : EventArgs
    {
        public byte[] RawData {get; set;}
    }
}
