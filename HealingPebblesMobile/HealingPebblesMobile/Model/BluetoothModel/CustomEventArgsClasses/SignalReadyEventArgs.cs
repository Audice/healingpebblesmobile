﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HealingPebblesMobile.Model.BluetoothModel.CustomEventArgsClasses
{
    public class SignalReadyEventArgs : EventArgs
    {
        public short[] Data { get; set; }
    }
}
