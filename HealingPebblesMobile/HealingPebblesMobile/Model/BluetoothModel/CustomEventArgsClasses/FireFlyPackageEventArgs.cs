﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HealingPebblesMobile.Model.BluetoothPart.CustomEventArgsClasses
{
    public class FireFlyPackageEventArgs : EventArgs
    {
        public FireFlyPackage Package { get; set; }
    }
}
