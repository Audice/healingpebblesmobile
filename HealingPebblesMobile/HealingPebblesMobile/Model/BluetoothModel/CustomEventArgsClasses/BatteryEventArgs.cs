﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HealingPebblesMobile.Model.BluetoothPart.CustomEventArgsClasses
{
    public class BatteryEventArgs : EventArgs
    {
        public byte ChargeLevel { get; set; }
    }
}
