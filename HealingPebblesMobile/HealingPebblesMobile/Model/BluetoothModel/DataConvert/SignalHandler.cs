﻿using HealingPebblesMobile.Model.BluetoothPart.DataConvert.Filters;
using HealingPebblesMobile.Model.BluetoothPart.DataConvert.SignalsFilters;
using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Text;

namespace HealingPebblesMobile.Model.BluetoothPart.DataConvert
{
    public class SignalHandler
    {
        private int SampleRate = 500;
        private readonly byte AdditionalLeads = 6;

        public uint LeadsCount { get; private set; }
        public uint ChannelsNumber { get; private set; }

        private ushort UMV { get; set; } = 1;

        //private Biquad Filter_1CH_N1;
        private Biquad[] Filter_1CH_N2 = null;
        private Biquad[] Filter_1CH_N3 = null;
        //private Biquad Filter_2CH_N1;
        //private Biquad Filter_2CH_N2;
        //private Biquad Filter_2CH_N3;

        private MovinAverage[] IsolineFilter;
        private Integral[] IntegralFilter;
        private Differencial[] DifferencialFilter;

        public SignalHandler(uint channelsNumber, uint sampleRate = 500)
        {
            this.ChannelsNumber = channelsNumber;
            if (this.ChannelsNumber > 0 && this.ChannelsNumber <= 8)
            {
                this.LeadsCount = 1;
                if (this.ChannelsNumber == 2)
                {
                    this.LeadsCount = 6;
                }
                if (this.ChannelsNumber == 8)
                {
                    this.LeadsCount = 12;
                }  
            }
            else
            {
                this.LeadsCount = 0;
            }

            if (this.LeadsCount > 0)
            {
                ECGFilterInit(this.ChannelsNumber);
                this.SampleRate = (int)sampleRate;

                this.IsolineFilter = new MovinAverage[this.ChannelsNumber];
                this.IntegralFilter = new Integral[this.ChannelsNumber];
                this.DifferencialFilter = new Differencial[this.LeadsCount];

                //Создание фильтров для восстановления сигнала и выравнивания
                for (int i = 0; i < this.ChannelsNumber; i++)
                {
                    this.IsolineFilter[i] = new MovinAverage();
                    this.IntegralFilter[i] = new Integral();
                    this.IntegralFilter[i].Clear();
                    this.IsolineFilter[i].setLenght(this.SampleRate);
                }
                //Создание фильтров для восстановления сигнала и выравнивания
                for (int i = 0; i < this.LeadsCount; i++)
                {
                    this.DifferencialFilter[i] = new Differencial();
                    this.DifferencialFilter[i].Clear();
                }
            }
            
        }

        public void SetUMV(ushort umv)
        {
            this.UMV = umv;
        }

        void ECGFilterInit(uint channelsCount)
        {
            this.Filter_1CH_N2 = new Biquad[channelsCount];
            this.Filter_1CH_N3 = new Biquad[channelsCount];

            //Инициализация для Filter_1CH_N2
            for (int i=0; i < this.Filter_1CH_N2.Length; i++)
            {
                this.Filter_1CH_N2[i] = new Biquad();
                this.Filter_1CH_N2[i].setBiquad((int)FilterType.bq_type_notch, 50.0 / this.SampleRate, 0.71, 50.0);
                this.Filter_1CH_N3[i] = new Biquad();
                this.Filter_1CH_N3[i].setBiquad((int)FilterType.bq_type_lowpass, 30.0 / this.SampleRate, 0.71, 50.0);
            }
        }

        public List<short[]> GetDeltasSignals(List<short[]> signals)
        {
            if (signals == null) return null;
            if (signals.Count != this.LeadsCount) return null;

            List<short[]> deltsResult = new List<short[]>();
            for (int i = 0; i < this.LeadsCount; i++)
            {
                deltsResult.Add(this.DifferencialFilter[i].Process(signals[i]));
            }
            return deltsResult;
        }

        /// <summary>
        /// Обработка сырого сигнала
        /// </summary>
        /// <param name="data">Двумерный массив byte</param>
        /// <returns></returns>
        public List<short[]> GetProcessedSignal(List<short[]> signals)
        {
            if (signals == null) return null;
            if (signals.Count != this.ChannelsNumber) return null;

            
            for (int i=0; i < this.ChannelsNumber; i++)
            {
                var qvPaceDataDouble = IsolineFilter[i].FilterProcess(signals[i]);
                qvPaceDataDouble = Filter_1CH_N3[i].FilterProcess(qvPaceDataDouble);
                qvPaceDataDouble = Filter_1CH_N2[i].FilterProcess(qvPaceDataDouble);
                for (int j = 0; j < signals[i].Length; j++)
                {
                    signals[i][j] = (short)(qvPaceDataDouble[j]);
                }
            }

            if (signals == null) return null;
            return signals;
        }
        /// <summary>
        /// Расчёт дополнительных отведений ЭКГ по двум имеющимся
        /// </summary>
        /// <param name="leads">Полученные отведения от кардиографа</param>
        /// <returns>Список данных по отведениям: I, II, III, AVR, AVL, AVF</returns>
        public List<short[]> LeadsCalculate(List<short[]> leads)
        {
            if (leads != null && leads.Count == this.ChannelsNumber)
            {
                if (this.ChannelsNumber >= 2)
                {
                    short[] III = new short[leads[0].Length];
                    short[] avR = new short[leads[0].Length];
                    short[] avL = new short[leads[0].Length];
                    short[] avF = new short[leads[0].Length];
                    for (int i = 0; i < III.Length; i++)
                    {
                        III[i] = (short)(leads[1][i] - leads[0][i]);
                        avR[i] = (short)((-leads[0][i] - leads[1][i]) / 2);
                        avL[i] = (short)((leads[0][i] - III[i]) / 2);
                        avF[i] = (short)((leads[1][i] + III[i]) / 2);
                    }
                    List<short[]> resultList = new List<short[]>();
                    resultList.Add(leads[0]); resultList.Add(leads[1]); resultList.Add(III);
                    resultList.Add(avR); resultList.Add(avL); resultList.Add(avF);
                    return resultList;
                }
                else
                {
                    if (this.ChannelsNumber == 1) return leads;
                }
            }
            return null;
        }

        public List<short[]> SignalsSplitting(byte[,] data)
        {
            if (data == null) return null;
            List<short[]> resultsList = new List<short[]>();
            int signalSize = (int)(data.GetLength(0) / 2); //
            for (int i=0; i < data.GetLength(1); i++)
            {
                short[] currentLead = new short[signalSize];
                int chByte = 0;
                for (int j = 0; j < signalSize; j++)
                {
                    currentLead[j] = BitConverter.ToInt16(new byte[] { data[chByte, i], data[chByte + 1, i] }, 0);
                    chByte += 2;
                }
                resultsList.Add(currentLead);
            }
            if (resultsList.Count == 0) resultsList = null;

            return resultsList;
        }



    }
}
