﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HealingPebblesMobile.Model.BluetoothPart.DataConvert.SignalsFilters
{
    enum FilterType
    {
        bq_type_lowpass = 0,
        bq_type_highpass,
        bq_type_bandpass,
        bq_type_notch,
        bq_type_peak,
        bq_type_lowshelf,
        bq_type_highshelf
    };
    class Biquad
    {

        bool bIsEnable;

        int type;
        double a0, a1, a2, b1, b2;
        double Fc, Q, peakGain;
        double z1, z2;


        public Biquad()
        {
            type = (int)FilterType.bq_type_lowpass;
            a0 = 1.0;
            a1 = a2 = b1 = b2 = 0.0;
            Fc = 0.50;
            Q = 0.707;
            peakGain = 0.0;
            z1 = z2 = 0.0;
            bIsEnable = true;
        }
        public Biquad(int type, double Fc, double Q, double peakGainDB)
        {
            setBiquad(type, Fc, Q, peakGainDB);
            z1 = z2 = 0.0;
            bIsEnable = true;
        }
        public void setBiquad(int type, double Fc, double Q, double peakGainDB)
        {
            this.type = type;
            this.Q = Q;
            this.Fc = Fc;
            setPeakGain(peakGainDB);
        }
        public void setType(int type)
        {
            this.type = type;
            calcBiquad();
        }
        public void setQ(double Q)
        {
            this.Q = Q;
            calcBiquad();
        }

        public void setFc(double Fc)
        {
            this.Fc = Fc;
            calcBiquad();
        }

        public void setPeakGain(double peakGainDB)
        {
            this.peakGain = peakGainDB;
            calcBiquad();
        }

        void calcBiquad()
        {
            double norm;
            double V = Math.Pow(10, Math.Abs(peakGain) / 20.0);
            double K = Math.Tan(Math.PI * Fc);
            switch (this.type)
            {
                case (int)FilterType.bq_type_lowpass:
                    norm = 1 / (1 + K / Q + K * K);
                    a0 = K * K * norm;
                    a1 = 2 * a0;
                    a2 = a0;
                    b1 = 2 * (K * K - 1) * norm;
                    b2 = (1 - K / Q + K * K) * norm;
                    break;

                case (int)FilterType.bq_type_highpass:
                    norm = 1 / (1 + K / Q + K * K);
                    a0 = 1 * norm;
                    a1 = -2 * a0;
                    a2 = a0;
                    b1 = 2 * (K * K - 1) * norm;
                    b2 = (1 - K / Q + K * K) * norm;
                    break;

                case (int)FilterType.bq_type_bandpass:
                    norm = 1 / (1 + K / Q + K * K);
                    a0 = K / Q * norm;
                    a1 = 0;
                    a2 = -a0;
                    b1 = 2 * (K * K - 1) * norm;
                    b2 = (1 - K / Q + K * K) * norm;
                    break;

                case (int)FilterType.bq_type_notch:
                    norm = 1 / (1 + K / Q + K * K);
                    a0 = (1 + K * K) * norm;
                    a1 = 2 * (K * K - 1) * norm;
                    a2 = a0;
                    b1 = a1;
                    b2 = (1 - K / Q + K * K) * norm;
                    break;

                case (int)FilterType.bq_type_peak:
                    if (peakGain >= 0)
                    {    // boost
                        norm = 1 / (1 + 1 / Q * K + K * K);
                        a0 = (1 + V / Q * K + K * K) * norm;
                        a1 = 2 * (K * K - 1) * norm;
                        a2 = (1 - V / Q * K + K * K) * norm;
                        b1 = a1;
                        b2 = (1 - 1 / Q * K + K * K) * norm;
                    }
                    else
                    {    // cut
                        norm = 1 / (1 + V / Q * K + K * K);
                        a0 = (1 + 1 / Q * K + K * K) * norm;
                        a1 = 2 * (K * K - 1) * norm;
                        a2 = (1 - 1 / Q * K + K * K) * norm;
                        b1 = a1;
                        b2 = (1 - V / Q * K + K * K) * norm;
                    }
                    break;
                case (int)FilterType.bq_type_lowshelf:
                    if (peakGain >= 0)
                    {    // boost
                        norm = 1 / (1 + Math.Sqrt(2) * K + K * K);
                        a0 = (1 + Math.Sqrt(2 * V) * K + V * K * K) * norm;
                        a1 = 2 * (V * K * K - 1) * norm;
                        a2 = (1 - Math.Sqrt(2 * V) * K + V * K * K) * norm;
                        b1 = 2 * (K * K - 1) * norm;
                        b2 = (1 - Math.Sqrt(2) * K + K * K) * norm;
                    }
                    else
                    {    // cut
                        norm = 1 / (1 + Math.Sqrt(2 * V) * K + V * K * K);
                        a0 = (1 + Math.Sqrt(2) * K + K * K) * norm;
                        a1 = 2 * (K * K - 1) * norm;
                        a2 = (1 - Math.Sqrt(2) * K + K * K) * norm;
                        b1 = 2 * (V * K * K - 1) * norm;
                        b2 = (1 - Math.Sqrt(2 * V) * K + V * K * K) * norm;
                    }
                    break;
                case (int)FilterType.bq_type_highshelf:
                    if (peakGain >= 0)
                    {    // boost
                        norm = 1 / (1 + Math.Sqrt(2) * K + K * K);
                        a0 = (V + Math.Sqrt(2 * V) * K + K * K) * norm;
                        a1 = 2 * (K * K - V) * norm;
                        a2 = (V - Math.Sqrt(2 * V) * K + K * K) * norm;
                        b1 = 2 * (K * K - 1) * norm;
                        b2 = (1 - Math.Sqrt(2) * K + K * K) * norm;
                    }
                    else
                    {    // cut
                        norm = 1 / (V + Math.Sqrt(2 * V) * K + K * K);
                        a0 = (1 + Math.Sqrt(2) * K + K * K) * norm;
                        a1 = 2 * (K * K - 1) * norm;
                        a2 = (1 - Math.Sqrt(2) * K + K * K) * norm;
                        b1 = 2 * (K * K - V) * norm;
                        b2 = (V - Math.Sqrt(2 * V) * K + K * K) * norm;
                    }
                    break;
            }

            return;
        }

        public double process(double inVal)
        {
            double outVal = inVal * a0 + z1;
            z1 = inVal * a1 + z2 - b1 * outVal;
            z2 = inVal * a2 - b2 * outVal;
            return outVal;
        }

        public double[] FilterProcess(short[] qvInput)
        {
            double[] qvOutput = new double[qvInput.Length];
            if (bIsEnable)
            {
                for (int i = 0; i < qvInput.Length; i++)
                {
                    qvOutput[i] = process(qvInput[i]);
                }
            }
            else
            {
                for (int i = 0; i < qvInput.Length; i++)
                {
                    qvOutput[i] = qvInput[i];
                }
            }
            return qvOutput;
        }

        public double[] FilterProcess(double[] qvInput)
        {
            double[] qvOutput = new double[qvInput.Length];
            if (bIsEnable)
            {
                for (int i = 0; i < qvInput.Length; i++)
                {
                    qvOutput[i] = process(qvInput[i]);
                }
            }
            else
            {
                for (int i = 0; i < qvInput.Length; i++)
                {
                    qvOutput[i] = qvInput[i];
                }
            }
            return qvOutput;
        }

    }
}
