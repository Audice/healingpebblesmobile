﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HealingPebblesMobile.Model.BluetoothPart.DataConvert.SignalsFilters
{
    public class MovinAverage
    {
        bool bIsEnable;

        int lenght;
        double[] qvBuffer;
        double dSumm;
        int Cnt;

        public MovinAverage()
        {
            this.Cnt = 0;
            this.lenght = 10000;
            this.qvBuffer = new double[lenght + 20];
        }

        public MovinAverage(int lenght)
        {
            this.bIsEnable = true;
            this.lenght = lenght;
        }
        public void setLenght(int lenght)
        {
            this.Cnt = 0;
            this.lenght = lenght;
            this.qvBuffer = new double[lenght + 20];
            this.dSumm = 0;
        }

        public double[] FilterProcess(int[] qvInput)
        {
            double[] qvOutput = new double[qvInput.Length];
            if (bIsEnable)
            {
                for (int i = 0; i < qvInput.Length; i++)
                {
                    qvOutput[i] = qvInput[i] - process(qvInput[i]);
                }
            }
            else
            {
                for (int i = 0; i < qvInput.Length; i++)
                {
                    qvOutput[i] = qvInput[i];
                }
            }

            return qvOutput;
        }

        public double[] FilterProcess(short[] qvInput)
        {
            double[] qvOutput = new double[qvInput.Length];
            if (bIsEnable)
            {
                for (int i = 0; i < qvInput.Length; i++)
                {
                    qvOutput[i] = qvInput[i] - process(qvInput[i]);
                }
            }
            else
            {
                for (int i = 0; i < qvInput.Length; i++)
                {
                    qvOutput[i] = qvInput[i];
                }
            }

            return qvOutput;
        }


        public double[] FilterProcess(double[] qvInput)
        {
            double[] qvOutput = new double[qvInput.Length];
            if (bIsEnable)
            {
                for (int i = 0; i < qvInput.Length; i++)
                {
                    qvOutput[i] = qvInput[i] - process(qvInput[i]);
                }
            }
            else
            {
                for (int i = 0; i < qvInput.Length; i++)
                {
                    qvOutput[i] = qvInput[i];
                }
            }

            return qvOutput;
        }

        double process(double inVal)
        {
            qvBuffer[Cnt] = inVal;
            dSumm += qvBuffer[Cnt];
            Cnt = (Cnt + 1) % (lenght);
            dSumm -= qvBuffer[Cnt];

            return dSumm / lenght;
        }
    }
}
