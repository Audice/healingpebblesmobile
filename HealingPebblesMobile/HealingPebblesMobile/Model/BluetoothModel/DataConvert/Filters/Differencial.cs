﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HealingPebblesMobile.Model.BluetoothPart.DataConvert.Filters
{
    public class Differencial
    {
        short sLastValue;
        int iLastvalue;
        public Differencial()
        {
            sLastValue = 0;
            iLastvalue = 0;
        }

        public short[] Process(short[] qvInput)
        {
            short[] qvOutput = new short[qvInput.Length];
            int iLenght = qvInput.Length;
            qvOutput[0] = (short)(qvInput[0] - sLastValue);
            for (int i = 1; i < iLenght; i++)
            {
                qvOutput[i] = (short)(qvInput[i] - qvInput[i - 1]);
            }
            sLastValue = qvInput[iLenght - 1];
            return qvOutput;
        }


        public void Clear()
        {
            sLastValue = 0;
            iLastvalue = 0;
        }
    }
}
