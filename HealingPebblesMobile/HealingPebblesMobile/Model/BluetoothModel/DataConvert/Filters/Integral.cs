﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HealingPebblesMobile.Model.BluetoothPart.DataConvert.SignalsFilters
{
    public class Integral
    {
        double dLastValue;
        int iLastvalue;
        public Integral()
        {
            dLastValue = 0;
            iLastvalue = 0;
        }
        public double[] Process(double[] qvInput)
        {
            double[] qvOutput = new double[qvInput.Length];
            int iLenght = qvInput.Length;
            qvOutput[0] = qvInput[0] + dLastValue;
            for (int i = 1; i < iLenght; i++)
            {
                qvOutput[i] = qvInput[i] + qvOutput[i - 1];
            }
            dLastValue = qvOutput[iLenght - 1];
            return qvOutput;
        }

        public double[] Process(short[] qvInput)
        {
            double[] qvOutput = new double[qvInput.Length];
            int iLenght = qvInput.Length;
            qvOutput[0] = qvInput[0] + dLastValue;

            for (int i = 1; i < iLenght; i++)
            {
                qvOutput[i] = qvInput[i] + qvOutput[i - 1];
            }

            dLastValue = qvOutput[iLenght - 1];

            return qvOutput;
        }


        public void Clear()
        {
            dLastValue = 0;
            iLastvalue = 0;
        }
    }
}
