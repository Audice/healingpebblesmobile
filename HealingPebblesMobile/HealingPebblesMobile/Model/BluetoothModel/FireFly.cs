﻿using HealingPebblesMobile.Model.BluetoothPart.CustomEventArgsClasses;
using HealingPebblesMobile.Model.ServerPart;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Xml;
using System.Xml.Linq;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace HealingPebblesMobile.Model.BluetoothPart
{
    /// <summary>
    /// Объект, описывающий мобильный кардиограф
    /// </summary>
    public class FireFly
    {
        ServerWorker ServerWorker;

        private static FireFly BluetoothWorker;
        /// <summary>
        /// Объект обеспечения коннекта с кардиографом
        /// </summary>
        public BluetoothConnectServicer BluetoothConnectController
        {
            get; private set;
        }

        private RawDataWorker RawDataWorker
        {
            get; set;
        }

        public string DevicesName
        {
            get; private set;
        } = "HealingPebbles";

        public int CharageLevel
        {
            get; private set;
        } = -1;


        public event EventHandler<EventArgs> NoDataCameEvent;
        protected virtual void NoDataCame(EventArgs e)
        {
            EventHandler<EventArgs> handler = NoDataCameEvent;
            if (handler != null)
            {
                handler(this, e);
            }
        }

        public event EventHandler<FireFlyPackageEventArgs> PackageCameEvent;
        protected virtual void OnPackageCame(FireFlyPackageEventArgs e)
        {
            EventHandler<FireFlyPackageEventArgs> handler = PackageCameEvent;
            if (handler != null)
            {
                handler(this, e);
            }
        }

        protected FireFly()
        {
            this.ServerWorker = ServerWorker.getInstance();
            BluetoothConnectController = new BluetoothConnectServicer("HealingPebbles", GetDevicesList()[0]);
            RawDataWorker = new RawDataWorker(this.BluetoothConnectController);
            RawDataWorker.FireFlyPackageReadyEvent += PackageCameHandler;
        }

        private void PackageCameHandler(object sender, FireFlyPackageEventArgs e)
        {
            //string res = (this.ServerWorker.SendPackage(this.ServerWorker.CovertData(e.Package.GetSignal()))).GetAwaiter().GetResult();
            this.OnPackageCame(new FireFlyPackageEventArgs() { Package = e.Package });
        }

        private void NoDataHandler(object sender, EventArgs e)
        {
            this.NoDataCame(EventArgs.Empty);
        }

        private void BattStateChangeAction(object sender, BatteryEventArgs e)
        {
            this.CharageLevel = e.ChargeLevel;
        }

        public static FireFly getInstance()
        {
            if (BluetoothWorker == null)
            {
                BluetoothWorker = new FireFly();
            }
            return BluetoothWorker;
        }


        public List<string> GetDevicesList()
        {
            List<string> resultsString = new List<string>();
            if (DependencyService.Get<IBluetoothController>().CheckBluetoothModule())
            {
                var devicesList = DependencyService.Get<IBluetoothController>().GetListDevices(this.DevicesName);
                for (int i=0; i < devicesList.Count; i++)
                {
                    resultsString.Add(devicesList[i].Item2);
                }
            }
            return resultsString;
        }




    }
}
