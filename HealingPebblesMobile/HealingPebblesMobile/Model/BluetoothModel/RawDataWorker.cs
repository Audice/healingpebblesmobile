﻿using HealingPebblesMobile.Model.BluetoothPart.CustomEventArgsClasses;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HealingPebblesMobile.Model.BluetoothPart
{
    public class RawDataWorker
    {
        public bool IsConvertionProcessRun
        {
            get; private set;
        } = false;

        public bool IsConvertionTaskRun
        {
            get; private set;
        } = false;

        private ConcurrentQueue<byte> RawDataQueue;


        //Загаловки пакетов
        private readonly ushort MinimumPackageByte = 1100;
        private readonly byte[] EcgDataTemplate = new byte[] { 69, 67, 71, 32, 68, 65, 84, 65, 34 };
        private readonly byte[] EndFileTemplate = new byte[] { 34, 13, 10 };
        private readonly byte[] EndTemplate = new byte[] { 34, 13, 10 };
        private readonly byte[] EndBattTemplate = new byte[] { 13, 10 };
        private readonly byte[] BatteryTemplate = new byte[] { 66, 65, 84, 84, 58 };

        private readonly byte[] OKTemplate = new byte[] { 79, 75, 13, 10 };

        private readonly byte[] SD_PresentTemplate = new byte[] { 83, 68, 58, 32 };
        private readonly byte[] NoDataTemplate = new byte[] { 78, 79, 32, 68, 65, 84, 65, 13, 10 };

        private readonly byte[] File_ChannelsTemplate = new byte[] { 70, 105, 108, 101, 32, 67, 72, 65, 78, 69, 76, 83, 58 };
        private readonly byte[] EndDirInfoTemplate = new byte[] { 13, 10 };
        private readonly ushort DirInfoMaxSize = 95;
        private readonly byte NumDirInfosStroke = 7;

        private ushort RemovableBorder = 18;
        private ushort MaxRemovableBorder = 9;
        private byte PackageHeaderSize = 10;
        private byte BatteryResponseSize = 4;


        public RawDataWorker(BluetoothConnectServicer bluetoothConnectServicer)
        {
            RawDataQueue = new ConcurrentQueue<byte>();
            bluetoothConnectServicer.RawDataReadyEvent += RawDataCame;
            bluetoothConnectServicer.ChangeBluetoothStateEvent += ParseProcessController;
            RemovableBorder = (ushort)(EcgDataTemplate.Length * 2);
            this.MaxRemovableBorder = (ushort)(EcgDataTemplate.Length);
        }


        public event EventHandler<FireFlyPackageEventArgs> FireFlyPackageReadyEvent;
        protected virtual void OnFireFlyPackageReady(FireFlyPackageEventArgs e)
        {
            EventHandler<FireFlyPackageEventArgs> handler = FireFlyPackageReadyEvent;
            if (handler != null)
            {
                handler(this, e);
            }
        }



        public event EventHandler<EventArgs> NoDataEvent;
        protected virtual void OnNoDataReady(EventArgs e)
        {
            EventHandler<EventArgs> handler = NoDataEvent;
            if (handler != null)
            {
                handler(this, e);
            }
        }


        private void RawDataCame(object sender, DataReadyEventArgs e)
        {
            BluetoothConnectServicer bluetoothConnectServicer = sender as BluetoothConnectServicer;
            if (bluetoothConnectServicer.IsBluetoothConnect)
            {
                byte[] data = e.RawData;
                for (int i=0; i < data.Length; i++)
                    RawDataQueue.Enqueue(data[i]);
            }
        }

        /// <summary>
        /// Метод поиска содержательных пакетов в потоке байт. Task позволит не тормозить процесс заполнения контейнера данных
        /// </summary>
        /// <returns></returns>
        private Task ParsePackege()  // делать иначе
        {
            return Task.Run(() =>
            {
                List<byte> responseList = new List<byte>();
                while (this.IsConvertionProcessRun) // Условие не самое логичное
                {
                    if (this.RawDataQueue.Count > 0)
                    {
                        byte value = 0;
                        if (this.RawDataQueue.TryDequeue(out value))
                        {
                            responseList.Add(value);

                            //поиска загаловка ЭКГ
                            int startIndexECGTemplate = SearchTemplate(responseList, this.EcgDataTemplate);
                            ECGPackagePreparation(startIndexECGTemplate, responseList);

                            //Поиск заряда батареи
                            int startIndexBattTemplate = SearchTemplate(responseList, this.BatteryTemplate);
                            BatteryPackagePreparation(startIndexBattTemplate, responseList);

                            //Освобождение неиспользуемой памяти. Реализовано для 2 команд
                            if (startIndexBattTemplate < 0 && startIndexECGTemplate > 0)
                            {
                                responseList.RemoveRange(0, startIndexECGTemplate);
                            }
                        }
                    }
                }
            });
        }

        private void ECGPackagePreparation(int startTemplateIndex, List<byte> rawECGData)
        {
            if (rawECGData == null) return; 
            if (startTemplateIndex >= 0 && (rawECGData.Count - startTemplateIndex) > this.MinimumPackageByte)
            {
                int startIndex = startTemplateIndex + this.EcgDataTemplate.Length;
                //Удаление старых данных
                //responseList.RemoveRange(startIndexECGTemplate, this.EcgDataTemplate.Length);

                List<byte> ecgPackage = new List<byte>();
                //Если мы нашли заголовог ECG DATA
                for (int i = 0; i < PackageHeaderSize; i++)
                {
                    ecgPackage.Add(rawECGData[startIndex + i]);
                }
                startIndex += PackageHeaderSize;

                short packageLength = BitConverter.ToInt16(new byte[] { ecgPackage[0], ecgPackage[1] }, 0);
                short samplingFrequency = BitConverter.ToInt16(new byte[] { ecgPackage[2], ecgPackage[3] }, 0);
                byte numOfBit = ecgPackage[4];
                short umv = BitConverter.ToInt16(new byte[] { ecgPackage[5], ecgPackage[6] }, 0);
                byte numOfChenal = ecgPackage[7];
                ushort measurementNumber = (ushort)(ecgPackage[8] + ecgPackage[9] * 256);
                //После получения заголовка, считываем данные
                if (packageLength <= 0)
                {
                    return; //Если длина пакета не валидна - выходим из цикла
                }
                int dataLength = 2 * samplingFrequency;
                ecgPackage.Clear();
                for (int i = 0; i < dataLength; i++)
                    ecgPackage.Add(rawECGData[startIndex + i]);

                startIndex += dataLength;

                byte[] ecgPackageData = ecgPackage.ToArray();
                ecgPackage.Clear();
                //Проверка конца пакета на корректность
                for (int i = 0; i < EndFileTemplate.Length; i++)
                {
                    ecgPackage.Add(rawECGData[startIndex + i]);
                }
                startIndex += EndFileTemplate.Length;

                if (ecgPackage[0] == this.EndFileTemplate[0]
                && ecgPackage[1] == this.EndFileTemplate[1]
                && ecgPackage[2] == this.EndFileTemplate[2])
                {
                    //Отправляем пакет дальше
                    FireFlyPackageEventArgs args = new FireFlyPackageEventArgs();
                    args.Package = new FireFlyPackage(packageLength, samplingFrequency, numOfBit, umv, numOfChenal, measurementNumber, ecgPackageData);
                    this.OnFireFlyPackageReady(args);
                }

                ecgPackage.Clear();
                ecgPackage = null;
                //Удаление старых данных
                rawECGData.RemoveRange(startTemplateIndex, startIndex - startTemplateIndex);
            }
        }

        private void BatteryPackagePreparation(int startTemplateIndex, List<byte> rawBattData)
        {
            if (rawBattData == null) return;
            if (startTemplateIndex >= 0 && 
                (rawBattData.Count - startTemplateIndex) >= (this.BatteryResponseSize + this.BatteryTemplate.Length + this.EndBattTemplate.Length))
            {
                int startIndex = startTemplateIndex + BatteryTemplate.Length;
                //Если мы нашли заголовок BATT
                List<byte> batteryPackage = new List<byte>();
                for (int i = 0; i < this.BatteryResponseSize; i++)
                {
                    batteryPackage.Add(rawBattData[i + startIndex]);
                }
                startIndex += this.BatteryResponseSize + this.EndBattTemplate.Length;

                batteryPackage.Clear();
                batteryPackage = null;
                rawBattData.RemoveRange(startTemplateIndex, startIndex - startTemplateIndex);
            }
        }

        private int SearchTemplate(List<byte> responseList, byte[] ecgDataTemplate)
        {
            if (responseList.Count >= ecgDataTemplate.Length)
            {
                for (int i=0; i <= responseList.Count - ecgDataTemplate.Length; i++)
                {
                    bool isSearched = true;
                    for (int j=0; j < ecgDataTemplate.Length; j++)
                    {
                        if (responseList[i + j] != ecgDataTemplate[j])
                        {
                            isSearched = false;
                            break;
                        }
                    }
                    if (isSearched)
                    {
                        return i;
                    }
                }
            }
            return -1;
        }

        private async void ParseProcessController(object sender, EventArgs e)
        {
            BluetoothConnectServicer bluetoothConnectServicer = sender as BluetoothConnectServicer;
            bool StreamsReady = bluetoothConnectServicer.IsBluetoothConnect;
            if (StreamsReady && !this.IsConvertionProcessRun)
            {
                this.IsConvertionProcessRun = StreamsReady;
                byte clearedObject = 0;
                //Запуск задачи обработки данных
                while (RawDataQueue.TryDequeue(out clearedObject))
                {
                    // do nothing
                    //RawDataQueue.Clear();
                }

                await this.ParsePackege();
            }
            else
            {
                if (!StreamsReady) this.IsConvertionProcessRun = StreamsReady;
            }
        }
    }
}
