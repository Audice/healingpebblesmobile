﻿using HealingPebblesMobile.Model.BluetoothPart.CustomEventArgsClasses;
using System;
using System.Collections.Generic;
using System.Text;

namespace HealingPebblesMobile.Model.BluetoothPart.PackageControl
{
    public interface IPackageSequenceControler
    {
        EventHandler<RawDataEventArgs> PartRawDataReadyEvent { get; set; }
    }
}
