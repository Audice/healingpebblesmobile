﻿using HealingPebblesMobile.Model.ConclusionPart.SegmentationPart.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HealingPebblesMobile.Model.ConclusionPart.SegmentationPart
{
    class EcgDenoise: fwt
    {
        private string FiltersDir;             //filters dir
        private double[] pEcgData;            //pointer to [original sig]
        private double[] pTmpData;           //[SRadd][original sig][SRadd]

        public EcgDenoise()
        {
            pTmpData = null;
        }

        public void InitDenoise(string fltdir, double[] data, int size, double sr, bool mirror = true)
        {
            FiltersDir = fltdir;
            pEcgData = data;
            SR = sr;
            Length = (uint)size;

            if (pTmpData != null) pTmpData = null;
        
            pTmpData = new double[Length + (int)(2 * SR)];    // [SR add] [sig] [SR add]

            for (int i = 0; i < Length; i++)           //signal
                pTmpData[i+(int)SR] = pEcgData[i];

            if (Length < SR) mirror = false;

            if (mirror) { //mirror extension of signal
                for (int i = 0; i < (int)SR; i++)          //left side
                        pTmpData[i] = pEcgData[(int)SR-i];
                for (int i = (int)Length + (int)SR; i < Length + 2*SR; i++)         //right side
                        pTmpData[i] = pEcgData[(Length-2) - (i-(Length+(int)SR))];
            } else {
                for (int i = 0; i < (int)SR; i++)          //left side
                        pTmpData[i] = pEcgData[0];
                for (int i = (int)Length + (int)SR; i < Length + 2 * SR; i++)         //right side
                        pTmpData[i] = pEcgData[Length-1];
            }
        }

        public void CloseDenoise()
        {
        
            if (pTmpData != null) {
                pTmpData = null;        
            }
        }

        public bool  LFDenoise()
        {
            string filter = "";
            //get base line J///////
            int J = (int)Math.Ceiling(Math.Log(SR / 0.8, 2)) - 1;
            filter = FiltersDir;
            filter += "\\daub2.flt";
            if (InitFWT(new daub2(), pTmpData, (int)((int)Length + (int)2*SR)) == false)
                return false;

            //transform////////////////////////
            FwtTrans(J);
            ///////////////////////////////////


        
            int[] Jnumbs = GetJnumbs(J, (int)((int)Length + (int)2*SR));
        
            double[] lo = GetFwtSpectrum();
        
            for (int i = 0; i < Jnumbs[0]; i++)
                lo[i] = 0.0;

            //synth
            FwtSynth(J);

            for (int i = 0; i < Length; i++)
                pEcgData[i] = lo[i+(int)SR];

            CloseFWT();
        
            return true;
        }

        public bool HFDenoise()
        {
            string filter = "";
            //get HF scale J///////
            int J = (int)Math.Ceiling(Math.Log(SR / 23.0, 2)) - 2;     //[30Hz - ...] hf denoising
            filter = FiltersDir;
            filter += "\\bior97.flt";
            if (InitFWT(new bior97(), pTmpData, (int)(Length + 2*SR)) == false)
                return false;

            //transform
            FwtTrans(J);

            int[] Jnumbs = GetJnumbs(J, (int)(Length + 2*SR));
        
            int hinum=0, lonum=0;
        
            HiLoNumbs(J, (int)(Length + 2*SR), ref hinum, ref lonum);
        
            double[] lo = GetFwtSpectrum();

            double[] hi = new double[(int)(Length + 2 * SR)];
            int HiIndex = ((int)(Length + 2 * SR) - hinum);
            for (int i = 0; i < (lo.Length - HiIndex); i++)
            {
                hi[i] = lo[HiIndex + i];
            }



            int window =0;   //3sec window
            for (int j = J; j > 0; j--) {
                window = (int)(3.0 * SR / Math.Pow(2.0, (double)j));
                Denoise(ref hi, Jnumbs[J-j], window);
                setNewPart(ref lo, ref hi, HiIndex, HiIndex + Jnumbs[J - j], lo.Length); // Обязательно проверь!!!
                HiIndex += Jnumbs[J-j];
            }

            //synth
            FwtSynth(J);
     
            for (int i = 0; i < Length; i++)
                pEcgData[i] = lo[i+(int)SR];

            CloseFWT();
            return true;
        }

        private void setNewPart(ref double[] lo, ref double[] hi, int oldIndex, int startIndex, int endIndex)
        {
            for (int i = 0; i < endIndex - oldIndex; i++)
            {
                lo[oldIndex + i] = hi[i];
            }
            hi = new double[endIndex - startIndex];
            for (int i = 0; i < endIndex - startIndex; i++)
            {
                hi[i] = lo[startIndex + i];
            }
        }

        public bool LFHFDenoise()
        {
            string filter = "";
            int J;
            int[] Jnumbs;
            double[] lo;
            double[] hi;

            //get base line J///////
            J = (int)Math.Ceiling(Math.Log(SR / 0.8, 2)) - 1;
            filter = FiltersDir;
        
            //filter += "\\daub2.flt";
        
            if (InitFWT(new daub2(), pTmpData, (int)((int)Length + (int)(2*SR))) == false)
                return false;

            //transform
            FwtTrans(J);

            Jnumbs = GetJnumbs(J, (int)((int)Length + (int)(2 * SR)));
            lo = GetFwtSpectrum();
            for (int i = 0; i < Jnumbs[0]; i++)
                lo[i] = 0.0;

        
            //synth
            FwtSynth(J);

            for (int i = 0; i < Length + 2*SR; i++)
                pTmpData[i] = lo[i];

        
            ////////get min max///////////////////

            double min=0.0, max=0.0;
            double[] tmpLo = new double[lo.Length - (int)SR];
            MinMax(tmpLo, (int)Length, ref min, ref max);

            CloseFWT();
            //get HF scale J///////
        
            J = (int)Math.Ceiling(Math.Log(SR / 23.0, 2)) - 2;     //[30Hz - ...] hf denoising

            filter = FiltersDir;
        
            filter += "\\bior97.flt";
        
            if (InitFWT(new bior97(), pTmpData, (int)((int)Length + (int)(2*SR))) == false)
                return false;

            //transform
            FwtTrans(J);

        
            Jnumbs = GetJnumbs(J, (int)((int)Length + (int)(2*SR)));
            int hinum = 0, lonum = 0;
            HiLoNumbs(J, (int)((int)Length + (int)(2*SR)), ref hinum, ref lonum);

            lo = GetFwtSpectrum();

            hi = new double[(int)(Length + 2 * SR)];
            int HiIndex = ((int)(Length + 2 * SR) - hinum);
            for (int i = 0; i < (lo.Length - HiIndex); i++)
            {
                hi[i] = lo[HiIndex + i];
            }



            int window = 0;   //3sec window
            for (int j = J; j > 0; j--)
            {
                window = (int)(3.0 * SR / Math.Pow(2.0, (double)j));
                Denoise(ref hi, Jnumbs[J - j], window);
                setNewPart(ref lo, ref hi, HiIndex, HiIndex + Jnumbs[J - j], lo.Length); // Обязательно проверь!!!
                HiIndex += Jnumbs[J - j];
            }

            //synth
            FwtSynth(J);

            for (int i = 0; i < Length; i++)
                pEcgData[i] = lo[i+(int)SR];

            //renormalize
            nMinMax(ref pEcgData, (int)Length, ref  min, ref max);
            CloseFWT();
            return true;
        }
    }
}
