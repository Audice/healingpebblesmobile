﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using HealingPebblesMobile.Model.ConclusionPart.SegmentationPart.Filters;

namespace HealingPebblesMobile.Model.ConclusionPart.SegmentationPart
{

    class fwt : signal
    {
        private struct FWThdr
        {
            public char[] hdr; //4 элемента 
            public uint size;
            public float sr;
            public Byte bits;
            public Byte lead;
            public ushort umv;
            public char[] wlet; //[8]
            public ushort J;
            public char[] rsrv; //[14];
        };

        //filters inits
        StreamReader filter;
        private  FWThdr phdr;
        private string FilterName;
        private double[] tH;
        private double[] tG;     //analysis filters
        private double[] H;
        private double[] G;       //synth filters
        private int thL, tgL, hL, gL;     //filters lenghts
        private int thZ, tgZ, hZ, gZ;     //filter centers
        private int J;                //scales
        private int[] Jnumbs;          //hi values per scale
        private int SignalSize;       //signal size
        private int LoBandSize;       //divided signal size
        //spectra
        private double[] pFwtSpectrum;              //buffer with fwt spectra
        private double[] pTmpSpectrum;              //temporary
        private double[] pHiData;
        private int indexOfHiData;
        private double[] pLoData;         //pointers to pTmpSpectrum
        private int HiNum, LoNum;                       //number of lo and hi coeffs

        public fwt()
        {
            indexOfHiData = 0;
            phdr = new FWThdr();
            phdr.hdr = new char[4];
            phdr.wlet = new char[8];
            phdr.rsrv = new char[14];
            tH = new double[1];
            tG = new double[1];
            H = new double[1];
            G = new double[1];
            thL = 0; tgL = 0; hL = 0; gL = 0;
            thZ = 0; tgZ = 0; hZ = 0; gZ = 0;
            pFwtSpectrum = new double[1];
            pTmpSpectrum = new double[1];
            J = 0;
            Jnumbs = new int[1];
        }


        public void CloseFWT()
        {
            if (tH != null) {
                tH = null;
            }
            if (tG != null) {
                tG = null;
            }
            if (H != null) {
                H = null;
            }
            if (G != null) {
                G = null;
            }
            if (pFwtSpectrum != null) {
                pFwtSpectrum = null;
            }
            if (pTmpSpectrum != null) {
                pTmpSpectrum =  null;
            }
            if (Jnumbs != null) {
                Jnumbs = null;
            }
        }

        //Getters

        public double[] GetFwtSpectrum()
        {
            return pFwtSpectrum;
        }
        public int GetLoBandSize()
        {
            return LoBandSize;
        }
        public int GetJ()
        {   
            return J;
        }

        public bool InitFWT(IFilter filter, double[] data, int size)
        {
            //filter = new StreamReader(fltname);  //Chenge fltname in relise

            if (filter != null) {
                tH = filter.Load_TH_Filter(ref thL, ref thZ);
                tG = filter.Load_TG_Filter(ref tgL, ref tgZ);
                H = filter.Load_H_Filter(ref hL, ref hZ);
                G = filter.Load_G_Filter(ref gL, ref gZ);
                filter = null;

                LoBandSize = size;
                SignalSize = size;
                pFwtSpectrum = new double[size];
                pTmpSpectrum = new double[size];
                pLoData = pTmpSpectrum;
                pHiData = pTmpSpectrum;//pTmpSpectrum + size;   попробуем заменить работу с указателем на простое объявление
                indexOfHiData = pTmpSpectrum.Length;

                for (int i = 0; i < size; i++)
                {
                    pFwtSpectrum[i] = data[i];
                    pTmpSpectrum[i] = 0;
                }

                J = 0;

                return true;
            } else
                return false;
        }

        private double[] LoadFilter(ref int L,ref int Z, StreamReader sr)
        {
            string line = sr.ReadLine();
            string tmp = "";
            while (line.Equals(""))
            {
                line = sr.ReadLine();
            }
            int i = 0;
            for (i = 0; i < line.Length; i++)
            {
                if (line[i] == ' ') break;
                tmp += line[i];
            }
            L = System.Convert.ToInt32(tmp);
            tmp = "";
            i++;
            for (; i < line.Length; i++)
            {
                if (line[i] == ' ') break;
                tmp += line[i];
            }
            Z = System.Convert.ToInt32(tmp);

            double[] flt = new double[L];

            for (int j = 0; j < L; j++)
            {
                line = sr.ReadLine();
                line = line.Replace('.', ',');
                flt[j] = double.Parse(line.ToString()); // заменяй точки на запятые
            }
            return flt;
        }

        private void HiLoTrans()
        {
            int n;
            double s, d;
            for (int k = 0; k < LoBandSize / 2; k++) {
                s = 0;
                d = 0;

                for (int m = -thZ; m < thL - thZ; m++) {   
                    n = 2 * k + m;
                    if (n < 0) n = 0 - n; 
                    if (n >= LoBandSize) n -= (2 + n - LoBandSize);
                    s += tH[m+thZ] * pFwtSpectrum[n];
                }

                for (int m = -tgZ; m < tgL - tgZ; m++) {                        
                    n = 2 * k + m;     
                    if (n < 0) n = 0 - n;   
                    if (n >= LoBandSize) n -= (2 + n - LoBandSize);    
                    d += tG[m+tgZ] * pFwtSpectrum[n];
                }
                pLoData[k] = s;
                pHiData[k + indexOfHiData] = d;
            }

            
        for (int i = 0; i < SignalSize; i++)
                pFwtSpectrum[i] = pTmpSpectrum[i];

        }

        public void FwtTrans(int scales)
        {
            for (int j = 0; j < scales; j++) {
                //pHiData -= LoBandSize / 2; // Указатель сместился с конца на половину
                indexOfHiData -= LoBandSize / 2;
                HiLoTrans();

                LoBandSize /= 2;
                J++;
            }
        }

        private void HiLoSynth()
        {
            int n;
            double s2k, s2k1;

            for (int i = 0; i < SignalSize; i++)
                pTmpSpectrum[i] = pFwtSpectrum[i]; 

            for (int k = 0; k < LoBandSize; k++) {
                s2k = 0;
                s2k1 = 0;

                for (int m = -hZ; m < hL - hZ; m++) {       //s2k and s2k1 for H[]
                        n = k - m;
                        if (n < 0) n = 0 - n;
                        if (n >= LoBandSize) n -= (2 + n - LoBandSize);

                        if (2*m >= -hZ && 2*m < hL - hZ)
                                s2k += H[(2*m)+hZ] * pLoData[n];
                        if ((2*m + 1) >= -hZ && (2*m + 1) < hL - hZ)
                                s2k1 += H[(2*m+1)+hZ] * pLoData[n];
                }

                for (int m = -gZ; m < gL - gZ; m++) {      //s2k and s2k1 for G[]
                        n = k - m;
                        if (n < 0) n = 0 - n;
                        if (n >= LoBandSize) n -= (2 + n - LoBandSize);

                        // indexOfHiData - это альтернатива указателю на конкретный
                        // участок памяти соответствующий конкретному элементу массива pTmpSpectrum.
                        // Так можно отследить положение указателя в массиве
 
                        if (2*m >= -gZ && 2*m < gL - gZ)
                                s2k += G[(2*m)+gZ] * pHiData[n + indexOfHiData];
                        if ((2*m + 1) >= -gZ && (2*m + 1) < gL - gZ)
                            s2k1 += G[(2 * m + 1) + gZ] * pHiData[n + indexOfHiData];
                }

                pFwtSpectrum[2*k] = 2.0 * s2k;
                pFwtSpectrum[2*k+1] = 2.0 * s2k1;
            }
        }

        public void FwtSynth(int scales)
        {        
            for (int j = 0; j < scales; j++) {
                HiLoSynth();
                indexOfHiData += Jnumbs[j]; //альтернатива pHiData += Jnumbs[j];
                LoBandSize *= 2;
                J--;        
            }
        }
////////////////////////////////////////////////////////////////////////////////////////////////



        public int[] GetJnumbs(int j, int size)
        {
            if (Jnumbs != null) Jnumbs = null; ;

            Jnumbs = new int[j];

            for (int i = 0; i < j; i++)
                Jnumbs[i] = size / (int)Math.Pow(2, (double)(j - i));

            return Jnumbs;
        }


        public void HiLoNumbs(int j, int size, ref int hinum, ref int lonum)
        {
            lonum = 0;
            hinum = 0;
            for (int i = 0; i < j; i++) {
                size /= 2;
                hinum += size;
            }
            lonum = size;
        }

        // Данные методы невозможно протестировать текущим набором данных. Вернёмся к ним позже
        /*

                bool FWT::FwtSaveFile(const wchar_t *name, const double *hipass, const double *lopass, PFWTHDR hdr)
        {
                int filesize;
                short tmp;

                HiLoNumbs(hdr->J, hdr->size, HiNum, LoNum);

                switch (hdr->bits) {
                case 12:
                        if ((HiNum + LoNum) % 2 != 0)
                                filesize = int((double)((HiNum + LoNum) + 1) * 1.5);
                        else
                                filesize = int((double)(HiNum + LoNum) * 1.5);
                        break;
                case 16:
                        filesize = (HiNum + LoNum) * 2;
                        break;
                case 0:
                case 32:
                        filesize = (HiNum + LoNum) * 4;
                        break;
                }

                fp = CreateFileW(name, GENERIC_WRITE | GENERIC_READ, 0, 0, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, 0);
                if (fp == INVALID_HANDLE_VALUE) {
                        fp = 0;
                        return false;
                }
                fpmap = CreateFileMapping(fp, 0, PAGE_READWRITE, 0, filesize + sizeof(FWTHDR), 0);
                lpMap = MapViewOfFile(fpmap, FILE_MAP_WRITE, 0, 0, filesize + sizeof(FWTHDR));

                lpf = (float *)lpMap;
                lps = (short *)lpMap;
                lpc = (char *)lpMap;

                memset(lpMap, 0, filesize + sizeof(FWTHDR));
                memcpy(lpMap, hdr, sizeof(FWTHDR));

                lpf += sizeof(FWTHDR) / sizeof(float);
                lps += sizeof(FWTHDR) / sizeof(short);
                lpc += sizeof(FWTHDR);

                for (int i = 0; i < HiNum + LoNum; i++) {
                        if (i < HiNum)
                                tmp = short(hipass[i] * (double)hdr->umv);
                        else
                                tmp = short(lopass[i-HiNum] * (double)hdr->umv);

                        switch (hdr->bits) {
                        case 12:
                                if (i % 2 == 0) {
                                        lpc[0] = LOBYTE(tmp);
                                        lpc[1] = 0;
                                        lpc[1] = HIBYTE(tmp) & 0x0f;
                                } else {
                                        lpc[2] = LOBYTE(tmp);
                                        lpc[1] |= HIBYTE(tmp) << 4;
                                        lpc += 3;
                                }
                                break;

                        case 16:                                               //16format
                                *lps++ = tmp;
                                break;

                        case 0:
                        case 32:
                                if (i < HiNum)                                        //32bit float
                                        *lpf++ = (float)hipass[i];
                                else
                                        *lpf++ = (float)lopass[i-HiNum];
                                break;
                        }
                }

                CloseFile();
                return true;
        }


        bool FWT::FwtReadFile(const wchar_t *name, const char *appdir)
        {
                short tmp;

                fp = CreateFileW(name, GENERIC_WRITE | GENERIC_READ, 0, 0, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, 0);
                if (fp == INVALID_HANDLE_VALUE) {
                        fp = 0;
                        return false;
                }
                fpmap = CreateFileMapping(fp, 0, PAGE_READWRITE, 0, 0, 0);
                lpMap = MapViewOfFile(fpmap, FILE_MAP_WRITE, 0, 0, 0);

                phdr = (PFWTHDR)lpMap;
                lpf = (float *)lpMap;
                lps = (short *)lpMap;
                lpc = (char *)lpMap;

                Length = phdr->size;
                SR = phdr->sr;
                Bits = phdr->bits;
                Lead = phdr->lead;
                UmV = phdr->umv;
                strcpy(FilterName, phdr->wlet);
                J = phdr->J;


                lpf += sizeof(FWTHDR) / sizeof(float);
                lps += sizeof(FWTHDR) / sizeof(short);
                lpc += sizeof(FWTHDR);

                HiLoNumbs(J, Length, HiNum, LoNum);
                SignalSize = LoNum + HiNum;
                LoBandSize = LoNum;

                pFwtSpectrum = new double[SignalSize];
                pTmpSpectrum = new double[SignalSize];
                memset(pTmpSpectrum, 0, sizeof(double)*SignalSize);
                pLoData = pFwtSpectrum;
                pHiData = pFwtSpectrum + LoNum;

                for (int i = 0; i < SignalSize; i++) {
                        switch (Bits) {
                        case 12:                                             //212 format   12bit
                                if (i % 2 == 0) {
                                        tmp = MAKEWORD(lpc[0], lpc[1] & 0x0f);
                                        if (tmp > 0x7ff)
                                                tmp |= 0xf000;
                                } else {
                                        tmp = MAKEWORD(lpc[2], (lpc[1] & 0xf0) >> 4);
                                        if (tmp > 0x7ff)
                                                tmp |= 0xf000;
                                        lpc += 3;
                                }

                                if (i < HiNum) 
                                        pHiData[i] = (double)tmp / (double)UmV;
                                else 
                                        pLoData[i-HiNum] = (double)tmp / (double)UmV;                        
                                break;

                        case 16:                                             //16format
                                if (i < HiNum) 
                                        pHiData[i] = (double)lps[i] / (double)UmV;
                                else 
                                        pLoData[i-HiNum] = (double)lps[i] / (double)UmV;                        
                                break;

                        case 0:
                        case 32:                                             //32bit float
                                if (i < HiNum) 
                                        pHiData[i] = (double)lpf[i];
                                else 
                                        pLoData[i-HiNum] = (double)lpf[i];                        
                                break;
                        }

                }

                pLoData = pTmpSpectrum;
                pHiData = pTmpSpectrum + LoNum;


                if (appdir) { //load filter for synthesis
                        char flt[256];
                        strcpy(flt, appdir);
                        strcat(flt, "\\filters\\");
                        strcat(flt, FilterName);
                        strcat(flt, ".flt");

                        filter = fopen(flt, "rt");
                        printf(flt);
                        if (filter) {
                                tH = LoadFilter(thL, thZ);
                                tG = LoadFilter(tgL, tgZ);
                                H = LoadFilter(hL, hZ);
                                G = LoadFilter(gL, gZ);
                                fclose(filter);
                        } else {
                                CloseFile();
                                return false;
                        }
                }


                CloseFile();
                return true;
        }

        */


    }
}
