﻿using HealingPebblesMobile.Model.ConclusionPart.SegmentationPart.Filters;
using System;
using System.Collections.Generic;
using System.Linq;

namespace HealingPebblesMobile.Model.ConclusionPart.SegmentationPart
{

    class EcgAnnotation: signal
    {
        public static string[] anncodes = new string[51]{
            "notQRS",   
            "N",        //1
            "LBBB",
            "RBBB",
            "ABERR",
            "PVC",
            "FUSION",
            "NPC",
            "APC",
            "SVPB",
            "VESC",
            "NESC",
            "PACE",
            "UNKNOWN",
            "NOISE",
            "q", //15
            "ARFCT",
            "Q", //17
            "STCH",
            "TCH",
            "SYSTOLE",
            "DIASTOLE",
            "NOTE",
            "MEASURE",
            "P", //24
            "BBB",
            "PACESP",
            "T", // 27
            "RTM",
            "U",
            "LEARN",
            "FLWAV",
            "VFON",
            "VFOFF",
            "AESC",
            "SVESC",
            "LINK",
            "NAPC",
            "PFUSE",
            "(", //39
            ")", //40
            "RONT",
            "(p", // 42
            "p)", //43
            "(t", //44
            "t)", // 45
            "ECT", //46
            "r", //47
            "R", //48
            "s", //49
            "S" // 50
        };

        public struct AnnRec 
        {
            public uint pos;    //offset
            public uint type;   //beat type
            public uint aux;    //index to aux array
        };

        public struct Annhdr
        {
            public int minbpm;
            public int maxbpm;
            public double minUmV;  //min R,S amplitude
            public double minQRS;  //min QRS duration
            public double maxQRS;  //max QRS duration
            public double qrsFreq; //qrs filtration freq 13Hz default
            public int ampQRS;     //amplify QRS complex
            public double minPQ;
            public double maxPQ;
            public double minQT;
            public double maxQT;
            public double pFreq;   //p wave freq for CWT
            public double tFreq;   //t wave freq for CWT
            public int biTwave;
            public bool isEmpty;   //Стартовое значение структуры - раньше не работало
        };

        public enum WAVETYPE { NORMAL, BIPHASE };
        public enum AMPLIFYQRS { INTER1, BIOR13 };
        private Annhdr ahdr;                    //annotation ECG params
        private int[,] ANN;
        private int annNum;              //annotation after getPTU (RR's + PT)
        private int[,] qrsANN;
        private int qrsNum;           //annotation after getQRS (RR's+MAnoise)  +ECT after getECT()
        private List<int> MA;                //MA noise // это вектор!!!
        private int auxNum;
        private char[,] AUX;                     //auxiliary ECG annotation data

        public EcgAnnotation(Annhdr p) // проверить!
        {
            qrsNum = 0;
            annNum = 0;
            auxNum = 0;
            ANN = null;
            AUX = null;
            qrsANN = null;
            MA = new List<int>();
            p.isEmpty = true;
            if (p.isEmpty)
            {
                ahdr.minbpm = 40;     //min bpm
                ahdr.maxbpm = 200;    //max bpm
                ahdr.minQRS = 0.04;   //min QRS duration
                ahdr.maxQRS = 0.2;    //max QRS duration
                ahdr.qrsFreq = 13.0;    //QRS filtration frequency
                ahdr.ampQRS = (int)AMPLIFYQRS.INTER1;   //inter1 filter
                ahdr.minUmV = 0.2;    //min UmV of R,S peaks
                ahdr.minPQ = 0.07;    //min PQ duration
                ahdr.maxPQ = 0.20;    //max PQ duration
                ahdr.minQT = 0.21;    //min QT duration
                ahdr.maxQT = 0.48;    //max QT duration
                ahdr.pFreq = 9.0;     //cwt Hz for P wave
                ahdr.tFreq = 3.0;     //cwt Hz for T wave
                ahdr.biTwave = 0;
                ahdr.isEmpty = false;
            }
            else
            {
                ahdr.minbpm = p.minbpm;     //min bpm
                ahdr.maxbpm = p.maxbpm;    //max bpm
                ahdr.minQRS = p.minQRS;   //min QRS duration
                ahdr.maxQRS = p.maxQRS;    //max QRS duration
                ahdr.minUmV = p.minUmV;    //min UmV of R,S peaks
                ahdr.minPQ = p.minPQ;    //min PQ duration
                ahdr.qrsFreq = p.qrsFreq;
                ahdr.ampQRS = p.ampQRS;   //inter1 filter   //min UmV of R,S peaks
                ahdr.maxPQ = p.maxPQ;    //max PQ duration
                ahdr.minQT = p.minQT;    //min QT duration
                ahdr.maxQT = p.maxQT;    //max QT duration
                ahdr.pFreq = p.pFreq;     //cwt Hz for P wave
                ahdr.tFreq = p.tFreq;     //cwt Hz for T wave
                ahdr.biTwave = p.biTwave;
                ahdr.isEmpty = false;
            }
        }


        public int GetQrsNumber(){
            return qrsNum;
        }

        public int GetEcgAnnotationSize(){
            return annNum;
        }
        public int[,] GetEcgAnnotation(){        
            return ANN;
        }
        public int[,] GetQrsAnnotation(){
            return qrsANN;
        }
        public char[,] GetAuxData(){
            return AUX;
        }
        public Annhdr GetAnnotationHeader(){
            return ahdr;
        }


        private bool IsNoise(double[] data, int window){
            for (int i = 0; i < window; i++)
                if (data[i] != 0)
                {
                    return true;
                }
            return false;
        }
        private void FindRS(double[] data, int size, ref int R, ref int S, double err)  //find RS or QR
        {
            double min= 0.0, max = 0.0;
            MinMax(data, size, ref min, ref max);
            R = -1;
            S = -1;
        
            if (!(max < 0.0 || max == data[0] || max == data[size-1] || max < err)) { //(fabs(max-data[0])<err && fabs(max-data[size-1])<err) ))
                for (int i = 1; i < size - 1; i++)
                        if (data[i] == max) {
                                R = i;
                                break;
                        }
            } 
        
            if (!(min > 0.0 || min == data[0] || min == data[size-1] || -min < err)) { //(fabs(min-data[0])<err && fabs(min-data[size-1])<err) ))
                for (int i = 1; i < size - 1; i++)
                        if (data[i] == min) {
                                S = i;
                                break;
                        }
            }
        }
        private int FindTmax(double[] data, int size)  //find T max/min peak position
        {
            double min=0.0, max=0.0;        
            MinMax(data, size, ref min, ref max);       
            int tmin = -1, tmax = -1;        
            for (int i = 0; i < size; i++) {
                if (data[i] == max) {
                        tmax = i;
                        break;
                }        
            }
            for (int i = 0; i < size; i++) {
                if (data[i] == min) {
                        tmin = i;
                        break;
                }
            }
            //max closest to the center
            if (tmin == -1 || tmax == -1) //??no max min found
                return -1;
        
            else {
                if (Math.Abs(tmax - (size / 2)) < Math.Abs(tmin - (size / 2)))
                        return tmax;
                else
                        return tmin;   
            }
        }
        private int Findr(double[] data, int size, double err = 0.0) //find small r in PQ-S
        {
            double tmp = 0.0, min = 0.0, max = 0.0;
            MinMax(data, size, ref min, ref max);
            if (max < 0.0 || max == data[0] || max == data[size-1] || Math.Abs(max - data[0]) < err) return -1;
            else tmp = max;        
            for (int i = 1; i < size - 1; i++) {
                if (data[i] == tmp)
                        return i;
            }
            return -1;
        }
        private int Findq(double[] data, int size, double err = 0.0)//find small q in PQ-R
        {
            double tmp = 0.0, min = 0.0, max = 0.0;
            MinMax(data, size, ref min, ref max);

            if (min > 0.0 || min == data[0] || min == data[size-1] || Math.Abs(min - data[0]) < err) return -1;
            else tmp = min;

            for (int i = 1; i < size - 1; i++) {
                if (data[i] == tmp)
                        return i;
            }
            return -1;
        }
        private int Finds(double[] data, int size, double err = 0.0)//find small s in R-Jpnt
        {
            double tmp = 0.0, min = 0.0, max = 0.0;
            MinMax(data, size, ref min, ref max);

            if (min > 0.0 || min == data[0] || min == data[size-1] || Math.Abs(min - data[size-1]) < err) return -1;
            else tmp = min;

            for (int i = 1; i < size - 1; i++) {
                if (data[i] == tmp)
                        return i;
            }
            return -1;
        }

        private bool Filter30hz(ref double[] data, int size, double sr, string fltdir)
        {
            cwt CWT = new cwt();
            cwt.WAVELET a = cwt.WAVELET.GAUS1;
            
            //CWT 10Hz transform
            CWT.InitCWT(size, a, 0, sr);        //gauss1 wavelet 6-index
        
            double[] pspec = CWT.CwtTrans(data, ahdr.qrsFreq);     //10-13 Hz transform?
        
            for (int i = 0; i < size; i++)
                data[i] = pspec[i];
        
            CWT.CloseCWT();
            //debug
            //ToTxt(L"10hz.txt",data,size);

            //string flt = "";
            //flt = fltdir;
            IFilter flt = null;
        
            switch (ahdr.ampQRS) {  //тут может ошибка выскочить
                case (int)AMPLIFYQRS.INTER1:
                    flt = new inter1();
                    break;
                case (int)AMPLIFYQRS.BIOR13: 
                    for (int i = 0; i < size; i++)  //ridges
                        data[i] *= (Math.Abs(data[i]) / 2.0);
                    flt = new bior13();
                        break;
            }

        
            fwt FWT = new fwt();
        ////////////FWT 0-30Hz removal//////////////////////////////////////////
            if (FWT.InitFWT(flt, data, size) == false)
                return false;

            int J = (int)Math.Ceiling(log2(sr / 23.0)) - 2;
        //trans///////////////////////////////////////////////////

            FWT.FwtTrans(J);


            int[] Jnumbs = FWT.GetJnumbs(J, size);
        
            int hinum = 0, lonum = 0;
            FWT.HiLoNumbs(J, size, ref hinum, ref lonum);

            double[] lo = FWT.GetFwtSpectrum();

            int HiIndex = size - hinum;
            double[] hi = new double[lo.Length - HiIndex]; // was 

            for (int jj = 0; jj < lo.Length - HiIndex; jj++)
            {
                hi[jj] = lo[HiIndex + jj];
            }

        
            int window=0;   //2.0sec window
        
            for (int j = J; j > 0; j--) {
                window = (int)((2.0 * sr) / Math.Pow(2.0, (double)j));    //2.0sec interval
                Denoise(ref hi, Jnumbs[J-j], window, 0, false);  //hard,MINIMAX denoise [30-...Hz]
                setNewPart(ref lo, ref hi, HiIndex, HiIndex + Jnumbs[J - j], lo.Length);   // Обязательно проверь!!!
                HiIndex += Jnumbs[J-j];
            }
        
            for (int i = 0; i < lonum; i++)               //remove [0-30Hz]
                lo[i] = 0.0;

        
            //synth/////////////////////////////
            FWT.FwtSynth(J);

            for (int i = 0; i < FWT.GetLoBandSize(); i++)
                data[i] = lo[i];
            for (int i = size - (size - FWT.GetLoBandSize()); i < size; i++)
                data[i] = 0.0;

            FWT.CloseFWT();       
            //debug        
            //ToTxt(L"10hz(intr1).txt",data,size);        
            return true;
        }

        private void setNewPart(ref double[] lo, ref double[] hi, int oldIndex, int startIndex, int endIndex)
        {
            for (int i = 0; i < endIndex - oldIndex; i++)
            {
                lo[oldIndex + i] = hi[i];
            }
            hi = new double[endIndex - startIndex];
            for (int i = 0; i < endIndex - startIndex; i++)
            {
                hi[i] = lo[startIndex + i];
            }
        }

        public int[,] GetQRS(double[] data, int size, double sr, string fltdir)
        {
            double[] pdata = new double[size];
            for (int i = 0; i < size; i++)
                pdata[i] = data[i];

            if (Filter30hz(ref pdata, size, sr, fltdir) == false) { //pdata filed with filterd signal
                pdata = null;
                return null;
            }

                // 2 continuous passes of CWT filtering
                /*if(Filter30hz(pdata,size,sr,fltdir,qNORM) == false)   //pdata filed with filterd signal
                        return 0;
                if(stype == qNOISE)
                if(Filter30hz(pdata,size,sr,fltdir,qNORM) == false)
                        return 0;*/
            


        
            double eCycle = (60.0 / (double)ahdr.maxbpm) - ahdr.maxQRS;  //secs

            if ((int)(eCycle*sr) <= 0) {
                eCycle = 0.1;
                ahdr.maxbpm = (int)(60.0 / (ahdr.maxQRS + eCycle));
            }
        //////////////////////////////////////////////////////////////////////////////

        
            int lqNum = 0;
            List<int> QRS = new List<int>();    //clean QRS detected
            int add = 0;


            while ((add < pdata.Length) && (pdata[add] != 0)) add += (int)(0.1 * sr);                  //skip QRS in begining

            while ((add < pdata.Length) && (pdata[add] == 0))   //проверка
            {
                add++;
            }                            //get  1st QRS

            QRS.Add(add - 1);
        
            /////////////////////////////////////////////////////////////////////////////
        
            for (int m = add; m < size; m++) {                  //MAX 200bpm  [0,3 sec  min cario size]
                m += (int)(ahdr.maxQRS * sr);                     //smpl + 0,20sec    [0,20 max QRS length]



                if (m >= size) m = size - 1;
                add = 0;

                //noise checking///////////////////////////////////////
                if (m + (int)(eCycle*sr) >= size) { //near end of signal
                        QRS.RemoveAt(QRS.Count - 1); // удаляет последний элемент списка
                        break;
                }


                double[] tmpPData = new double[pdata.Length - m];
                for (int z = 0; z < pdata.Length - m; z++){
                    tmpPData[z] = pdata[m+z];
                }

                if (IsNoise(tmpPData, (int)(eCycle*sr))) {  //smp(0.10sec)+0,20sec in noise
                        if (lqNum != (int)QRS.Count() - 1)
                                MA.Add(QRS[QRS.Count()-1]);     //push MA noise location

                        
                    QRS.RemoveAt(QRS.Count - 1);
                        
                    lqNum = QRS.Count;

                        //Find for next possible QRS start
                        while (IsNoise(tmpPData, (int)(eCycle*sr))) {
                                m += (int)(eCycle * sr);
                                tmpPData = new double[pdata.Length - m];
                                for (int z = 0; z < pdata.Length - m; z++)
                                {
                                    tmpPData[z] = pdata[m + z];
                                }
                                if (m >= size - (int)(eCycle*sr)) break;   //end of signal
                        }

                        if (m >= size - (int)(eCycle*sr)) break;
                        else {
                                while (pdata[m] == 0) {
                                        m++;
                                        if (m >= size) break;
                                }
                                if (m >= size) break;
                                else {
                                        QRS.Add(m - 1);
                                        continue;
                                }
                        }
                }
                tmpPData = null;
                ////////////////////////////////////////////////////////


                while (pdata[m-add] == 0.0) add++;               //Find back for QRS end


                if ((m - add + 1) - QRS[QRS.Count-1] > ahdr.minQRS*sr)  //QRS size > 0.04 sec
                        QRS.Add(m - add + 2); //QRS end
                else
                        QRS.RemoveAt(QRS.Count - 1);


                m += (int)(eCycle * sr);                                //smpl + [0,20+0,10]sec    200bpm MAX
                if (size - m < (int)(sr / 2)) break;



                while ((pdata[m] == 0) && ((size - m) >= (int)(sr / 2)))   //Find nearest QRS
                        m++;

                if (size - m < (int)(sr / 2)) break;  //end of data

                QRS.Add(m - 1);  //QRS begin
        }
        /////////////////////////////////////////////////////////////////////////////
        pdata = null;
            

            qrsNum = QRS.Count() / 2;

        if (qrsNum > 0)                              //         46: ?
        {                                           //          1: N    -1: nodata in **AUX
                qrsANN = new int[2*qrsNum, 3];                    // [samps] [type] [?aux data]

                for (int i = 0; i < 2*qrsNum; i++) {
                        qrsANN[i,0] = QRS[i];                                     //samp

                        if (i % 2 == 0)
                                qrsANN[i,1] = 1;                                        //type N
                        else {
                                qrsANN[i,1] = 40;                                      //type QRS)
                                //if( (qrsANN[i][0]-qrsANN[i-1][0]) >= int(sr*0.12) || (qrsANN[i][0]-qrsANN[i-1][0]) <= int(sr*0.03) )
                                // qrsANN[i-1][1] = 46;                                  //N or ? beat  (0.03?-0.12secs)
                        }

                        qrsANN[i,2] = -1;                                         //no aux data
                }

            return qrsANN;
        } 
        else     
            return null;
        }

        // Find ectopic beats in HRV data
        public void GetEctopics(ref int[,] ann, int qrsnum, double sr)
        {        
            if (qrsnum < 3)
                return;

            List<double> RRs = new List<double>(); ;
        
            for (int n = 0; n < qrsnum - 1; n++)
                RRs.Add((double)(ann[n*2+2, 0] - ann[n*2, 0]) / sr); //qrsNum-1 rr's
        
            RRs.Add(RRs[RRs.Count-1]);

            //  [RR1  RR2  RR3]   RR2 beat classification
        
            double rr1 = 0.0, rr2 = 0.0, rr3 = 0.0;
        
            for (int n = -2; n < (int)RRs.Count - 2; n++) {
                if (n == -2) {
                        rr1 = RRs[1];  //2
                        rr2 = RRs[0];  //1
                        rr3 = RRs[0];  //0
                } else if (n == -1) {
                        rr1 = RRs[1];
                        rr2 = RRs[0];
                        rr3 = RRs[1];
                } else {
                        rr1 = RRs[n];
                        rr2 = RRs[n+1];
                        rr3 = RRs[n+2];
                }

                if (60.0 / rr1 < ahdr.minbpm || 60.0 / rr1 > ahdr.maxbpm) //if RR's within 40-200bpm
                        continue;
                if (60.0 / rr2 < ahdr.minbpm || 60.0 / rr2 > ahdr.maxbpm) //if RR's within 40-200bpm
                        continue;
                if (60.0 / rr3 < ahdr.minbpm || 60.0 / rr3 > ahdr.maxbpm) //if RR's within 40-200bpm
                        continue;

                if (1.15*rr2 < rr1 && 1.15*rr2 < rr3) {
                        ann[n*2+4,1] = 46;
                        continue;
                }
                if (Math.Abs(rr1 - rr2) < 0.3 && rr1 < 0.8 && rr2 < 0.8 && rr3 > 2.4*(rr1 + rr2)) {
                        ann[n*2+4,1] = 46;
                        continue;
                }
                if (Math.Abs(rr1 - rr2) < 0.3 && rr1 < 0.8 && rr2 < 0.8 && rr3 > 2.4*(rr2 + rr3)) {
                        ann[n*2+4, 1] = 46;
                        continue;
                }
            }            
        }

        public void AddAnnotationOffset(int add)
        {
            for (int i = 0; i < qrsNum; i++)
                qrsANN[i, 0] += add;

            for (int i = 0; i < annNum; i++)
                ANN[i,0] += add;
        }

        private static void swap<T>(ref T a, ref T b)
        {
            T c = a;
            a = b;
            b = c;
        }

        public int[,] GetPTU(double[] data, int length, double sr, string fltdir, int[,] ann, int qrsnum)
        {       
            int size = 0, annPos = 0;        
            int T1 = -1, T = -1, T2 = -1, Twaves = 0;
            int P1 = -1, P = -1, P2 = -1, Pwaves = 0;

            cwt CWT = new cwt();
            List<int> Pwave = new List<int>();
            List<int> Twave = new List<int>();                             //Twave [ ( , T , ) ]
        
            double[] pspec;        // Это был указатель                     //      [ 0 , 0 , 0 ]  0 no Twave
        
            double min = 0.0, max = 0.0;                           //min,max for gaussian1 wave, center is zero crossing
            double rr = 0.0;   //rr interval

            bool sign, twave, pwave;
            int add = 0;//int(sr*0.04);  //prevent imprecise QRS end detection
            int maNum = 0;
        
            for (int n = 0; n < qrsnum - 1; n++) {
                annPos = ann[n*2+1, 0];                //i
                size = ann[n*2+2, 0] - ann[n*2+1, 0];  //i   size of  (QRS) <----> (QRS)

                bool maNs = false;
                for (int i = maNum; i < (int)MA.Count(); i++) {
                        if (MA[i] > ann[n*2+1, 0] && MA[i] < ann[n*2+2, 0]) {
                                Pwave.Add(0);
                                Pwave.Add(0);
                                Pwave.Add(0);
                                Twave.Add(0);
                                Twave.Add(0);
                                Twave.Add(0);
                                maNum++;
                                maNs = true;
                                break;
                        }
                }

                if (maNs) continue;


                rr = (double)(ann[n*2+2, 0] - ann[n*2, 0]) / sr;
                if (60.0 / rr < ahdr.minbpm || 60.0 / rr > ahdr.maxbpm - 20) { //check if normal RR interval (40bpm - 190bpm)
                        Pwave.Add(0);
                        Pwave.Add(0);
                        Pwave.Add(0);
                        Twave.Add(0);
                        Twave.Add(0);
                        Twave.Add(0);
                        continue;
                }


                ///////////////search for TWAVE///////////////////////////////////////////////////////////

                if (sr*ahdr.maxQT - (ann[n*2+1, 0] - ann[n*2+0, 0]) > size - add)
                        size = size - add;
                else
                        size = (int)(sr * ahdr.maxQT) - (ann[n*2+1, 0] - ann[n*2+0, 0]) - add;


                //double avg = Mean(data+annPos+add,size);         //avrg extension on boundaries
                //double lvl,rvl;
                //lvl = data[annPos+add];
                //rvl = data[annPos+add+size-1];

                if (ahdr.biTwave == (int)WAVETYPE.BIPHASE)
                {
                    CWT.InitCWT(size, cwt.WAVELET.GAUS, 0, sr);
                }
                else
                {
                    CWT.InitCWT(size, cwt.WAVELET.GAUS1, 0, sr);
                }
                

                double[] tmpData = new double[data.Length - annPos - add];
                for (int iii = 0; iii < (data.Length - annPos - add); iii++){
                    tmpData[iii] = data[annPos + add + iii];
                }

                // tmpData - клон data но со сдвигом
                pspec = CWT.CwtTrans(tmpData, ahdr.tFreq);

                MinMax(pspec, size, ref min, ref max);
                for (int i = 0; i < size; i++) {
                        if (pspec[i] == min) T1 = i + annPos + add;
                        if (pspec[i] == max) T2 = i + annPos + add;
                }
                if (T1 > T2) swap(ref T1, ref T2);

                //additional constraints on [T1 T T2] duration, symmetry, QT interval
                twave = false;
                if ((pspec[T1-annPos-add] < 0 && pspec[T2-annPos-add] > 0) || (pspec[T1-annPos-add] > 0 && pspec[T2-annPos-add] < 0))
                        twave = true;
                if (twave) {
                        if ((double)(T2 - T1) >= 0.09*sr) { // && (double)(T2-T1)<=0.24*sr)   //check for T wave duration
                                twave = true;                  //QT interval = .4*sqrt(RR)
                                if ((double)(T2 - ann[n*2+0, 0]) >= ahdr.minQT*sr && (double)(T2 - ann[n*2+0, 0]) <= ahdr.maxQT*sr)
                                        twave = true;
                                else
                                        twave = false;
                        } else
                                twave = false;
                }

                if (twave) {
                        if (pspec[T1-annPos-add] > 0) sign = true;
                        else sign = false;

                        for (int i = T1 - annPos - add; i < T2 - annPos - add; i++) {
                                if (sign) {
                                        if (pspec[i] > 0) continue;
                                } else {
                                        if (pspec[i] < 0) continue;
                                }

                                T = i + annPos + add;
                                break;
                        }

                        //check for T wave symetry//////////////////////////
                        double ratio;
                        if (T2 - T < T - T1) ratio = (double)(T2 - T) / (double)(T - T1);
                        else ratio = (double)(T - T1) / (double)(T2 - T);
                        ////////////////////////////////////////////////////

                        if (ratio < 0.4) { //not a T wave
                                Twave.Add(0);
                                Twave.Add(0);
                                Twave.Add(0);
                                twave = false;
                        } else {
                                //adjust center of T wave
                                //smooth it with gaussian, Find max ?
                                            
                            double[] tmpDataMM = new double[data.Length - T1];
                            for (int iii = 0; iii < (data.Length - T1); iii++){
                                tmpDataMM[iii] = data[T1 + iii];
                            }
                                int Tcntr = FindTmax(tmpDataMM, T2 - T1);
                                if (Tcntr != -1) {
                                        Tcntr += T1;
                                        if (Math.Abs((Tcntr - T1) - ((T2 - T1) / 2)) < Math.Abs((T - T1) - ((T2 - T1) / 2)))  //which is close to center 0-cross or T max
                                                T = Tcntr;
                                }

                                Twaves++;
                                Twave.Add(T1);
                                Twave.Add(T);
                                Twave.Add(T2);
                        }
                } else { //no T wave???    empty (QRS) <-------> (QRS)
                        Twave.Add(0);
                        Twave.Add(0);
                        Twave.Add(0);
                }
                T = -1;
                ///////////////search for TWAVE///////////////////////////////////////////////////////////
                CWT.CloseCWT();

                ///////////////search for PWAVE///////////////////////////////////////////////////////////

                size = ann[n*2+2, 0] - ann[n*2+1, 0];  //n   size of  (QRS) <----> (QRS)

                if (sr*ahdr.maxPQ < size)
                        size = (int)(sr * ahdr.maxPQ);

                if (twave) {
                        if (T2 > ann[n*2+2, 0] - size - (int)(0.04*sr))   // pwave wnd far from Twave at least on 0.02sec
                                size -= T2 - (ann[n*2+2, 0] - size - (int)(0.04 * sr));
                }
                int size23 = (ann[n*2+2, 0] - ann[n*2+1, 0]) - size;

                //size -= 0.02*sr;   //impresize QRS begin detection
                if (size <= 0.03*sr) {
                        Pwave.Add(0);
                        Pwave.Add(0);
                        Pwave.Add(0);
                        continue;
                }


                //avg = Mean(data+annPos+size23,size);                     //avrg extension on boundaries
                //lvl = data[annPos+size23];
                //rvl = data[annPos+size23+size-1];
                CWT.InitCWT(size, cwt.WAVELET.GAUS1, 0, sr);                                        //6-Gauss1 wlet


                double[] tmpData23 = new double[data.Length - annPos - size23];
                for (int iii = 0; iii < (data.Length - annPos - size23); iii++)
                {
                    tmpData23[iii] = data[annPos + size23 + iii];
                }

                pspec = CWT.CwtTrans(tmpData23, ahdr.pFreq);//,false,lvl,rvl);    //9Hz transform  pspec = size-2/3size

                MinMax(pspec, size, ref min, ref max);
                for (int i = 0; i < size; i++) {
                        if (pspec[i] == min) P1 = i + annPos + size23;
                        if (pspec[i] == max) P2 = i + annPos + size23;
                }
                if (P1 > P2) swap(ref P1, ref P2);

                //additional constraints on [P1 P P2] duration, symmetry, PQ interval
                pwave = false;
                if ((pspec[P1-annPos-size23] < 0 && pspec[P2-annPos-size23] > 0) || (pspec[P1-annPos-size23] > 0 && pspec[P2-annPos-size23] < 0))
                        pwave = true;
                if (pwave) {
                        if ((double)(P2 - P1) >= 0.03*sr && (double)(P2 - P1) <= 0.15*sr) { //check for P wave duration  9Hz0.03 5Hz0.05
                                pwave = true;                //PQ interval = [0.07 - 0.12,0.20]
                                if ((double)(ann[n*2+2,0] - P1) >= ahdr.minPQ*sr && (double)(ann[n*2+2,0] - P1) <= ahdr.maxPQ*sr)
                                        pwave = true;
                                else
                                        pwave = false;
                        } else
                                pwave = false;
                }

                if (pwave) {
                        if (pspec[P1-annPos-size23] > 0) sign = true;
                        else sign = false;

                        for (int i = P1 - annPos - size23; i < P2 - annPos - size23; i++) {
                                if (sign) {
                                        if (pspec[i] > 0) continue;
                                } else {
                                        if (pspec[i] < 0) continue;
                                }

                                P = i + annPos + size23;
                                break;
                        }

                        //check for T wave symetry//////////////////////////
                        double ratio;
                        if (P2 - P < P - P1) ratio = (double)(P2 - P) / (double)(P - P1);
                        else ratio = (double)(P - P1) / (double)(P2 - P);
                        ////////////////////////////////////////////////////

                        if (ratio < 0.4f) { //not a P wave
                                Pwave.Add(0);
                                Pwave.Add(0);
                                Pwave.Add(0);
                        } else {
                                Pwaves++;
                                Pwave.Add(P1);
                                Pwave.Add(P);
                                Pwave.Add(P2);
                        }
                } else {
                    Pwave.Add(0);
                    Pwave.Add(0);
                    Pwave.Add(0);
                }
                P1 = -1;
                P = -1;
                P2 = -1;
                ///////////////search for PWAVE///////////////////////////////////////////////////////////
                CWT.CloseCWT();
            }






        
            /////////////////get q,r,s peaks//////////////////////////////////////////////////////////
        
            // on a denoised signal       
            int peaksnum = 0;        
            int Q = 0, R = 0, S = 0;        
            List<int> qrsPeaks = new List<int>();          //q,r,s peaks [ q , r , s ]                                        //            [ 0,  R , 0 ]  zero if not defined       
            List<char> qrsTypes = new List<char>();         //[q,r,s] or [_,R,s], etc...


        
            for (int n = 0; n < qrsnum; n++) { //fill with zeros
                for (int i = 0; i < 3; i++) {
                        qrsPeaks.Add(0);
                        qrsTypes.Add(' ');                
                }
            }


        
            double[] buff = new double[length];
            for (int i = 0; i < length; i++)
                buff[i] = data[i];

            EcgDenoise enoise = new EcgDenoise();

            enoise.InitDenoise(fltdir, buff, length, sr);
        
            if (enoise.LFDenoise()) {
                double[] pbuff = null;
                for (int n = 0; n < qrsnum; n++) {   
                    annPos = ann[n*2, 0];   //PQ                        
                    size = ann[n*2+1, 0] - ann[n*2, 0] + 1; //PQ-Jpnt, including Jpnt

                    pbuff = new double[buff.Length - annPos];
                    for (int iii = 0; iii < buff.Length - annPos; iii++)
                    {
                        pbuff[iii] = buff[annPos + iii];
                    }
                        //pbuff = &buff[annPos];

                        Q = -1;
                        FindRS(pbuff, size, ref R, ref S, ahdr.minUmV);
                        if (R != -1) R += annPos;
                        if (S != -1) S += annPos;

                        if (R != -1 && S != -1) {  // Rpeak > 0mV Speak < 0mV
                                if (S < R) { //check for S
                                        if (buff[R] > -buff[S]) {
                                                Q = S;
                                                S = -1;

                                                size = ann[n*2+1, 0] - R + 1;  //including Jpnt

                                                pbuff = new double[buff.Length - R];
                                                for (int iii = 0; iii < buff.Length - R; iii++)
                                                {
                                                    pbuff[iii] = buff[R + iii];
                                                }

                                                //pbuff = &buff[R];
                                                S = Finds(pbuff, size, 0.05);
                                                if (S != -1) S += R;
                                        }
                                } else {   //check for Q
                                        size = R - annPos + 1;  //including R peak
                                        Q = Findq(pbuff, size, 0.05);
                                        if (Q != -1) Q += annPos;
                                }
                        }
                        //else if only S
                        else if (S != -1) { //Find small r if only S detected  in rS large T lead
                                size = S - annPos + 1; //including S peak

                                pbuff = new double[buff.Length - annPos];
                                for (int iii = 0; iii < buff.Length - annPos; iii++)
                                {
                                    pbuff[iii] = buff[annPos + iii];
                                }
                                //pbuff = &buff[annPos];
                                R = Findr(pbuff, size, 0.05);
                                if (R != -1) R += annPos;
                        }
                        //else if only R
                        else if (R != -1) { //only R Find small q,s
                                size = R - annPos + 1; //including R peak
                                Q = Findq(pbuff, size, 0.05);
                                if (Q != -1) Q += annPos;
                                size = ann[n*2+1,0] - R + 1;  //including Jpnt

                                pbuff = new double[buff.Length - R];
                                for (int iii = 0; iii < buff.Length - R; iii++)
                                {
                                    pbuff[iii] = buff[R + iii];
                                }

                                //pbuff = &buff[R];
                                S = Finds(pbuff, size, 0.05);
                                if (S != -1) S += R;
                        }


                        //put peaks to qrsPeaks vector
                        if (R == -1 && S == -1) { //no peaks
                                ann[n*2,1] = 16;   //ARTEFACT
                                //remove P,T
                                if (n != 0) {
                                        if (Pwave[3*(n-1)] != 0) {
                                                Pwaves--;
                                                Pwave[3*(n-1)] = 0;
                                                Pwave[3*(n-1)+1] = 0;
                                                Pwave[3*(n-1)+2] = 0;
                                        }
                                }
                                if (n != qrsnum - 1) {
                                    if (Twave[3 * n] != 0)
                                    {
                                                Twaves--;
                                                Twave[3*n] = 0;
                                                Twave[3*n+1] = 0;
                                                Twave[3*n+2] = 0;
                                        }
                                }
                        }
                        if (Q != -1) {
                                peaksnum++;
                                qrsPeaks[n*3] = Q;
                                if (Math.Abs(buff[Q]) > 0.5)
                                    qrsTypes[n * 3] = (char)17; //'Q';
                                else
                                    qrsTypes[n * 3] = (char)15; //'q';
                        }
                        if (R != -1) {
                                peaksnum++;
                                qrsPeaks[n*3+1] = R;
                                if (Math.Abs(buff[R]) > 0.5)
                                    qrsTypes[n * 3 + 1] = (char)48; //'R';
                                else
                                    qrsTypes[n * 3 + 1] = (char)47; //'r';
                        }
                        if (S != -1) {
                                peaksnum++;
                                qrsPeaks[n*3+2] = S;
                                if (Math.Abs(buff[S]) > 0.5)
                                    qrsTypes[n * 3 + 2] = (char)50; //'S';
                                else
                                        qrsTypes[n*3+2] = (char)49; //'s';
                        }
                }
        }

            buff = null;
        /////////////////get q,r,s peaks//////////////////////////////////////////////////////////






        ///////////////////////// complete annotation array///////////////////////////////////////
        maNum = 0;

        //Pwave vec size = Twave vec size
        annNum = Pwaves * 3 + qrsnum * 2 + peaksnum + Twaves * 3 + (int)MA.Count;   //P1 P P2 [QRS] T1 T T2  noise annotation
        if (annNum > qrsnum)                        //42-(p 43-p) 24-Pwave
        {                                           //44-(t 45-t) 27-Twave
                ANN = new int[annNum , 3];                    // [samps] [type] [?aux data]
                /*for (int i = 0; i < annNum; i++)
                        ANN[i] = new int[3];*/

                int index = 0; //index to ANN
                int qindex = 0;  //index to qrsANN

                for (int i = 0; i < (int)Twave.Count; i += 3) {   //Twave=Pwaves=qrsPeaks size
                        //QRS complex
                        ANN[index, 0] = ann[qindex, 0];           //(QRS
                        ANN[index, 1] = ann[qindex, 1];           //
                        ANN[index++, 2] = ann[qindex++, 2];       //aux
                        if (qrsPeaks[i] != 0) { //q
                                ANN[index,0] = qrsPeaks[i];
                                ANN[index, 1] = qrsTypes[i];
                                ANN[index++, 2] = -1;              //no aux
                        }
                        if (qrsPeaks[i+1] != 0) { //r
                                ANN[index,0] = qrsPeaks[i+1];
                                ANN[index,1] = qrsTypes[i+1];
                                ANN[index++,2] = -1;              //no aux
                        }
                        if (qrsPeaks[i+2] != 0) { //s
                                ANN[index, 0] = qrsPeaks[i+2];
                                ANN[index, 1] = qrsTypes[i+2];
                                ANN[index++, 2] = -1;              //no aux
                        }
                        ANN[index, 0] = ann[qindex,0];           //QRS)
                        ANN[index, 1] = ann[qindex, 1];           //
                        ANN[index++, 2] = ann[qindex++, 2];       //aux

                        //T wave
                        if (Twave[i] != 0) {
                                ANN[index, 0] = Twave[i];                //(t
                                ANN[index, 1] = 44;
                                ANN[index++, 2] = -1;                    //no aux
                                ANN[index, 0] = Twave[i+1];              //T
                                ANN[index, 1] = 27;
                                ANN[index++, 2] = -1;                    //no aux
                                ANN[index, 0] = Twave[i+2];              //t)
                                ANN[index, 1] = 45;
                                ANN[index++, 2] = -1;                    //no aux
                        }
                        //P wave
                        if (Pwave[i] != 0) {
                                ANN[index, 0] = Pwave[i];                //(t
                                ANN[index, 1] = 42;
                                ANN[index++, 2] = -1;                    //no aux
                                ANN[index, 0] = Pwave[i+1];              //T
                                ANN[index, 1] = 24;
                                ANN[index++, 2] = -1;                    //no aux
                                ANN[index, 0] = Pwave[i+2];              //t)
                                ANN[index, 1] = 43;
                                ANN[index++, 2] = -1;                    //no aux
                        }

                        if (!(Twave[i] !=0) && !(Pwave[i] != 0)) {          //check for MA noise //могу ошибаться 
                                for (int m = maNum; m < (int)MA.Count; m++) {
                                        if (MA[m] > ann[qindex-1,0] && MA[m] < ann[qindex,0]) {
                                                ANN[index,0] = MA[m];      //Noise
                                                ANN[index,1] = 14;
                                                ANN[index++,2] = -1;       //no aux
                                                maNum++;
                                                break;
                                        }
                                }
                        }
                }

                //last QRS complex
                int ii = 3 * (qrsnum - 1);
                ANN[index, 0] = ann[qindex, 0];           //(QRS
                ANN[index, 1] = ann[qindex, 1];           //
                ANN[index++, 2] = ann[qindex++, 2];       //aux
                if (qrsPeaks[ii] != 0) { //q
                        ANN[index,0] = qrsPeaks[ii];
                        ANN[index,1] = qrsTypes[ii];
                        ANN[index++,2] = -1;              //no aux
                }
                if (qrsPeaks[ii+1] != 0) { //r
                        ANN[index, 0] = qrsPeaks[ii+1];
                        ANN[index, 1] = qrsTypes[ii+1];
                        ANN[index++, 2] = -1;              //no aux
                }
                if (qrsPeaks[ii+2] != 0) { //s
                        ANN[index, 0] = qrsPeaks[ii+2];
                        ANN[index, 1] = qrsTypes[ii+2];
                        ANN[index++, 2] = -1;              //no aux
                }
                ANN[index, 0] = ann[qindex, 0];           //QRS)
                ANN[index, 1] = ann[qindex, 1];           //
                ANN[index++, 2] = ann[qindex++, 2];       //aux

                //check if noise after last qrs
                if (maNum < (int)MA.Count) {
                        if (MA[maNum] > ann[qindex-1,0]) {
                                ANN[index, 0] = MA[maNum];      //Noise
                                ANN[index, 1] = 14;
                                ANN[index++, 2] = -1;
                        }
                }
                return ANN;
        } else
                return null;
        }

        public bool GetRRseq(int[,] ann, int nums, double sr, ref List<double> RR, ref List<int> RRpos) // RR and Rrpos  is pointer
        {    
            int add = -1;
            double rr = 0.0, r1 = 0.0, r2 = 0.0;

        
            RR = new List<double>();
            RRpos = new List<int>();
            //R peak or S peak annotation
            bool rrs = true;
            int rNum = 0, sNum = 0;
            for (int i = 0; i < nums; i++) {
                if (ann[i,1] == 47 || ann[i,1] == 48) rNum++;
                else if (ann[i,1] == 49 || ann[i,1] == 50) sNum++;
            }
        
            if ((int)(1.2f*(float)rNum) < sNum)
                rrs = false;  //R peaks less than S ones


        
            for (int i = 0; i < nums; i++) {
                switch (ann[i,1]) {
                case 0:    //non beats
                case 15:   //q
                case 17:   //Q
                case 18:   //ST change
                case 19:   //T change
                case 20:   //systole
                case 21:   //diastole
                case 22:
                case 23:
                case 24:
                case 25:
                case 26:
                case 27:
                case 28:
                case 29:
                case 30:
                case 31:
                case 32:
                case 33:
                case 36:
                case 37:
                case 40:   //')' J point
                case 42:   //(p
                case 43:   //p)
                case 44:   //(t
                case 45:   //t)
                case 47:   //r
                case 48:   //R
                case 49:   //s
                case 50:   //S
                        continue;

                case 14:   //noise
                case 16:   //artifact
                        add = -1;
                        continue;
                }

                if (add != -1) {
                        //annotation on RRs peaks
                        if (rrs) {
                                if (i + 1 < nums && (ann[i+1,1] == 47 || ann[i+1,1] == 48))  //r only
                                        r2 = ann[i+1,0];
                                else if (i + 2 < nums && (ann[i+2,1] == 47 || ann[i+2,1] == 48))  //q,r
                                        r2 = ann[i+2,0];
                                else //(ann[i][1]==N,ECT,...)  //no detected R only S
                                        r2 = ann[i,0];

                                if (add + 1 < nums && (ann[add+1,1] == 47 || ann[add+1,1] == 48))
                                        r1 = ann[add+1,0];
                                else if (add + 2 < nums && (ann[add+2,1] == 47 || ann[add+2,1] == 48))
                                        r1 = ann[add+2, 0];
                                else //(ann[add][1]==N,ECT,...) //no detected R only S
                                        r1 = ann[add, 0];
                        }
                        //annotation on S peaks
                        else {
                                if (i + 1 < nums && (ann[i+1,1] == 40))  //N)
                                        r2 = ann[i,0];
                                else if (i + 1 < nums && (ann[i+1,1] == 49 || ann[i+1,1] == 50))  //Sr
                                        r2 = ann[i+1,0];
                                else if (i + 2 < nums && (ann[i+2,1] == 49 || ann[i+2,1] == 50))  //rS
                                        r2 = ann[i+2,0];
                                else if (i + 3 < nums && (ann[i+3,1] == 49 || ann[i+3,1] == 50))  //errQ rS
                                        r2 = ann[i+3,0];
                                else if (i + 1 < nums && (ann[i+1,1] == 47 || ann[i+1,1] == 48))  //no S
                                        r2 = ann[i+1,0];
                                else if (i + 2 < nums && (ann[i+2,1] == 47 || ann[i+2,1] == 48))  //no S
                                        r2 = ann[i+2,0];

                                if (add + 1 < nums && (ann[add+1,1] == 40))  //N)
                                        r1 = ann[add,0];
                                else if (add + 1 < nums && (ann[add+1,1] == 49 || ann[add+1,1] == 50))
                                        r1 = ann[add+1,0];
                                else if (add + 2 < nums && (ann[add+2,1] == 49 || ann[add+2,1] == 50))
                                        r1 = ann[add+2,0];
                                else if (add + 3 < nums && (ann[add+3,1] == 49 || ann[add+3,1] == 50))
                                        r1 = ann[add+3,0];
                                else if (add + 1 < nums && (ann[add+1,1] == 47 || ann[add+1,1] == 48))  //no S
                                        r1 = ann[add+1,0];
                                else if (add + 2 < nums && (ann[add+2,1] == 47 || ann[add+2,1] == 48))  //no S
                                        r1 = ann[add+2,0];
                        }

                        
                    rr = 60.0 / ((r2 - r1) / sr); 
                    if (rr >= ahdr.minbpm && rr <= ahdr.maxbpm) {
                                RR.Add(rr);         //in bpm
                                RRpos.Add((int)r1);                       
                    }               
                }
                add = i;
        
            }        
            if (RR.Count != 0)                
                return true;        
            else                               
                return false;
        }

    }
}
