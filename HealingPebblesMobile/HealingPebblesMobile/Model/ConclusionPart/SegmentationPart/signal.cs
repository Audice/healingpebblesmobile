﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;


namespace HealingPebblesMobile.Model.ConclusionPart.SegmentationPart
{
    class signal
    {
        private struct DataHDR
        {
            public char[] hdr; // Массив char состоит из 4 элементов
            public uint size;
            public float sr;
            public Byte bits;
            public Byte lead;
            public ushort umv;
            public ushort bline;
            public Byte hh;
            public Byte mm;
            public Byte ss;
            public char[] rsrv; //Массив char состоит из 19 элементов
        }

        //Поля и свойства

        protected double[] pData; // Указатель на данные, решить это позже
        protected double  SR;
        protected int Lead, UmV, Bits;
        protected uint Length;
        protected int hh, mm, ss; // Начальное время записи  экг

        // Пропущены типы связанные с map file

        protected string file; // в исходнике он FILE*
        protected float[] lpf;
        protected short[] lps;
        protected ushort[] lpu;
        protected char[] lpc;

        private bool IsBinFile;
        string EcgFileName;
        DataHDR pEcgHeader;        // Данные загаловки экг  
        List<DataHDR> EcgHeaders;  //Список хидеров
        List<double[]> EcgSignals; //Список сигналов


        //Методы класса
        public signal()
        {
            pEcgHeader = new DataHDR();
            pEcgHeader.hdr = new char[4];
            pEcgHeader.rsrv = new char[19];
            pData = new double[5]; //разберись в хранении данных!!! какого размера массив?
            SR = 0.0;
            Bits = 0; UmV = 0; Lead = 0; Length = 0;
            hh=0; mm=0; ss=0;
            EcgFileName = "";
            EcgHeaders = new List<DataHDR>();
            EcgSignals = new List<double[]>();
        }

        public double[] GetData(int index)
        {
            if (EcgSignals.Count == 0) 
                return null; //Обработай ошибку!!
        
            if (index > (int)EcgSignals.Count-1) 
                index = (int)EcgSignals.Count-1;
            else if (index < 0) 
                index = 0;
       
            pEcgHeader = EcgHeaders[index];     
            pData = EcgSignals[index];
            SR = pEcgHeader.sr;        
            Lead = pEcgHeader.lead;        
            UmV = pEcgHeader.umv;        
            Bits = pEcgHeader.bits;        
            Length = pEcgHeader.size;        
            hh = pEcgHeader.hh;        
            mm = pEcgHeader.mm;        
            ss = pEcgHeader.ss;

            return pData;
        }
        public void mSecToTime(int msec, ref int h, ref int m, ref int s, ref int ms)
        {
            ms = msec % 1000;        
            msec /= 1000;
            if (msec < 60) {
                h = 0;
                m = 0;
                s = msec;                 
            } else {
                double tmp;
                tmp = (double)(msec % 60) / 60;
                tmp *= 60;
                s = (int)tmp;
                msec /= 60;
                if (msec < 60) {
                        h = 0;
                        m = msec;
                } else {
                        h = msec / 60;
                        tmp = (double)(msec % 60) / 60;
                        tmp *= 60;
                        m = (int)tmp;
                }
        }
}
        public uint GetLength(){
            return Length;
        }
        public int GetBits()
        {
            return Bits;
        }
        public int GetUmV()
        {
            return UmV;
        }
        public int GetLead()
        {
            return Lead;
        }
        public int GetLeadsNum()
        {
            return (int)EcgSignals.Count;
        }
        public double GetSR()
        {
            return SR;
        }
        public int GetH()
        {
            return hh;
        }
        public int GetM()
        {
            return mm;
        }
        public int GetS()
        {
            return ss;
        }
        public double log2(double x)
        {
            double a = Math.Log(x) / Math.Log(2.0);
            return Math.Log(x) / Math.Log(2.0);
        }
      


        public double[] ReadFile(string pathToDatFile)
        {
            EcgFileName = pathToDatFile;
            BinaryReader br;
            try
            {
                if ((br = new BinaryReader(File.Open(EcgFileName, FileMode.Open))) != null)
                {
                    ReadMitbihFile(br);
                }
                br.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error: Could not read file. Original error: " + ex.Message);
            }
        
            return GetData(0);
        }
        public void ChangeExtension(ref string path, string ext)
        {
            int i = 0;
            for (i = path.Length - 1; i > 0; i--) {
                if (path[i] == '.') {
                    path = path.Remove(i+1, 3) + ext;
                        return;
                }
            }
            if (i == 0){            
                Console.WriteLine("File name is not valid");
                return;
            }
        }
        public bool ReadMitbihFile(BinaryReader br)
        {
        
            string HeaFile = "";
            HeaFile = EcgFileName;
            ChangeExtension(ref HeaFile, "hea");


            StreamReader sr=new StreamReader(HeaFile);
            if (sr == null) 
                return false;
             
        
            if (ParseMitbihHeader(sr)) {
                sr.Close();
                pEcgHeader = EcgHeaders[0];
                int lNum = (int)EcgHeaders.Count;
                int size = System.Convert.ToInt32(pEcgHeader.size);

                short numeric;
                
                for (int i = 0; i < lNum; i++) {
                        pData = new double[pEcgHeader.size];
                        EcgSignals.Add(pData);
                }

                for (int s = 0; s < size; s++) {
                        for (int n = 0; n < lNum; n++) {
                                pData = EcgSignals[n];
                                pEcgHeader = EcgHeaders[n];

                                switch (pEcgHeader.bits) {

                                case 16:                                      //16format
                                        numeric = br.ReadInt16();        // 
                                        pData[s] = (double)(numeric - (short)pEcgHeader.bline) / (double)pEcgHeader.umv;
                                        break;
                                }
                        }
                }
                return true;
        } else {
                return false;
        }
        }
        private bool ParseMitbihHeader(StreamReader streamRead)
        {
            string[] leads = new string[18]{"I", "II", "III", "aVR", "aVL", "aVF", "v1", "v2",
                              "v3", "v4", "v5", "v6", "MLI", "MLII", "MLIII", "vX", "vY", "vZ"}; 
   
            string[] str = new string[10];
            int sNum, size;
            float sr;


            str[0] = streamRead.ReadLine();
            int currentIndex = 1;
            for (int i = 0; i < str[0].Length; i++)
            {
                if ((str[0][i] == ' ') && (str[0][i + 1] != ' '))
                {
                    currentIndex++;
                }
                else {
                    str[currentIndex] += str[0][i];
                }
                
            }
            if (str[4] == null)
                return false;

            if ((str[5] != null) && (str[5].Length == 8))
            {
                string ptime = str[5];
                hh = System.Convert.ToInt32(ptime[0] + ptime[1] + ptime[2]);
                mm = System.Convert.ToInt32(ptime[3] + ptime[4] + ptime[5]);
                ss = System.Convert.ToInt32(ptime[6] + ptime[7] + ptime[8]);
            }

            sNum = System.Convert.ToInt32(str[2].ToString());
            sr = (float)System.Convert.ToDouble(str[3].ToString());
            size = System.Convert.ToInt32(str[4].ToString());

            int eNum = 0;
            for (int i = 0; i < sNum; i++)
            {
                str[0] = streamRead.ReadLine();
                if ((str[0] == null) || (str[0].Length <= 0))
                    return false;

                int umv, bits, bline;
                str[9] = "";

                for (int iii = 1; iii < 5; iii++)
                {
                    str[iii] = "";
                }

                currentIndex = 1;
                for (int ii = 0; ii < str[0].Length; ii++)
                {
                    if ((str[0][ii] == ' ') && (str[0][ii + 1] != ' '))
                    {
                        currentIndex++;
                    }
                    else
                    {
                        str[currentIndex] += str[0][ii];
                    }

                }

                if (str[5] == null) return false;

                bits = System.Convert.ToInt32(str[2].ToString());
                
                int indexForSearcheNumeric = 0;
                string tmpStroke = "";
                while ((str[3].Length > indexForSearcheNumeric) && (IsNumeric((str[3][indexForSearcheNumeric]).ToString())))
                {
                    tmpStroke += (str[3][indexForSearcheNumeric]).ToString();
                    indexForSearcheNumeric++;
                }

                umv = System.Convert.ToInt32(tmpStroke);

                bline = System.Convert.ToInt32(str[5].ToString());

                int offs = str[1].Length, j = 0;
                for (j = 0; j < offs; j++)
                {
                    if (EcgFileName[((EcgFileName).Length - offs) + j] != str[1][j])  
                        break;
                }
                if (j == offs)
                {
                    eNum++;
                    DataHDR hdr = new DataHDR();
                    hdr.hdr = new char[4];
                    hdr.rsrv = new char[19];
                    hdr.sr = sr;
                    hdr.bits = (byte)((bits == 212) ? 12 : bits);
                    hdr.umv = (ushort)((umv == 0) ? 200 : umv);
                    hdr.bline = (ushort)bline;
                    hdr.size = (uint)size;
                    hdr.hh = (byte)hh;
                    hdr.mm = (byte)mm;
                    hdr.ss = (byte)ss;
                    for (int l = 0; l < 18; l++)
                    {
                        if (leads[l].Equals(str[9]))
                        {
                            hdr.lead = (byte)(l + 1);
                            break;
                        }
                    }
                    EcgHeaders.Add(hdr);
                }
            }
            
            return true;
        }

        public void setTXTData(float sr, int sizeData, ushort umv, ushort bline, Int16[] data)
        {
            DataHDR hdr = new DataHDR();
            hdr.hdr = new char[4];
            hdr.rsrv = new char[19];
            hdr.sr = sr;
            hdr.bits = 16;
            hdr.umv = (ushort)((umv == 0) ? 200 : umv);
            hdr.bline = (ushort)bline;
            hdr.size = (uint)sizeData;
            hdr.hh = (byte)hh;
            hdr.mm = (byte)mm;
            hdr.ss = (byte)ss;
            hdr.lead = 3; // по умолчанию
            EcgHeaders.Add(hdr);

            pEcgHeader = EcgHeaders[0];
            int lNum = (int)EcgHeaders.Count;
            int size = System.Convert.ToInt32(pEcgHeader.size);
            int numeric;
            for (int i = 0; i < lNum; i++)
            {
                pData = new double[pEcgHeader.size];
                EcgSignals.Add(pData);
            }

            for (int s = 0; s < size; s++)
            {
                for (int n = 0; n < lNum; n++)
                {
                    pData = EcgSignals[n];
                    pEcgHeader = EcgHeaders[n];
                    switch (pEcgHeader.bits)
                    {
                        case 16:                                      //16format        
                            numeric = data[s];        //        
                            pData[s] = (double)(((double)numeric - (double)bline)/(double)(umv));//(double)(numeric - (short)pEcgHeader.bline) / (double)pEcgHeader.umv;
                            break;
                    }
                }
            }
            GetData(0);
        }

        private static Boolean IsNumeric(string stringToTest)
        {
            int result;
            return int.TryParse(stringToTest, out result);
        }
        public double[] GetpData(int index)
        {
            return EcgSignals[index];
        }

        public bool ToTXT(string pathForSave, double[] buffer, int size)
        {
            StreamWriter sw = new StreamWriter(pathForSave, false);
            if (sw != null)
            {
                for (int i = 0; i < buffer.Length; i++)
                {
                    //double outData = buffer[i];
                    sw.WriteLine(buffer[i].ToString());
                }
                return true;
            }
            else
            {
                return false;
            }

        }

        public void MinMax(double[] buffer, int size, ref double min, ref double max)
        {
            max = buffer[0];
            min = buffer[0];
            for (int i = 1; i < size; i++) {  
                if (buffer[i] > max) max = buffer[i]; 
                if (buffer[i] < min) min = buffer[i];
            }
        }

        public double Mean(double[] buffer, int size)
        {    
            double mean = 0;
            for (int i = 0; i < size; i++) mean += buffer[i];
            return (double)mean / (double)size;
        }

        public double Std(double[] buffer, int size)
        {        
            double mean, disp = 0.0;       
            mean = Mean(buffer, size);  
            for (int i = 0; i < size; i++) disp += (buffer[i] - mean) * (buffer[i] - mean);
            return (Math.Sqrt((double)disp / (double)(size - 1)));
        }

        public void  nMinMax(ref double[] buffer, int size, ref double a, ref double b)
        {
            double min=0, max=0;
            MinMax(buffer, size, ref min, ref max);
            for (int i = 0; i < size; i++) {
                if ((max - min) != 0.0)   //Не крутое сравнение
                    buffer[i] = (buffer[i] - min) * ((b - a) / (max - min)) + a;
                else   
                    buffer[i] = a;
            }
        }

        public void nMean(ref double[] buffer, int size)
        {        
            double mean = Mean(buffer, size);
            for (int i = 0; i < size; i++)
                buffer[i] = buffer[i] - mean;
        }

        public void nZscore(ref double[] buffer, int size)
        {
            double mean = Mean(buffer, size);
            double disp = Std(buffer, size);
            if (disp == 0.0) disp = 1.0;
            for (int i = 0; i < size; i++)
                buffer[i] = (double)(buffer[i] - mean) / (double)disp;
        }

        public void nSoftmax(ref double[] buffer, int size)
        {       
            double mean = Mean(buffer, size);       
            double disp = Std(buffer, size);     
            if (disp == 0.0) disp = 1.0;
            for (int i = 0; i < size; i++)
                buffer[i] = 1.0 / (1 + Math.Exp(-((double)(buffer[i] - mean) / (double)disp)));
        }

        public void nEnergy(double[] buffer, int size, int L)
        {
            double enrg = 0.0;
            for (int i = 0; i < size; i++) 
                enrg += Math.Pow(Math.Abs(buffer[i]), (double)L);
    
            enrg = Math.Pow(enrg, (double)1.0 / (double)L);
            if (enrg == 0.0) enrg = 1.0;

            for (int i = 0; i < size; i++)
                buffer[i] /= (double)enrg;
        }

        public double MINIMAX(double[] buffer, int size)
        {
            return Std(buffer, size)*(0.3936 + 0.1829*Math.Log((double)size));
        }

        public double FIXTHRES(double[] buffer, int size)
        {      
            return Std(buffer, size)*Math.Sqrt(2.0*Math.Log((double)size));
        }

        public double SURE(double[] buffer, int size)
        {
            return Std(buffer, size) * Math.Sqrt(2.0 * Math.Log((double)size * Math.Log((double)size)));
        }

        //Не могу решить проблему с Denoise. Решил - осталось проверить

        public void Denoise(ref double[] buffer, int size, int window, int type = 0, bool soft = true)
        {
            double TH = 0.0;
            int currentIndex = 0;
            double[] bufferTMP = new double[buffer.Length];
            for (int i = 0; i < (buffer.Length); i++)
            {
                bufferTMP[i] = buffer[i];
            }

            for (int i = 0; i < size / window; i++) {
                switch (type) {
                case 0:
                        TH = MINIMAX(bufferTMP, window);
                        break;
                case 1:
                        TH = FIXTHRES(bufferTMP, window);
                        break;
                case 2:
                        TH = SURE(bufferTMP, window);
                        break;
                }
                if (soft)
                    SoftTH(ref bufferTMP, window, TH);
                else
                    HardTH(ref bufferTMP, window, TH);

                for (int j = 0; j < window; j++)
                {
                    buffer[j + currentIndex] = bufferTMP[j];
                }
                //buffer += window; 
                currentIndex += window;
                if (currentIndex < buffer.Length){            
                    bufferTMP = new double[window];         
                    for (int j = 0; j < (window); j++)            
                    {           
                        bufferTMP[j] = buffer[currentIndex + j];
                    }
                }
            }


        
            if (size % window > 5) { //skip len=1

                bufferTMP = new double[buffer.Length - currentIndex];
                for (int i = 0; i < (buffer.Length - currentIndex); i++)
                {
                    bufferTMP[i] = buffer[i + currentIndex];
                }

                switch (type) {
                case 0:
                        TH = MINIMAX(bufferTMP, size % window);
                        break;
                case 1:
                        TH = FIXTHRES(bufferTMP, size % window);
                        break;
                case 2:
                        TH = SURE(bufferTMP, size % window);
                        break;
                }
                if (soft)
                    SoftTH(ref bufferTMP, size % window, TH);
                else
                    HardTH(ref bufferTMP, size % window, TH);


                // костыль - проверь работоспособность!
                for (int i = 0; i < (buffer.Length - currentIndex); i++)
                {
                    buffer[i + currentIndex] = bufferTMP[i];
                }
            }
        }

        private void  HardTH(ref double[] buffer, int size, double TH, double l=0.0)
        {
            for (int i = 0; i < size; i++)
                if (Math.Abs(buffer[i]) <= TH)       
                    buffer[i] *= l;
        }

        private void  SoftTH(ref double[] buffer, int size, double TH, double l=0.0)
        {
        
            for (int i = 0; i < size; i++) {
                if (Math.Abs(buffer[i]) <= TH) {
                        buffer[i] *= l;
                } else {
                    if (buffer[i] > 0) {
                        buffer[i] -= TH * (1 - l);
                    } else {      
                        buffer[i] += TH * (1 - l);  
                    }
                }
            }
        }

        public void AutoCov(ref double[] buffer, int size)
        {
        
            double[] rk;
            double mu = 0.0; ;
            int t=0;
            rk = new double[size];
            mu = Mean(buffer, size);

            for (int k = 0; k < size; k++) {
                rk[k] = 0;
                t = 0;
                while (t + k != size) {       
                    rk[k] += (buffer[t] - mu) * (buffer[t+k] - mu);
                        t++;
                }
                rk[k] /= (double)t;                       // rk[k] /= t ?  autocovariance
            }
            for (int i = 0; i < size; i++)
                buffer[i] = rk[i];        
        }

        public void AutoCov1(ref double[] buffer, int size)
        {
            double[] rk;
            double mu=0.0;        
            rk = new double[size];

            for (int k = 0; k < size; k++) {
                rk[k] = 0;
                for (int t = 0; t < size; t++) {   
                    if (t + k >= size)     
                        rk[k] += (buffer[t] - mu) * (buffer[2*size-(t+k+2)] - mu);
                    else     
                        rk[k] += (buffer[t] - mu) * (buffer[t+k] - mu);
                }
                rk[k] /= (double)size;
            }
            for (int i = 0; i < size; i++)
                buffer[i] = rk[i];        
}

        public void AutoCor(ref double[] buffer, int size)
        {
        
            double[] rk;
            double mu = 0.0, std = 0.0;
            int t = 0;
            rk = new double[size];
            mu = Mean(buffer, size);
            std = Std(buffer, size);

            for (int k = 0; k < size; k++) {
                rk[k] = 0;
                t = 0;
                while (t + k != size) { 
                    rk[k] += (buffer[t] - mu) * (buffer[t+k] - mu); 
                    t++;
                }
                rk[k] /= (double)t * std * std;
            }

            for (int i = 0; i < size; i++)
                buffer[i] = rk[i];
        }

        public void AutoCor1(ref double[] buffer, int size)
        {
        
            double[] rk;
            double mu = 0.0, std = 0.0;
            rk = new double[size];
            mu = Mean(buffer, size);
            std = Std(buffer, size);

        
            for (int k = 0; k < size; k++) {
                rk[k] = 0;
                for (int t = 0; t < size; t++) {
                    if (t + k >= size)       
                        rk[k] += (buffer[t] - mu) * (buffer[2*size-(t+k+2)] - mu);
                    else  
                        rk[k] += (buffer[t] - mu) * (buffer[t+k] - mu);
                }
                rk[k] /= (double)size * std * std;
            }
            for (int i = 0; i < size; i++)
                buffer[i] = rk[i];      
        }


    }
}
