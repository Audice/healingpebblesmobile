﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HealingPebblesMobile.Model.ConclusionPart.SegmentationPart.Filters
{
    class bior97:IFilter
    {
        public static string Title = "bior97";

        public double[] Load_TH_Filter(ref int thL, ref int thZ)
        {
            double[] TH_Filter = new double[] { 
                0.02675, 
                -0.01686, 
                -0.07822, 
                0.26686, 
                0.60295, 
                0.26686, 
                -0.07822, 
                -0.01686, 
                0.02675
            };
            thL = 9;
            thZ = 5;
            return TH_Filter;
        }

        public double[] Load_TG_Filter(ref int tgL, ref int tgZ)
        {
            double[] TG_Filter = new double[] { 
                0.0,
                0.0,            
                0.045635,
                -0.02877,
                -0.295635,           
                0.55754,
                -0.295635,
                -0.02877,
                0.045635,
            };
            tgL = 9;
            tgZ = 5;
            return TG_Filter;
        }

        public double[] Load_H_Filter(ref int hL, ref int hZ)
        {
            double[] H_Filter = new double[] { 
                -0.045635,
                -0.02877,
                0.295635,
                0.55754,
                0.295635,
                -0.02877,
                -0.045635
            };
            hL = 7;
            hZ = 4;
            return H_Filter;
        }

        public double[] Load_G_Filter(ref int gL, ref int gZ)
        {
            double[] G_Filter = new double[] { 
                0.02675,
                0.01686,
                -0.07822,
                -0.26686,
                0.60295,
                -0.26686,
                -0.07822,
                0.01686,
                0.02675
            };
            gL = 9;
            gZ = 4;
            return G_Filter;
        }
    }
}
