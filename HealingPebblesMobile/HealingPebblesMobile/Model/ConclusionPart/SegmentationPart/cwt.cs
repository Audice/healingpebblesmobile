﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HealingPebblesMobile.Model.ConclusionPart.SegmentationPart
{
    class cwt: signal
    {
        struct CWThdr {
            public char[] hdr; //4
            public float fmin;
            public float fmax;
            public float fstep;
            public uint size;
            public float sr;
            public Byte type;   //1-log; 0-norm scale
            public char[] rsrv; //15
        };

        // Data
        public enum WAVELET { MHAT, INV, MORL, MORLPOW, MORLFULL, GAUS, GAUS1, GAUS2, GAUS3, GAUS4, GAUS5, GAUS6, GAUS7 };
        public enum SCALETYPE { LINEAR_SCALE, LOG_SCALE };

        private CWThdr phdr;

        private double MinFreq;
        private double MaxFreq;
        private double FreqInterval;
        private double w0;        
        
        private SCALETYPE ScaleType;        
        private WAVELET Wavelet;                   //Wavelet

        private int SignalSize; 
        private double[] pData;               //pointer to original signal
        private double[] pCwtSpectrum;              //buffer with spectra
        private double[] pReal;
        private double[] pImag;             //Re,Im parts of wavelet
        
        private bool IsPrecision; 
        private int PrecisionSize; 

        private bool IsPeriodicBoundary;                //periodic boundary extension
        private double LeftVal, RightVal;          //left/right value for signal ext at boundaries

        public cwt()
        {
            phdr.hdr = new char[4];
            phdr.rsrv = new char[15];
            ScaleType = SCALETYPE.LINEAR_SCALE;
            pCwtSpectrum = null;
            pReal = null;
            pImag = null;
        }

        //initialize continue wavelet transformation
        public void InitCWT(int size, WAVELET wavelet, double w, double sr)
        {
            SignalSize = size;
            if (sr != 0)  // 0 = false
                SR = sr;

            w0 = w;
            pReal = new double[2 * SignalSize - 1];
            pImag = new double[2 * SignalSize - 1];
            pCwtSpectrum = new double[SignalSize];
            Wavelet = wavelet;        

            for (int i = 0; i < 2*SignalSize - 1; i++) {
                pReal[i] = 0;
                pImag[i] = 0;
            }
        }

        // Оба метода не используются в текущем контексте данных, их дальнейшее определение зависит от
        // типа входных данных
        /*
        float[] CwtCreateFileHeader(string name, CWThdr hdr, WAVELET wavelet, double w)
        {
        
            string tmp = "";
            int filesize;
            switch (wavelet) {
                case WAVELET.MHAT:
                    name += "(mHat).w";
                break;
                case WAVELET.INV:
                    name += "(Inv).w";
                break;
                case WAVELET.MORL:
                    name += "(Morl).w";
                break;
                case WAVELET.MORLPOW:
                    name += "(MPow).w";
                break;
                case WAVELET.MORLFULL:
                    name += "(MComp"+((int)w).ToString() + ").w" ;
                break;
                case WAVELET.GAUS:
                    name += "(Gaussian).w";
                break;
                case WAVELET.GAUS1:
                    name += "(1Gauss).w";
                break;
                case WAVELET.GAUS2:
                    name += "(2Gauss).w";
                break;
                case WAVELET.GAUS3:
                    name += "(3Gauss).w";
                break;
                case WAVELET.GAUS4:
                    name += "(4Gauss).w";
                break;
        
                case WAVELET.GAUS5:
                
                    name += "(5Gauss).w";
                break;
                case WAVELET.GAUS6:
                    name += "(6Gauss).w";
                break;
                case WAVELET.GAUS7:
                    name += "(7Gauss).w";
                break;
        }

        if (hdr.type != 0)
                filesize = (int)hdr.size * (int)Math.Ceiling((Math.Log(hdr.fmax) + hdr.fstep - Math.Log(hdr.fmin)) / hdr.fstep);
        else
                filesize = (int)hdr.size * (int)Math.Ceiling((hdr.fmax + hdr.fstep - hdr.fmin) / hdr.fstep);


        filesize = sizeof(float) * filesize + sizeof(hdr);

        fp = CreateFileW(name, GENERIC_WRITE | GENERIC_READ, 0, 0, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, 0);
        if (fp == INVALID_HANDLE_VALUE)
                return 0;
        fpmap = CreateFileMapping(fp, 0, PAGE_READWRITE, 0, filesize, 0);
        lpMap = MapViewOfFile(fpmap, FILE_MAP_WRITE, 0, 0, filesize);

        lpf = (float *)lpMap;

        memset(lpMap, 0, filesize);
        memcpy(lpMap, hdr, sizeof(CWTHDR));

        return (lpf + sizeof(CWTHDR) / sizeof(float));
}

float* CWT::CwtReadFile(const wchar_t *name)
{
        fp = CreateFileW(name, GENERIC_WRITE | GENERIC_READ, 0, 0, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, 0);
        if (fp == INVALID_HANDLE_VALUE)
                return 0;
        fpmap = CreateFileMapping(fp, 0, PAGE_READWRITE, 0, 0, 0);
        lpMap = MapViewOfFile(fpmap, FILE_MAP_WRITE, 0, 0, 0);

        phdr = (PCWTHDR)lpMap;
        lpf = (float *)lpMap;


        if (memcmp(phdr->hdr, "WLET", 4))
                return 0;

        MinFreq = phdr->fmin;
        MaxFreq = phdr->fmax;
        FreqInterval = phdr->fstep;
        Length = phdr->size;
        SR = phdr->sr;
        if (phdr->type == LINEAR_SCALE)
                ScaleType = LINEAR_SCALE;
        else if(phdr->type == LOG_SCALE)
                ScaleType = LOG_SCALE;

        return (lpf + sizeof(CWTHDR) / sizeof(float));
}
          */


        public double HzToScale(double f, double sr, WAVELET wavelet, double w)
        {
            double k = 0;
            switch (wavelet) {
                case WAVELET.MHAT:
                    k = 0.22222 * sr;
                    break;
                case WAVELET.INV:
                    k = 0.15833 * sr;                
                    break;
                case WAVELET.MORL: break; // не уверен, но думаю лучше поставить break. Проверить при тесте!
                case WAVELET.MORLPOW:  k = sr;   break;
                case WAVELET.MORLFULL:
                    k = sr * w * 0.1589;
                    break;            
                case WAVELET.GAUS:                  
                    k = 0.2 * sr;                
                    break;            
                case WAVELET.GAUS1:                
                    k = 0.16 * sr;                
                    break;            
                case WAVELET.GAUS2:                                
                    k = 0.224 * sr;                
                    break;            
                case WAVELET.GAUS3:                
                    k = 0.272 * sr;                
                    break;            
                case WAVELET.GAUS4:                
                    k = 0.316 * sr;                
                    break;            
                case WAVELET.GAUS5:                
                    k = 0.354 * sr;                
                    break;            
                case WAVELET.GAUS6:                
                    k = 0.388 * sr;                
                    break;            
                case WAVELET.GAUS7:                
                    k = 0.42 * sr;
                    break;
                default: k = 0; break; // break не было в оригинале...
            }
            return (k / f);
        }

        public void ConvertName(ref string name, WAVELET wavelet, double w)
        {
            switch (wavelet) {
                case WAVELET.MHAT:
                    name += "(mHat).w";
                break;
                case WAVELET.INV:
                    name += "(Inv).w";
                break;
                case WAVELET.MORL:
                    name += "(Morl).w";
                break;
                case WAVELET.MORLPOW:
                    name += "(MPow).w";
                break;
                case WAVELET.MORLFULL:
                    name += "(MComp"+((int)w).ToString() + ").w" ;
                break;
                case WAVELET.GAUS:
                    name += "(Gaussian).w";
                break;
                case WAVELET.GAUS1:
                    name += "(1Gauss).w";
                break;
                case WAVELET.GAUS2:
                    name += "(2Gauss).w";
                break;
                case WAVELET.GAUS3:
                    name += "(3Gauss).w";
                break;
                case WAVELET.GAUS4:
                    name += "(4Gauss).w";
                break;
                case WAVELET.GAUS5:
                    name += "(5Gauss).w";
                break;
                case WAVELET.GAUS6:
                    name += "(6Gauss).w";
                break;
                case WAVELET.GAUS7:
                    name += "(7Gauss).w";
                break;
            }
        }

        public double[] CwtTrans(double[] data, double freq, bool periodicboundary = true, double lval = 0.0, double rval = 0.0)
        {
            IsPeriodicBoundary = periodicboundary;
            LeftVal = lval;
            RightVal = rval;
            IsPrecision = false;
            PrecisionSize = 0;                                      //0,0000001 float prsision
            double t = 0.0, sn = 0.0, cs = 0.0, scale = 0.0;
            scale = HzToScale(freq, SR, Wavelet, w0);

///////////wavelet calculation//////////////////////////////////////////////////////////////////
///////// center = SignalSize-1 in wavelet mass////////////////////////////////////////////////////

        for (int i = 0; i < SignalSize; i++) {                     //positive side
                t = ((double)i) / scale;

                if (Wavelet > WAVELET.INV && Wavelet < WAVELET.MORLFULL)
                {
                        sn = Math.Sin(6.28 * t);
                        cs = Math.Cos(6.28 * t);
                }
                if (Wavelet == WAVELET.MORLFULL)
                {
                        sn = Math.Sin(w0 * t);
                        cs = Math.Cos(w0 * t);
                }

                switch (Wavelet) {
                    case WAVELET.MHAT:
                        pReal[(SignalSize-1)+i] = Math.Exp(-t * t / 2) * (-t * t + 1);
                        break;
                    case WAVELET.INV:
                        pReal[(SignalSize - 1) + i] = t * Math.Exp(-t * t / 2);
                        break;
                    case WAVELET.MORL:
                        pReal[(SignalSize - 1) + i] = Math.Exp(-t * t / 2) * (cs - sn);
                        break;
                    case WAVELET.MORLPOW:
                        pReal[(SignalSize - 1) + i] = Math.Exp(-t * t / 2) * cs;
                        pImag[(SignalSize - 1) + i] = Math.Exp(-t * t / 2) * sn;
                        break;
                    case WAVELET.MORLFULL:
                        pReal[(SignalSize - 1) + i] = Math.Exp(-t * t / 2) * (cs - Math.Exp(-w0 * w0 / 2));
                        pImag[(SignalSize - 1) + i] = Math.Exp(-t * t / 2) * (sn - Math.Exp(-w0 * w0 / 2));
                        break;

                    case WAVELET.GAUS:
                        pReal[(SignalSize - 1) + i] = Math.Exp(-t * t / 2);
                        break;
                    case WAVELET.GAUS1:
                        pReal[(SignalSize - 1) + i] = -t * Math.Exp(-t * t / 2);
                        break;
                    case WAVELET.GAUS2:
                        pReal[(SignalSize - 1) + i] = (t * t - 1) * Math.Exp(-t * t / 2);
                        break;
                    case WAVELET.GAUS3:
                        pReal[(SignalSize - 1) + i] = (2 * t + t - t * t * t) * Math.Exp(-t * t / 2);
                        break;
                    case WAVELET.GAUS4:
                        pReal[(SignalSize - 1) + i] = (3 - 6 * t * t + t * t * t * t) * Math.Exp(-t * t / 2);
                        break;
                    case WAVELET.GAUS5:
                        pReal[(SignalSize - 1) + i] = (-15 * t + 10 * t * t * t - t * t * t * t * t) * Math.Exp(-t * t / 2);
                        break;
                    case WAVELET.GAUS6:
                        pReal[(SignalSize - 1) + i] = (-15 + 45 * t * t - 15 * t * t * t * t + t * t * t * t * t * t) * Math.Exp(-t * t / 2);
                        break;
                    case WAVELET.GAUS7:
                        pReal[(SignalSize - 1) + i] = (105 * t - 105 * t * t * t + 21 * t * t * t * t * t - t * t * t * t * t * t * t) * Math.Exp(-t * t / 2);
                        break;
                }

                if (Math.Abs(pReal[(SignalSize-1)+i]) < 0.0000001)
                        PrecisionSize++;

                if (PrecisionSize > 15) {
                        PrecisionSize = i;
                        IsPrecision = true;
                        break;
                }
        }
        if (IsPrecision == false)
                PrecisionSize = SignalSize;

        for (int i = -(PrecisionSize - 1); i < 0; i++) {               //negative side
                t = ((double)i) / scale;

                if (Wavelet > WAVELET.INV && Wavelet < WAVELET.MORLFULL)
                {
                        sn = Math.Sin(6.28 * t);
                        cs = Math.Cos(6.28 * t);
                }
                if (Wavelet == WAVELET.MORLFULL)
                {
                        sn = Math.Sin(w0 * t);
                        cs = Math.Cos(w0 * t);
                }

                switch (Wavelet) {
                    case WAVELET.MHAT:
                        pReal[(SignalSize - 1) + i] = Math.Exp(-t * t / 2) * (-t * t + 1);
                        break;
                    case WAVELET.INV:
                        pReal[(SignalSize - 1) + i] = t * Math.Exp(-t * t / 2);
                        break;
                    case WAVELET.MORL:
                        pReal[(SignalSize - 1) + i] = Math.Exp(-t * t / 2) * (cs - sn);
                        break;
                    case WAVELET.MORLPOW:
                        pReal[(SignalSize - 1) + i] = Math.Exp(-t * t / 2) * cs;
                        pImag[(SignalSize - 1) + i] = Math.Exp(-t * t / 2) * sn;
                        break;
                    case WAVELET.MORLFULL:
                        pReal[(SignalSize - 1) + i] = Math.Exp(-t * t / 2) * (cs - Math.Exp(-w0 * w0 / 2));
                        pImag[(SignalSize - 1) + i] = Math.Exp(-t * t / 2) * (sn - Math.Exp(-w0 * w0 / 2));
                        break;

                    case WAVELET.GAUS:
                        pReal[(SignalSize-1)+i] = Math.Exp(-t * t / 2);    //gauss
                        break;
                    case WAVELET.GAUS1:
                        pReal[(SignalSize-1)+i] = -t * Math.Exp(-t * t / 2);   //gauss1
                        break;
                    case WAVELET.GAUS2:
                        pReal[(SignalSize-1)+i] = (t * t - 1) * Math.Exp(-t * t / 2);     //gauss2
                        break;
                    case WAVELET.GAUS3:
                        pReal[(SignalSize-1)+i] = (2 * t + t - t * t * t) * Math.Exp(-t * t / 2);    //gauss3
                        break;
                    case WAVELET.GAUS4:
                        pReal[(SignalSize-1)+i] = (3 - 6 * t * t + t * t * t * t) * Math.Exp(-t * t / 2);    //gauss4
                        break;
                    case WAVELET.GAUS5:
                        pReal[(SignalSize-1)+i] = (-15 * t + 10 * t * t * t - t * t * t * t * t) * Math.Exp(-t * t / 2);    //gauss5
                        break;
                    case WAVELET.GAUS6:
                        pReal[(SignalSize-1)+i] = (-15 + 45 * t * t - 15 * t * t * t * t + t * t * t * t * t * t) * Math.Exp(-t * t / 2);   //gauss6
                        break;
                    case WAVELET.GAUS7:
                        pReal[(SignalSize-1)+i] = (105 * t - 105 * t * t * t + 21 * t * t * t * t * t - t * t * t * t * t * t * t) * Math.Exp(-t * t / 2);    //gauss7
                        break;
                }
        }
///////end wavelet calculations////////////////////////////////////////////
            pData = data;  // не уверен что так можно делать!!! проверь!!! // присвоение указателей которые обнулятся
            for (int x = 0; x < SignalSize; x++)
                pCwtSpectrum[x] = CwtTrans(x, scale);

            return pCwtSpectrum;
        }

        private  double CwtTrans(int x, double scale)
        {
            double res = 0, Re = 0, Im = 0;
            for (int t = 0; t < SignalSize; t++) {                   //main
                if (IsPrecision == true) {
                        if (t < x - PrecisionSize) 
                                t = x - (PrecisionSize - 1);  //continue;
                        if (t >= PrecisionSize + x) 
                                break;
                }

                Re += pReal[((SignalSize-1)-x) + t] * pData[t];
                if (Wavelet == WAVELET.MORLPOW || Wavelet == WAVELET.MORLFULL)
                        Im += pImag[((SignalSize-1)-x) + t] * pData[t];
            }
        
        ////////////////////boundaries///////////////////////////////////////////////
        
            int p = 0;
        
            for (int i = (SignalSize - PrecisionSize); i < (SignalSize - 1) - x; i++) {        // Left edge calculations
                if (IsPeriodicBoundary) {
                        Re += pReal[i] * pData[(SignalSize-1)-i-x];  //IsPeriodicBoundary
                } else {
                        if (LeftVal != 0.0)
                                Re += pReal[i] * LeftVal;
                        else
                                Re += pReal[i] * pData[0];
                }

                if (Wavelet == WAVELET.MORLPOW || Wavelet == WAVELET.MORLFULL)
                { //Im part for complex wavelet
                        if (IsPeriodicBoundary) {
                                Im += pImag[i] * pData[(SignalSize-1)-i-x];
                        } else {
                                if (LeftVal != 0.0)
                                        Im += pImag[i] * LeftVal;
                                else
                                        Im += pImag[i] * pData[0];
                        }
                }
            }
        
            int q = 0;
            for (int i = 2 * SignalSize - (x + 1); i < SignalSize + PrecisionSize - 1; i++) {     // Right edge calculations
                if (IsPeriodicBoundary)
                        Re += pReal[i] * pData[(SignalSize-2)-q]; //IsPeriodicBoundary
                else {
                        if (RightVal != 0.0)
                                Re += pReal[i] * RightVal;
                        else
                                Re += pReal[i] * pData[SignalSize-1];
                }

                if (Wavelet == WAVELET.MORLPOW || Wavelet == WAVELET.MORLFULL)
                {
                        if (IsPeriodicBoundary) {
                                Im += pImag[i] * pData[(SignalSize-2)-q];
                        } else {
                                if (RightVal != 0.0)
                                        Im += pImag[i] * RightVal;
                                else
                                        Im += pImag[i] * pData[SignalSize-1];
                        }
                }
                q++;
            }
        ////////////////////boundaries///////////////////////////////////////////////

        switch (Wavelet) {
            case WAVELET.MORL:
                res = (1 / Math.Sqrt(6.28)) * Re;
                break;
            case WAVELET.MORLPOW:
                res = Math.Sqrt(Re * Re + Im * Im);
                res *= (1 / Math.Sqrt(6.28));
                break;
            case WAVELET.MORLFULL:
                res = Math.Sqrt(Re * Re + Im * Im);
                res *= (1 / Math.Pow(3.14, 0.25));
                break;

            default: res = Re; break;
        
        }
            res = (1 / Math.Sqrt(scale)) * res;
            return res;
        }

        public void CloseCWT(){
            if (pReal != null) {
                pReal = null;
            }
            if (pImag != null) {
                pImag = null;
            }
            if (pCwtSpectrum != null) {
                pCwtSpectrum = null;
            }
        }

        public int GetFreqRange()
        {
            if (ScaleType == SCALETYPE.LINEAR_SCALE)
                return (int)((MaxFreq + FreqInterval - MinFreq) / FreqInterval);
            else if (ScaleType == SCALETYPE.LINEAR_SCALE)
                return (int)((Math.Log(MaxFreq) + FreqInterval - Math.Log(MinFreq)) / FreqInterval);
            else 
                return 0;
        }
        double GetMinFreq()
        {
            return MinFreq;
        }
        public double GetMaxFreq()
        {
            return MaxFreq;
        }
        public double GetFreqInterva()
        {
            return FreqInterval;

        }
        public int GetScaleType()
        {
            return (int)ScaleType;
        }


    }
}
