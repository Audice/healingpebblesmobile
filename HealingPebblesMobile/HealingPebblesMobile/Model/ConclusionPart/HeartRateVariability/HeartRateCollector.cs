﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HealingPebblesMobile.Model.ConclusionPart.HeartRateVariability
{
    /// <summary>
    /// Класс, наелен на аккумуляцию R-R интервалов, их конвертацию и обсчёт
    /// </summary>
    public class HeartRateCollector
    {
        private readonly ushort DigitsNumberCoef = 1000; 
        /// <summary>
        /// Шаг RR интервалов в массиве моды
        /// </summary>
        readonly ushort ModeArrayStep = 50;
        /// <summary>
        /// Левая граница добавления интервала в массив моды
        /// </summary>
        readonly uint LeftBorderModeArrey = 300; //300 миллисекунд
        /// <summary>
        /// Правая граница добавления интервала в массив моды
        /// </summary>
        readonly uint RightBorderModeArrey = 3300; //3300 миллисекунд
        /// <summary>
        /// Массив для вычисления моды полученных RR мнтервалов
        /// </summary>
        private uint[] ModeArray;
        /// <summary>
        /// Правая граница RR интервалов
        /// </summary>
        private readonly double RightSpacingValue = 3.3;
        /// <summary>
        /// Левая граница RR интервалов
        /// </summary>
        private readonly double LeftSpacingValue = 0.3;

        /// <summary>
        /// Процент ошибки для определения достоверности максимального и минимального RR интервала
        /// </summary>
        private readonly byte MistakePercent = 3;

        //Переменные для расчёта ВСР
        /// <summary>
        /// Мода RR интервалов
        /// </summary>
        private double Mo = 0.0;
        /// <summary>
        /// Индекс моды RR интервалов
        /// </summary>
        private ushort IndexMo = 0;
        /// <summary>
        /// Амплитуда моды - процент кардиоинтервалов попавших в диапозон моды
        /// </summary>
        private ushort AMo = 0;
        /// <summary>
        /// Количество RR интервалов, попавших в рассматриваемый диапазон
        /// </summary>
        private uint NumRRintervals = 0;
        /// <summary>
        /// Вариационный размах - максимальная амплитуда колебаний значений кардиоинтервалов
        /// </summary>
        private double VariationalSpan = 0.0;

        HeartRatingCompiler HeartRatingCompiler = null;
        SystemTensionIndexController SystemTensionIndexController = null;
        VegetativeBalanceController VegetativeBalanceController = null;
        AdaptationLevel AdaptationLevel = null;
        public HeartRateCollector(HeartRatingCompiler heartRatingCompiler, SystemTensionIndexController systemTensionIndexController,
            VegetativeBalanceController vegetativeBalanceController, AdaptationLevel adaptationLevel)
        {
            this.ModeArray = new uint[(this.RightBorderModeArrey - this.LeftBorderModeArrey) / ModeArrayStep];
            this.HeartRatingCompiler = heartRatingCompiler;
            this.SystemTensionIndexController = systemTensionIndexController;
            this.VegetativeBalanceController = vegetativeBalanceController;
            this.AdaptationLevel = adaptationLevel;
            this.NumRRintervals = 0;
        }

        
        public void AppendRRArray(double[] currentRRPart)
        {
            if (currentRRPart != null && currentRRPart.Length > 1)
            {
                List<double> filteredRRIntervals = new List<double>();
                for (int i=0; i < currentRRPart.Length; i++)
                {
                    if (currentRRPart[i] < this.RightSpacingValue && currentRRPart[i] >= LeftSpacingValue)
                    {
                        filteredRRIntervals.Add(currentRRPart[i]);
                        uint indexRR = ((uint)(currentRRPart[i] * this.DigitsNumberCoef) - this.LeftBorderModeArrey) / this.ModeArrayStep;
                        if (indexRR < this.ModeArray.Length)
                        {
                            this.ModeArray[indexRR]++;
                            this.NumRRintervals++;
                        }
                    }
                    else
                        filteredRRIntervals.Add(-1); // Шум маркируем -1
                }
                CalculationVariabilityParam();
                if (this.SystemTensionIndexController != null) this.SystemTensionIndexController.UpdateTensionIndex(this.AMo, this.Mo, this.VariationalSpan);
                if (this.VegetativeBalanceController != null) this.VegetativeBalanceController.UpdateVegetativeBalanceIndex(AMo, VariationalSpan);
                if (this.AdaptationLevel != null) this.AdaptationLevel.UpdateAdaptationLevel(AMo);
                //Запуск обсчёта рейтинга системы
                if (this.HeartRatingCompiler != null) this.HeartRatingCompiler.RecalculationRating(filteredRRIntervals);
            }
        }

        void CalculationVariabilityParam()
        {
            if (this.ModeArray != null && this.ModeArray.Length > 0 && this.NumRRintervals > 0)
            {
                short curIndex = -1; //!!!
                uint curMaxValue = 0;
                for (int i=0; i < this.ModeArray.Length; i++)
                {
                    if (this.ModeArray[i] > curMaxValue)
                    {
                        curMaxValue = this.ModeArray[i];
                        curIndex = (short)i;
                    }
                }
                if (curIndex >= 0)
                {
                    this.Mo = (double)(curIndex * this.ModeArrayStep + this.LeftBorderModeArrey) / this.DigitsNumberCoef;
                    this.AMo = (ushort)(((double)this.ModeArray[curIndex] / this.NumRRintervals) * 100);
                }
                CalculateVariationalSpan();
            }
        }
        /// <summary>
        /// Метод расчёта вариационного размаха. Вводится некий процент, на основе которого и расчитывается правдоподобность максимального и минимального RR интервала
        /// </summary>
        void CalculateVariationalSpan()
        {
            if (this.ModeArray != null && this.ModeArray.Length > 0 && this.NumRRintervals > 0)
            {
                short minIndex = -1;
                bool isMinIndex = false;
                short maxIndex = -1;
                bool isMaxIndex = false;
                for (int i=0; i < this.ModeArray.Length; i++)
                {
                    //Поиск минимального RR
                    if (this.ModeArray[i] > 0 && !isMinIndex)
                    {
                        if ((((double)this.ModeArray[i] / this.NumRRintervals) * 100) > this.MistakePercent)
                        {
                            minIndex = (short)i;
                            isMinIndex = true;
                        }
                    }
                    //Поиск максимального RR
                    if (this.ModeArray[(this.ModeArray.Length - 1) - i] > 0 && !isMaxIndex)
                    {
                        if ((((double)this.ModeArray[(this.ModeArray.Length - 1) - i] / this.NumRRintervals) * 100) > this.MistakePercent)
                        {
                            maxIndex = (short)((this.ModeArray.Length - 1) - i);
                            isMaxIndex = true;
                        }
                    }
                    if (isMinIndex && isMaxIndex)
                        break;
                }
                if (minIndex >= maxIndex)
                    this.VariationalSpan = 0;
                else
                    this.VariationalSpan = (double)(maxIndex * this.ModeArrayStep + this.LeftBorderModeArrey) / this.DigitsNumberCoef
                        - (double)(minIndex * this.ModeArrayStep + this.LeftBorderModeArrey) / this.DigitsNumberCoef;
            }    
        }
    }
}
