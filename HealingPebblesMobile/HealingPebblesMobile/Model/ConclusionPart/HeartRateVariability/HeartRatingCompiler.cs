﻿using HealingPebblesMobile.Model.ConclusionPart.ConclusionEventArgs;
using System;
using System.Collections.Generic;
using System.Text;

namespace HealingPebblesMobile.Model.ConclusionPart.HeartRateVariability
{
    /// <summary>
    /// Класс обрабатывает R-R интервалы, и на их основе формирует заключение о состоянии сердечно сосудистой системе.
    /// Особенности работы: объект класса принимает RR интервалы и рассматривает количество подряд идущих, увеличивающихся по протяжённости интервалов.
    /// При появлении RR интервала, меньше предыдущего по протяжённости, расчёт оканчивается и повышается конкретный рейтинг. Такое количество определяет рейтинг ССС.
    /// Рейтинг обновляется в динамике, оповещая систему об изменениях. 
    /// </summary>
    public class HeartRatingCompiler
    {
        private readonly byte MinRatingsCount = 8;
        /// <summary>
        /// Код, характеризующий состояние системы. Код 0 - состояние не определено, Код 1-9 - конкретное состояние
        /// </summary>
        private byte CurrentHeartSystemRating = 0;
        /// <summary>
        /// Количество рассматриваемых рейтингов
        /// </summary>
        private readonly byte RatingsCount = 15;
        /// <summary>
        /// Массив рейтингов. Размер массива определяется величиной RatingsCount
        /// </summary>
        private uint[] Ratings;

        /// <summary>
        /// Событие, срабатывающее при обновлении рейтинга
        /// </summary>
        public event EventHandler<HeartrateRatingEventArgs> HeartrateRatingUpdateEvent;
        /// <summary>
        /// Инициация возникновения события HeartrateRatingUpdateEvent
        /// </summary>
        /// <param name="e">Пакет аргументов</param>
        protected void OnHeartrateRatingUpdate(HeartrateRatingEventArgs e)
        {
            EventHandler<HeartrateRatingEventArgs> handler = HeartrateRatingUpdateEvent;
            if (handler != null)
            {
                handler(this, e);
            }
        }

        public HeartRatingCompiler()
        {
            Ratings = new uint[RatingsCount];
        }

        public void RecalculationRating(List<double> r_rIntervals)
        {
            if (this.RatingsCount <= 1 || r_rIntervals == null)
                return;
            if (r_rIntervals.Count < 1)
                return;
            //Количество найденных возрастающих RR интервалов
            uint figureCount = 0;
            short curFigure = -1;
            for (int i=1; i < r_rIntervals.Count; i++)
            {
                if (r_rIntervals[i - 1] < 0)
                {
                    //Отрицательный интервал означает появление шума. Его игнорируем
                    curFigure = -1;
                    continue;
                }
                if (r_rIntervals[i - 1] < r_rIntervals[i])
                {
                    curFigure++;
                }
                else
                {
                    //Строгое возрастание прервалось, записываем рейтинг
                    if (figureCount == 0)
                    {
                        //Первую фигуру не рассматриваем из-за низкой достоверности на стыках сигналов
                        figureCount++;
                        continue;
                    }
                    else
                    {
                        if (curFigure >= this.RatingsCount)
                            curFigure = (short)(this.RatingsCount - 1);
                        if (curFigure < 0)
                            continue;
                        this.Ratings[curFigure]++;
                        curFigure = -1;
                        figureCount++;
                    }
                }
            }
            InitiateConclusionRating();
        }

        void InitiateConclusionRating()
        {
            byte heartrateRatingConclusionCode = RecalculateHeartrateRatingConclusion();
            byte heartrateRatingCode = HeartrateRatingCodeCalc(heartrateRatingConclusionCode);
            string heartrateRatingTitle = GetHeartrateRatingTitle(heartrateRatingConclusionCode);
            string heartrateRatingDiscription = GetHeartrateRatingDiscription(heartrateRatingConclusionCode);
            this.CurrentHeartSystemRating = heartrateRatingConclusionCode;
            HeartrateRatingEventArgs args = new HeartrateRatingEventArgs()
            {
                HeartrateRatingCode = heartrateRatingCode,
                HeartrateRatingConclusionCode = heartrateRatingConclusionCode,
                HeartrateRatingTitle = heartrateRatingTitle,
                HeartrateRatingDiscription = heartrateRatingDiscription
            };
            OnHeartrateRatingUpdate(args);
        }

        string GetHeartrateRatingTitle(byte heartrateRatingConclusionCode)
        {
            string title = "";
            switch (heartrateRatingConclusionCode)
            {
                case 1:
                    title = "Нормально-нетренированный уровень регуляции";
                    break;
                case 2:
                    title = "Состояние функционального напряжения (I стадия срочной адаптации)";
                    break;
                case 3:
                    title = "Состояние выраженного функционального напряжения (II стадия срочной адаптации)";
                    break;
                case 4:
                    title = "Переходный уровень к долговременной адаптации (первая стадия)";
                    break;
                case 5:
                    title = "Переходный уровень к долговременной адаптации (вторая стадия)";
                    break;
                case 6:
                    title = "Уровень долговременной адаптации. Преобладание ПСНС (I стадия тренерованности)";
                    break;
                case 7:
                    title = "Уровень долговременной адаптации. Преобладание ПСНС (II стадия тренерованности)";
                    break;
                case 8:
                    title = "Состояние перенапрежения";
                    break;
                case 9:
                    title = "Стадия истощения (Дезадаптация)";
                    break;
                default:
                    title = "Состояние не определено";
                    break;
            }
            return title;
        }

        //TODO
        string GetHeartrateRatingDiscription(byte heartrateRatingConclusionCode)
        {
            string discription = "";
            switch (heartrateRatingConclusionCode)
            {
                case 1:
                    discription = "";
                    break;
                case 2:
                    discription = "";
                    break;
                case 3:
                    discription = "";
                    break;
                case 4:
                    discription = "";
                    break;
                case 5:
                    discription = "";
                    break;
                case 6:
                    discription = "";
                    break;
                case 7:
                    discription = "";
                    break;
                case 8:
                    discription = "";
                    break;
                case 9:
                    discription = "";
                    break;
                default:
                    discription = "";
                    break;
            }
            return discription;
        }

        byte RecalculateHeartrateRatingConclusion()
        {
            if (this.Ratings != null && this.Ratings.Length == this.RatingsCount && this.Ratings.Length > this.MinRatingsCount)
            {
                uint countRatingSamples = this.Sum(this.Ratings);
                if (countRatingSamples == 0) return 0;
                uint count_N0_N2 = this.Ratings[0] + this.Ratings[1] + this.Ratings[2];
                uint count_N6_Nn = 0;
                for (int i=6; i < this.Ratings.Length; i++)
                {
                    count_N6_Nn += this.Ratings[i];
                }
                return HeartrateRatingMatrix(100.0 * ((double)count_N0_N2 / countRatingSamples), 100.0 * ((double)count_N6_Nn / countRatingSamples));
            }
            return 0;            
        }

        byte HeartrateRatingCodeCalc(byte heartrateRatingConclusionCode)
        {
            byte ratingCode = 0;
            switch (heartrateRatingConclusionCode)
            {
                case 1:
                    ratingCode = 0;
                    break;
                case 2:
                    ratingCode = 0;
                    break;
                case 3:
                    ratingCode = 1;
                    break;
                case 4:
                    ratingCode = 1;
                    break;
                case 5:
                    ratingCode = 2;
                    break;
                case 6:
                    ratingCode = 2;
                    break;
                case 7:
                    ratingCode = 3;
                    break;
                case 8:
                    ratingCode = 3;
                    break;
                case 9:
                    ratingCode = 4;
                    break;
                default:
                    ratingCode = 0;
                    break;
            }
            return ratingCode;
        }

        /// <summary>
        /// Формирование рейтинга системы по матрице: 1 - 9
        /// </summary>
        /// <param name="axis_X">Значение в мастрице по горизонтальной оси</param>
        /// <param name="axis_Y">Значение в мастрице по вертикальной оси</param>
        /// <returns>Код состояния по матрице</returns>
        byte HeartrateRatingMatrix(double axis_X, double axis_Y)
        {
            byte result = 0;
            if (axis_X >= 0 && axis_X < 14.5)
            {
                if (axis_Y >= 0 && axis_Y < 2.4) result = 5;
                if (axis_Y >= 2.4 && axis_Y < 14.9) result = 4;
                if (axis_Y >= 14.9) result = 3;
            }
            if (axis_X >= 14.5 && axis_X < 35.2)
            {
                if (axis_Y >= 0 && axis_Y < 2.4) result = 6;
                if (axis_Y >= 2.4 && axis_Y < 14.9) result = 1;
                if (axis_Y >= 14.9) result = 2;
            }
            if(axis_X >= 35.2)
            {
                if (axis_Y >= 0 && axis_Y < 2.4) result = 7;
                if (axis_Y >= 2.4 && axis_Y < 14.9) result = 8;
                if (axis_Y >= 14.9) result = 9;
            }
            return result;
        }

        uint Sum(uint[] array)
        {
            if (array.Length > 0)
            {
                uint sum = 0;
                for (int i=0; i < array.Length; i++)
                {
                    sum += array[i];
                }
                return sum;
            }
            return 0;
        }

    }
}
