﻿using HealingPebblesMobile.Enumeration;
using HealingPebblesMobile.Model.ConclusionPart.ConclusionEventArgs;
using System;
using System.Collections.Generic;
using System.Text;

namespace HealingPebblesMobile.Model.ConclusionPart.HeartRateVariability
{
    /// <summary>
    /// Класс формирования заключения по индексу напряжённость регуляторных систем (индекс баевского)
    /// </summary>
    public class SystemTensionIndexController
    {
        public uint PrevTensionIndex { get; private set; } = 0;
        /// <summary>
        /// Левая граница стресса организма в %
        /// </summary>
        public byte LeftBorderControl = 0;
        /// <summary>
        /// Правая граница стресса организма в %
        /// </summary>
        public byte RightBorderControl = 100;
        /// <summary>
        /// Описание смысла индекса напряжённости регуляторных систем
        /// </summary>
        private string IndexDiscription = "Индекс напряжённости регуляторных истем отражает степень централизации управления сердечнм ритмом." +
            "Чем меньше веричина, тем больше активность парасимпатического отдела и автономного контура. " +
            "Чем больше величина индекса, тем выше активность симпатического отдела и степень централизации оправления сердечным ритмом.";

        //Черезмерная расслабленность
        private readonly ushort TooRelaxedStateLeft = 0;
        private readonly ushort TooRelaxedStateRight = 29;

        //Переменные, описывающие хорошее состояние индекса
        private readonly ushort GoodStateLeft = 30;
        private readonly ushort GoodStateRight = 120;

        //Переменные, описывающие хорошее пограничное состояние индекса
        private readonly ushort NotVeryStateLeft = 121;
        private readonly ushort NotVeryStateRight = 250;

        //Переменные, описывающие ухудшающееся состояние индекса
        private readonly ushort WarningStateLeft = 251;
        private readonly ushort WarningStateRight = 400;

        //Переменные, описывающие сложное состояние индекса
        private readonly ushort DangerStateLeft = 401;
        private readonly ushort DangerStateRight = 800;

        //Переменные, описывающие критическое состояние индекса
        private readonly ushort CriticalStateLeft = 801;


        public event EventHandler<IndexChangeOptionsEventArgs> TensionIndexChangeEvent;
        protected void OnTensionIndexChange(IndexChangeOptionsEventArgs e)
        {
            EventHandler<IndexChangeOptionsEventArgs> handler = TensionIndexChangeEvent;
            if (handler != null)
                handler(this, e);
        }


        public SystemTensionIndexController()
        {

        }

        public void UpdateTensionIndex(ushort AMo, double Mo, double DX)
        {
            if (Mo == 0 || DX == 0) return;

            uint curTensionIndexValue = (uint)((double)AMo / (2 * DX * Mo));

            IndexChangeOptionsEventArgs args = new IndexChangeOptionsEventArgs();
            if (curTensionIndexValue >= TooRelaxedStateLeft && curTensionIndexValue <= TooRelaxedStateRight)
            {
                args.HeartRateVariabilityIndex = HeartRateVariabilityIndex.NotVeryGood;
                args.ValuePercent = CalculatePercent(TooRelaxedStateLeft, TooRelaxedStateRight, 25, 35, curTensionIndexValue);
            }
            if (curTensionIndexValue >= GoodStateLeft && curTensionIndexValue <= GoodStateRight)
            {
                args.HeartRateVariabilityIndex = HeartRateVariabilityIndex.Good;
                args.ValuePercent = CalculatePercent(GoodStateLeft, GoodStateRight, 5, 25, curTensionIndexValue);
            }
            if (curTensionIndexValue >= NotVeryStateLeft && curTensionIndexValue <= NotVeryStateRight)
            {
                args.HeartRateVariabilityIndex = HeartRateVariabilityIndex.NotVeryGood;
                args.ValuePercent = CalculatePercent(NotVeryStateLeft, NotVeryStateRight, 35, 50, curTensionIndexValue);
            }
            if (curTensionIndexValue >= WarningStateLeft && curTensionIndexValue <= WarningStateRight)
            {
                args.HeartRateVariabilityIndex = HeartRateVariabilityIndex.Warning;
                args.ValuePercent = CalculatePercent(WarningStateLeft, WarningStateRight, 50, 75, curTensionIndexValue);
            }
            if (curTensionIndexValue >= DangerStateLeft && curTensionIndexValue <= DangerStateRight)
            {
                args.HeartRateVariabilityIndex = HeartRateVariabilityIndex.Danger;
                args.ValuePercent = CalculatePercent(DangerStateLeft, DangerStateRight, 75, 95, curTensionIndexValue);
            }
            if (curTensionIndexValue >= CriticalStateLeft)
            {
                args.HeartRateVariabilityIndex = HeartRateVariabilityIndex.Critical;
                args.ValuePercent = this.RightBorderControl;
            }
            args.ValueChange = (short)(curTensionIndexValue - this.PrevTensionIndex);
            this.PrevTensionIndex = curTensionIndexValue;
            this.OnTensionIndexChange(args);
        }

        private byte CalculatePercent(double leftBorder, double rightBorder, byte leftBorderPercent, byte rightBorderPercent, double realValue)
        {
            double lengthInterval = rightBorder - leftBorder;
            byte percentLength = (byte)(rightBorderPercent - leftBorderPercent);
            double percentStep = ((double)percentLength) / lengthInterval;
            double reducedValue = realValue - leftBorder;
            byte resultPercent = (byte)((reducedValue * percentStep) + leftBorderPercent);
            if (resultPercent > 100 || resultPercent < 0)
                return 100;
            return resultPercent;
        }


    }
}
