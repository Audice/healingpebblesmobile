﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HealingPebblesMobile.Model.ConclusionPart.ConclusionEventArgs
{
    public class RithmEventArgs : EventArgs
    {
        public string RithmDiscription { get; set; }
        public string RithmSmallDiscription { get; set; }
        public byte ProblemRank { get; set; } //пятибальная оценка: 0, 1, 2, 3, 4
    }
}
