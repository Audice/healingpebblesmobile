﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HealingPebblesMobile.Model.ConclusionPart.ConclusionEventArgs
{
    public class ArrethmiaEventArgs : EventArgs
    {
        public string ArrethmiaDiscription { get; set; }
        public byte ProblemRank { get; set; }
    }
}
