﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HealingPebblesMobile.Model.ConclusionPart.ConclusionEventArgs
{
    public class PulseEventArgs : EventArgs
    {
        public uint Pulse { get; set; }
        public uint MaxPulse { get; set; }
        public uint MinPulse { get; set; }
        public byte ProblemRank { get; set; }
    }
}
