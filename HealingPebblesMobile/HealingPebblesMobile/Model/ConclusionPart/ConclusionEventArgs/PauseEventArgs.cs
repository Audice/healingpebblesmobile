﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HealingPebblesMobile.Model.ConclusionPart.ConclusionEventArgs
{
    public class PauseEventArgs : EventArgs
    {
        public string PauseDiscription { get; set; }
        public uint CountFromTwoToThreeSecondsPause { get; set; }
        public uint CountMoreThreeSecondsPause { get; set; }
        public string MaxPause { get; set; }
        public byte ProblemRank { get; set; }
    }
}
