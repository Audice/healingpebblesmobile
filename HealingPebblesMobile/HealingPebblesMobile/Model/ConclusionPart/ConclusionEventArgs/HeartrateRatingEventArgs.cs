﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HealingPebblesMobile.Model.ConclusionPart.ConclusionEventArgs
{
    public class HeartrateRatingEventArgs : EventArgs
    {
        /// <summary>
        /// Код, характеризующий сложность состояния: 0 - отлично, 1 - хорошо, 2 - внимание
        /// 3 - плохо, 4 - ужасно
        /// </summary>
        public byte HeartrateRatingCode { get; set; }
        /// <summary>
        /// Заголовок текущего состояния системы в форме текста
        /// </summary>
        public string HeartrateRatingTitle { get; set; }
        /// <summary>
        /// Описание текущего состояния системы в форме текста
        /// </summary>
        public string HeartrateRatingDiscription { get; set; }
        /// <summary>
        /// Код, описывающий состояние системы по матрице:
        /// https://sportdoktor.ru/genthemes/Rusinov_Prakticheskoe_posobie_georitm_sport.html
        /// </summary>
        public byte HeartrateRatingConclusionCode { get; set; }
    }
}
