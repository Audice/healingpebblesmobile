﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace HealingPebblesMobile.Model.ConclusionPart.ConclusionEventArgs
{
    public class BodyMassIndexEventArgs : EventArgs
    {
        public string BodyMassIndexDiscription { get; set; }
        public string BodyMassIndexTitleDiscription { get; set; }
        public string BodyMassIndexRecomendation { get; set; }
        public byte BodyMassIndexRating { get; set; }
        public Color BodyMassIndexColor { get; set; }
    }
}
