﻿using HealingPebblesMobile.Enumeration;
using System;
using System.Collections.Generic;
using System.Text;

namespace HealingPebblesMobile.Model.ConclusionPart.ConclusionEventArgs
{
    public class IndexChangeOptionsEventArgs : EventArgs
    {
        public HeartRateVariabilityIndex HeartRateVariabilityIndex { get; set; }
        public byte ValuePercent { get; set; }
        public short ValueChange { get; set; }
    }
}
