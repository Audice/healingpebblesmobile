﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HealingPebblesMobile.Model.ConclusionPart.Diagnosis
{
    /// <summary>
    /// Класс описывающий диагнос и его характеристики
    /// </summary>
    public class DiagnosisDiscription : EventArgs
    {
        /// <summary>
        /// Уровень опасности диагноза
        /// </summary>
        public DiagnosisRank DiagnosisRank { get; private set; }
        /// <summary>
        /// Описание диагноза
        /// </summary>
        public string DiagnosDiscription { get; private set; }
        /// <summary>
        /// Рекомендации по диагнозу
        /// </summary>
        public string DiagnosisRecommendation { get; private set; }

        /// <summary>
        /// Малая дикрипция заболевания
        /// </summary>
        public string SmallDiagnosisDiscription { get; private set; }
        /// <summary>
        /// Спицифическая строка
        /// </summary>
        public string SpecificDiagnosisDiscription { get; set; }
        /// <summary>
        /// Тайтл диагноза
        /// </summary>
        public string DiagnosisTitle { get; private set; }

        public DiagnosisDiscription(DiagnosisRank diagnosisRank, string smallDiagnosisDiscription, string diagnosDiscription, 
            string diagnosisRecommendation, string specificDiagnosisDiscription, string diagnosisTitle)
        {
            this.DiagnosDiscription = diagnosDiscription;
            this.DiagnosisRank = diagnosisRank;
            this.DiagnosisRecommendation = diagnosisRecommendation;
            this.SmallDiagnosisDiscription = smallDiagnosisDiscription;
            this.DiagnosisTitle = diagnosisTitle;
            this.SpecificDiagnosisDiscription = specificDiagnosisDiscription;
        }

        public static bool operator==(DiagnosisDiscription first, DiagnosisDiscription second)
        {
            if (ReferenceEquals(first, null) && ReferenceEquals(second, null)) return true;
            if (ReferenceEquals(first, null) && !ReferenceEquals(second, null)) return false;
            if (!ReferenceEquals(first, null) && ReferenceEquals(second, null)) return false;

            if (first.DiagnosisRank == second.DiagnosisRank
                && first.DiagnosDiscription == second.DiagnosDiscription
                && first.DiagnosisRecommendation == second.DiagnosisRecommendation
                && first.SmallDiagnosisDiscription == second.SmallDiagnosisDiscription)
                return true;
            return false;
        }

        /// <summary>
        /// !!!!!!!!!! НЕПРАВИЛЬНАЯ ПЕРЕГРУЗКА!!!!!!!!!!!
        /// </summary>
        /// <param name="first"></param>
        /// <param name="second"></param>
        /// <returns></returns>

        public static bool operator !=(DiagnosisDiscription first, DiagnosisDiscription second)
        {
            if (ReferenceEquals(first, null) && ReferenceEquals(second, null)) return false;
            if (ReferenceEquals(first, null) && !ReferenceEquals(second, null)) return true;
            if (!ReferenceEquals(first, null) && ReferenceEquals(second, null)) return true;

            if (first.DiagnosisRank != second.DiagnosisRank
                || first.DiagnosDiscription != second.DiagnosDiscription
                || first.DiagnosisRecommendation != second.DiagnosisRecommendation
                || first.SmallDiagnosisDiscription != second.SmallDiagnosisDiscription)
                return true;
            return false;
        }

        public override bool Equals(object obj)
        {
            if (obj == null) return false;
            DiagnosisDiscription dd = obj as DiagnosisDiscription; // возвращает null если объект нельзя привести к типу Money
            if (dd == null)
                return false;

            return dd.DiagnosisRank == this.DiagnosisRank
                && dd.DiagnosDiscription == this.DiagnosDiscription
                && dd.DiagnosisRecommendation == this.DiagnosisRecommendation
                && dd.SmallDiagnosisDiscription == this.SmallDiagnosisDiscription;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}
