﻿using System;
using System.Collections.Generic;
using System.Text;
using static HealingPebblesMobile.Model.ConclusionPart.ConclusionModel;

namespace HealingPebblesMobile.Model.ConclusionPart.Diagnosis
{
    public class RhythmAnalyzer : DiagnosisAnalyzer
    {
        /// <summary>
        /// Поле nullable, отвечающее за наличие синусового ритма в текущем участке данных
        /// </summary>
        public bool? ThereSinisRithmFind { get; private set; } = null;
        /// <summary>
        /// Поле nullable, отвечающее за наличие регулярного ритма в текущем участке данных
        /// </summary>
        public bool? ThereRegularRithm { get; private set; } = null;
        /// <summary>
        /// Поле nullable, отвечающее за наличие регулярности комплекса PQ в текущем участке данных
        /// </summary>
        public bool? ThereRegularPQ { get; private set; } = null;

        public RhythmAnalyzer()
        {

        }



        /*Длина волны P не меньше 30 миллисекунд.Высота амплитуды p волны
         * по модулю не меньше(разница между началом или концом и серидиной смотря что ниже)
         * 25 микроВольт.Если таких волн больше 50% то p-волны присутствуют.
         * Если в 50% после p-волны присутствует r-зубец то p-волны нормальные. 
         * Если p-волны нормальные и присутствуют то ритм синусовы. 
         * (перед r зубцом должна быть только одна p-волна)
         */
        void FindSinusRithm(List<P_Segment> p_Segments, List<string> segmentatedMapList)
        {
            if (p_Segments == null || segmentatedMapList == null)
            {
                this.ThereSinisRithmFind = null;
                return;
            }
            try
            {
                double percentGoodPWave = 0;
                int countGoodPWave = 0;

                for (int i = 0; i < p_Segments.Count; i++)
                {
                    if (p_Segments[i].LengthComplex > 0.03)
                    {
                        if (p_Segments[i].Amplitude >= 25)
                        {
                            countGoodPWave++;
                        }
                    }
                }
                percentGoodPWave = (double)countGoodPWave / p_Segments.Count;

                int countNormalPWave = 0;
                for (int i = 0; i < segmentatedMapList.Count - 3; i++)
                {
                    if (segmentatedMapList[i + 1] == "P" && segmentatedMapList[i + 2] == "QRS")
                    {
                        if (segmentatedMapList[i] != "P")
                        {
                            countNormalPWave++;
                        }
                    }
                }
                double percentNormalPWave = (double)countNormalPWave / p_Segments.Count;
                if (percentNormalPWave >= 0.5 && percentGoodPWave > 0.5)
                {
                    this.ThereSinisRithmFind = true;
                    return;
                }
            }
            catch (Exception ex)
            {
                string ExceptionMessage = ex.Message;
                this.ThereSinisRithmFind = null;
                return;
            }
            this.ThereSinisRithmFind = false;
        }

        //Регулярный ритм
        /* Берём отношение минимального r-r интервала к среднему r-r интервалу
         * (сумма по всем, делим на количество) (минимальный.средн). 
         * Отношение больше или равно 80%. 
         * Отношение максимально r-r интервала к среднему r-r дожно быть меньше или равно 115%. 
         * Если оба условие выполнены, то ритм регулярный.
         */
        void SearchRegularRithm(double minRRinterval, double maxRRinterval, double midRRinterval)
        {
            if (midRRinterval == 0)
            {
                this.ThereRegularRithm = null;
                return;
            }
            double MinOfMid_RRinterval = 0;
            double MaxOfMid_RRinterval = 0;
            try
            {
                MinOfMid_RRinterval = minRRinterval / midRRinterval;
                MaxOfMid_RRinterval = maxRRinterval / midRRinterval;
                if (MinOfMid_RRinterval >= 0.8 && MaxOfMid_RRinterval <= 1.15)
                    this.ThereRegularRithm = true;
                else
                    this.ThereRegularRithm = false;
            }
            catch (Exception ex)
            {
                string ExceptionMessage = ex.Message;
                this.ThereRegularRithm = null;
            }
        }

        /*
         * По всем PQ интервалам. По каждому делаем проверку:
         * Если его значение отличается больше чем на 40% от среднего PQ, то считаем его выпадающим,
         * Если количество выпадающих PQ интервалов больше 60% от общего числа, то говорим что у нас не регулярные PQ 
         * интервалы. И если синусовый ритм и регулярные PQ, то можно диагностировать тахи, бради, 
         *
         */
        void RegularPQ(List<P_Segment> p_Segments, List<QRS_Segment> qrs_Segments, List<string> segmentatedMapList)
        {
            try
            {
                List<double> listPQ = new List<double>();
                int indexP = 0;
                int indexR = 0;
                List<double> tmpP = new List<double>();
                List<double> tmpQ = new List<double>();
                for (int i = 0; i < segmentatedMapList.Count - 2; i++)
                {
                    if (segmentatedMapList[i] == "P" && segmentatedMapList[i + 1] == "QRS")
                    {
                        tmpP.Add(p_Segments[indexP].StartP);
                        tmpQ.Add(qrs_Segments[indexR].Q_TimeStamp);
                        indexP++;
                        indexR++;
                        i += 1;
                        continue;
                    }
                    if (segmentatedMapList[i] == "P" && segmentatedMapList[i + 1] != "QRS")
                    {
                        tmpP.Add(p_Segments[indexP].StartP);
                        indexP++;
                        tmpQ.Add(-1);
                    }
                    if (segmentatedMapList[i] != "P" && segmentatedMapList[i + 1] == "QRS")
                    {
                        tmpP.Add(-1);
                        tmpQ.Add(qrs_Segments[indexR].Q_TimeStamp);
                        indexR++;
                        i += 1;
                        continue;
                    }
                    if (segmentatedMapList[i] == "QRS")
                    {
                        tmpP.Add(-1);
                        tmpQ.Add(qrs_Segments[indexR].Q_TimeStamp);
                        indexR++;
                    }
                }
                int lengthResultedTMP = Math.Min(tmpP.Count, tmpQ.Count);
                double sum = 0.0;
                int countGood = 0;
                for (int i = 0; i < lengthResultedTMP; i++)
                {
                    if (tmpQ[i] > 0 && tmpP[i] > 0)
                    {
                        listPQ.Add(tmpQ[i] - tmpP[i]);
                        sum += tmpQ[i] - tmpP[i];
                        countGood++;
                    }
                }
                double MidPQ = sum / countGood;
                int Count_PQ_Out = 0;
                for (int i = 0; i < listPQ.Count; i++)
                {
                    if (listPQ[i] >= MidPQ + MidPQ * 0.4 || listPQ[i] <= MidPQ - MidPQ * 0.4)
                    {
                        Count_PQ_Out++;
                    }
                }
                if (Count_PQ_Out / listPQ.Count >= 0.6)
                    this.ThereRegularPQ = false;     //Нерегулярный PQ интервал
                else
                    this.ThereRegularPQ = true;      //Регулярный PQ интервал
            }
            catch (Exception ex)
            {
                string ExceptionMessage = ex.Message;
                this.ThereRegularPQ = null;
            }
        }

        /* Если ритм синусовый, регулярный и пульс меньше 50 
         * ставим ритм синусовый, регулярный, брадикардия.
         * Если ритм синусовый, регулярный и пульс больше 90, 
         * то ставим ритм синусовый, регулярный, тахикардию.
         * Если ритм синусовый, регулярный и пульс от 50 до 90
         * то диагноз ритм синусовый, регулярный. 
         * Если синусовый, но не регулярный, то ставим синусовую аритмию.
         */
        private void DiagnosisRithmType(PulseAnalyzer pulseAnalyzer)
        {
            if (this.ThereRegularPQ == null || this.ThereRegularRithm == null || this.ThereSinisRithmFind == null 
                || pulseAnalyzer.Pulse == null)
                return;

            try
            {
                string discription = "";
                string smalDiscription = "";
                DiagnosisRank rithmRank = DiagnosisRank.Good;
                bool isSinusRithm = this.ThereSinisRithmFind.Value;
                bool isRegularRithm = this.ThereRegularRithm.Value;

                double pulse = pulseAnalyzer.Pulse.Value;

                bool isRegularPQ = this.ThereRegularPQ.Value;
                if (isSinusRithm && isRegularRithm && pulse <= 50.0 && isRegularPQ)
                {
                    discription = "Ритм синусовый, регулярный, брадикардия. ";
                    smalDiscription = "Ритм синусовый, брадикардия";
                    rithmRank = DiagnosisRank.Good;
                }
                if (isSinusRithm && isRegularRithm && pulse >= 90.0 && isRegularPQ)
                {
                    discription = "Ритм синусовый, регулярный, тахикардия. ";
                    smalDiscription = "Ритм синусовый, тахикардия";
                    rithmRank = DiagnosisRank.Good;
                }
                if (isSinusRithm && isRegularRithm && pulse < 90.0 && pulse > 50.0 && isRegularPQ)
                {
                    discription = "Ритм синусовый, регулярный. ";
                    smalDiscription = discription;
                    rithmRank = DiagnosisRank.VeryGood;
                }
                if (isSinusRithm && !isRegularRithm && isRegularPQ)
                {
                    discription = "Cинусовая аритмия. ";
                    smalDiscription = discription;
                    rithmRank = DiagnosisRank.Warning;
                }
                if (!isSinusRithm && isRegularPQ)
                {
                    discription = "Несинусовый ритм. ";
                    smalDiscription = discription;
                    rithmRank = DiagnosisRank.Warning;
                }

                DiagnosisDiscription diagnosisDiscription = new DiagnosisDiscription(rithmRank, smalDiscription, discription, "", "", "" );
                this.OnDiagnosisUpdate(diagnosisDiscription);
            }
            catch (Exception ex)
            {
                string ExceptionMessage = ex.Message;
            }
        }
        //Многовато параметров...
        public void DiagnosticProcessing(P_SegmentCollection p_SegmentCol, QRS_SegmentCollection qrs_SegmentCol, 
            List<string> segmentatedMapList, PulseAnalyzer pulseAnalyzer)
        {
            FindSinusRithm(p_SegmentCol.P_Segments, segmentatedMapList);
            RegularPQ(p_SegmentCol.P_Segments, qrs_SegmentCol.QRS_Segments, segmentatedMapList);
            SearchRegularRithm(qrs_SegmentCol.MinRRinterval, qrs_SegmentCol.MaxRRinterval, qrs_SegmentCol.MidRRinterval);
            if (pulseAnalyzer != null)
                DiagnosisRithmType(pulseAnalyzer);
        }
    }
}
