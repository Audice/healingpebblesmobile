﻿using HealingPebblesMobile.Model.BluetoothModel.CustomEventArgsClasses;
using HealingPebblesMobile.Model.BluetoothPart.CustomEventArgsClasses;
using HealingPebblesMobile.ViewModel;
using SkiaSharp;
using SkiaSharp.Views.Forms;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HealingPebblesMobile.Classes
{
    public class SkiaPaintController : IDisposable
    {
        /// <summary>
        /// Буфер для хранения сигнала, предназначенный для отрисовки ЭКГ
        /// </summary>
        private List<short> ECGData = new List<short>();
        SKCanvasView GeneralCanvas;

        byte countSeconds = 1;
        double ecgStep = 1;

        public SkiaPaintController(SKCanvasView generalCanvas, HolterECGViewModel holterECGViewModel,
            string cutedName, short sampleFreq)
        {
            this.GeneralCanvas = generalCanvas;
            this.SamplingFrequancy = sampleFreq;
            //Заполняем информацию о пациенте
            this.CutedName = cutedName;
            this.GeneralCanvas.PaintSurface += OnCanvasViewPaintSurface;
            holterECGViewModel.PackageCameEvent += NewPartReady;
            
        }

        private void NewPartReady(object sender, SignalReadyEventArgs e)
        {
            ECGData.AddRange(e.Data);
            if (!IsDrawECGStart)
            {
                IsDrawECGStart = true;
                this.GeneralCanvas.InvalidateSurface();
            }
        }

        public bool IsDrawECGStart
        {
            get; private set;
        } = false;

        /// <summary>
        /// Метод, срабатывающий при обновлении канвы или вызове InvalidateSurface
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        void OnCanvasViewPaintSurface(object sender, SKPaintSurfaceEventArgs args)
        {
            UpdateCanvas(args);
            if (this.ECGData.Count > 0)
                this.GeneralCanvas.InvalidateSurface();
        }

        int Height
        {
            get; set;
        } = 0;

        /// <summary>
        /// Обновление canvas, в соответствии с текущими требованиями отображения
        /// </summary>
        /// <param name="args"></param>
        public void UpdateCanvas(SKPaintSurfaceEventArgs args)
        {
            SKImageInfo info = args.Info; //Информация о дисплее смартфона. Получаем ширину и высоту канваса именно отсюда
            SKCanvas canvas = args.Surface.Canvas; //Canvas for drawing  
            canvas.Clear(); //Каждое новое обновление - очищаем канву
            this.ecgStep = (double)(info.Width / this.SamplingFrequancy);
            this.Height = info.Height;
            GeneralDraw(canvas);
        }

        private int StartIndexDrawnData
        {
            get; set;
        } = 0;

        private void GeneralDraw(SKCanvas canvas)
        {
            try
            {
                if (this.ECGData != null && ECGData.Count > 0)
                {

                    //Построение ЭКГ линии общего вида
                    SKPath renderedLinesECG = DrawGeneralLine(this.StartIndexDrawnData);
                    //Добавление линии общего представления сигнала на канвас
                    this.DrawSignal(canvas, renderedLinesECG, SKColors.Black, 5);

                    this.GeneralCanvas.InvalidateSurface();
                }
            }
            catch (Exception ex)
            {
                string exM = ex.Message;
            }
        }

        private void DrawSignal(SKCanvas canvas, SKPath path, SKColor pathColor, float pathWidth = 5)
        {
            using (SKPaint generalPaint = new SKPaint())
            {
                generalPaint.Style = SKPaintStyle.Stroke;
                generalPaint.Color = pathColor;
                generalPaint.StrokeWidth = pathWidth;
                canvas.DrawPath(path, generalPaint);
            }
        }

        private SKPath DrawGeneralLine(int startIndex)
        {
            SKPath renderedGeneralLinesECG = new SKPath();
            int endIndex = startIndex + this.SamplingFrequancy > this.ECGData.Count ? 
                startIndex + (this.ECGData.Count - startIndex) : startIndex + this.SamplingFrequancy;


            short maxVal = short.MinValue;
            short minVal = short.MaxValue;
            for (int i= startIndex; i < endIndex; i++)
            {
                if (this.ECGData[i] < minVal)
                    minVal = this.ECGData[i];
                if (this.ECGData[i] > maxVal)
                    maxVal = this.ECGData[i];
            }

            double scaleCoef = (double)this.Height / (3000);
            double startPoint = (double)this.Height;



            renderedGeneralLinesECG.MoveTo(0, (float)(startPoint - scaleCoef * this.ECGData[startIndex]));

            double xstep = 0;
            for (int i = startIndex; i < endIndex; i++)
            {
                renderedGeneralLinesECG.LineTo((float)xstep,
                    (float)(startPoint - scaleCoef * this.ECGData[i]));
                xstep += this.ecgStep;
            }
            if (this.StartIndexDrawnData + 5 < this.ECGData.Count)
                this.StartIndexDrawnData += 5;
            return renderedGeneralLinesECG;
        }


        int RecomendedSize(List<List<short>> data)
        {
            List<int> sizes = new List<int>();
            for (int i = 0; i < data.Count; i++)
                sizes.Add(data[i].Count);
            return sizes.Min();
        }
        

        //Информаия о пациенте
        #region
        /// <summary>
        /// Имя обследуемого
        /// </summary>
        private string CutedName = "";
        /// <summary>
        /// Дата рождения пациента
        /// </summary>
        private string BornDate = "Дата рожд. ";
        /// <summary>
        /// Рост пациента
        /// </summary>
        private string Growth = "Рост: ";
        /// <summary>
        /// Вес пациента
        /// </summary>
        private string Weight = "Вес: ";
        /// <summary>
        /// Дата и время анализа
        /// </summary>
        private string DateAnalisys = "";
        /// <summary>
        /// Размер текста, рачитанный по максимальному количеству символов
        /// </summary>
        private float? TextSize = null;
        /// <summary>
        /// Масштабирующий коэфицент для сжатия текста
        /// </summary>
        private float ScaleCoef = 0.5f;
        #endregion


        /// <summary>
        /// Максимальное значение для индексатора визуализиремых данных
        /// </summary>
        public int CounterMaxValue
        {
            get;
            private set;
        }
        public bool IsDrawEnd
        {
            get;
            set;
        }
        public void SetEndDrawFlag()
        {
            IsDrawEnd = true;
        }
        /// <summary>
        /// Изменение максимального индекса данных для отрисовки
        /// </summary>
        /// <param name="maxValue">Новое значение максимального индекса</param>
        public void SetCounterMaxValue(int maxValue)
        {
            this.CounterMaxValue = maxValue;
        }
        public int Counter
        {
            get; set;
        }
        public int WaitTimeBetwienDraw
        {
            get; private set;
        }
        public int CountDrawData
        {
            get; private set;
        }

        //Private fields
        private short SamplingFrequancy = -1;


        
        
        private float CalculateTextSize(int canvasWidth, int canvasHeight)
        {
            int headCanvasSize = canvasWidth >= canvasHeight ? canvasHeight : canvasWidth;
            int sizeCutedName = this.CutedName.Length;
            string headSymbolString = this.CutedName.Length >= this.BornDate.Length ? this.CutedName : this.BornDate;
            SKPaint textPaint = new SKPaint();
            float textWidth = textPaint.MeasureText(headSymbolString);
            return 0.5f * headCanvasSize * textPaint.TextSize / textWidth;
        }

/*
        

        private void DrawPatientInformation(SKCanvas canvas, int canvasWidth, int canvasHeight, float textSize)
        {
            if (this.TextSize != null)
            {
                SKPaint cutedNamePaint = new SKPaint();
                cutedNamePaint.TextSize = this.TextSize.Value;
                cutedNamePaint.IsAntialias = true;
                cutedNamePaint.Color = SKColors.Black;
                cutedNamePaint.IsStroke = false;
                cutedNamePaint.TextAlign = SKTextAlign.Center;
                cutedNamePaint.FakeBoldText = true;
                cutedNamePaint.TextScaleX = this.ScaleCoef;
                canvas.DrawText(this.CutedName, canvasWidth / 4, cutedNamePaint.TextSize * 1.5f, cutedNamePaint);

                SKPaint dateBornPaint = new SKPaint();
                dateBornPaint.TextSize = this.TextSize.Value;
                dateBornPaint.IsAntialias = true;
                dateBornPaint.Color = SKColors.Black;
                dateBornPaint.IsStroke = false;
                dateBornPaint.TextAlign = SKTextAlign.Center;
                dateBornPaint.FakeBoldText = true;
                dateBornPaint.TextScaleX = this.ScaleCoef;
                canvas.DrawText(this.BornDate, 3f * canvasWidth / 4, dateBornPaint.TextSize, dateBornPaint);

                SKPaint growthWeightPaint = new SKPaint();
                growthWeightPaint.TextSize = this.TextSize.Value;
                growthWeightPaint.IsAntialias = true;
                growthWeightPaint.Color = SKColors.Black;
                growthWeightPaint.IsStroke = false;
                growthWeightPaint.TextAlign = SKTextAlign.Center;
                growthWeightPaint.FakeBoldText = true;
                growthWeightPaint.TextScaleX = this.ScaleCoef;
                canvas.DrawText(this.Growth + "  " + this.Weight, 3f * canvasWidth / 4, 2 * dateBornPaint.TextSize, dateBornPaint);

                SKPaint dateAnalisysPaint = new SKPaint();
                dateAnalisysPaint.TextSize = this.TextSize.Value;
                dateAnalisysPaint.IsAntialias = true;
                dateAnalisysPaint.Color = SKColors.Black;
                dateAnalisysPaint.IsStroke = false;
                dateAnalisysPaint.TextAlign = SKTextAlign.Center;
                dateAnalisysPaint.FakeBoldText = true;
                dateAnalisysPaint.TextScaleX = this.ScaleCoef;
                canvas.DrawText(this.DateAnalisys, canvasWidth / 2, (float)canvasHeight, dateAnalisysPaint);

            }
        }


        /*
        private void DrawECGNetwork(SKCanvas canvas, int currentWidth, int currentHeight, short samplingFrequancy)
        {
            //Calculate network step
            int maxSeconds = currentWidth / samplingFrequancy;
            if (maxSeconds < 1)
                maxSeconds = 1;
            maxSeconds = maxSeconds * 1000; //Transform seconds to milliseconds
            float sizeMillisecond = (float)currentWidth / maxSeconds;

            using (SKPaint paint = new SKPaint())
            {
                paint.Style = SKPaintStyle.Stroke;
                paint.Color = SKColors.LightGray;
                paint.StrokeWidth = 1;
                int countMilliseconds = 0;
                //Draw vertical line
                for (float i = 0; i <= currentWidth; i += SizeSmallDropcost * sizeMillisecond)
                {
                    if (countMilliseconds % SizeLargeDropCost == 0)
                        paint.StrokeWidth = 2;
                    else
                        paint.StrokeWidth = 1;
                    canvas.DrawLine(i, 0, i, currentHeight, paint);
                    countMilliseconds += SizeSmallDropcost;
                }
                //Draw horizontal line
                countMilliseconds = 0;
                for (float i = 0; i <= currentHeight; i += SizeSmallDropcost * sizeMillisecond)
                {
                    if (countMilliseconds % SizeLargeDropCost == 0)
                        paint.StrokeWidth = 2;
                    else
                        paint.StrokeWidth = 1;
                    canvas.DrawLine(0, i, currentWidth, i, paint);
                    countMilliseconds += SizeSmallDropcost;
                }
            }
        }
        */






        public void Dispose()
        {

        }
    }
}

