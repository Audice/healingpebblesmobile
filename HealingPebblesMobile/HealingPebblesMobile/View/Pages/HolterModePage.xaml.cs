﻿using HealingPebblesMobile.View.Pages.DiagnosisPage;
using HealingPebblesMobile.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace HealingPebblesMobile.View.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class HolterModePage : TabbedPage
    {
        HolterECGViewModel HolterECGViewModel = null;
        public HolterModePage()
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
            HolterECGViewModel = new HolterECGViewModel();
            Children.Add(new ECGPage(HolterECGViewModel));
            Children.Add(new DiagnosisPage.DiagnosisPage(HolterECGViewModel));
            Children.Add(new BaevskiyPage(HolterECGViewModel));
        }
    }
}