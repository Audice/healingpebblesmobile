﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace HealingPebblesMobile.View.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class LoginPage : ContentPage
    {
        double width = 0;
        double height = 0;

        public LoginPage()
        {
            InitializeComponent(); 
            NavigationPage.SetHasNavigationBar(this, false);
        }

        private async void DemoView(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new MonitoringTabbedPage());
        }

        protected override void OnSizeAllocated(double width, double height)
        {
            base.OnSizeAllocated(width, height);

            if (width != this.width || height != this.height)
            {
                this.width = width;
                this.height = height;

                PasswordField.WidthRequest = LogoImage.Width * 2;
                LoginField.WidthRequest = LogoImage.Width * 2;
                PasswordField.MinimumWidthRequest = LogoImage.Width * 2;
                LoginField.MinimumWidthRequest = LogoImage.Width * 2;

                LoginButton.WidthRequest = LogoImage.Width * 1.3;
                DemoButton.WidthRequest = LogoImage.Width * 1.3;
                LoginButton.MinimumWidthRequest = LogoImage.Width * 1.3;
                DemoButton.MinimumWidthRequest = LogoImage.Width * 1.3;


                if (width > height)
                {
                    HeadStack.Orientation = StackOrientation.Horizontal;
                    this.LogoStack.VerticalOptions = LayoutOptions.FillAndExpand;
                    this.LogoStack.HorizontalOptions = LayoutOptions.FillAndExpand;
                    this.LogoImage.VerticalOptions = LayoutOptions.CenterAndExpand;
                    this.LogoImage.HorizontalOptions = LayoutOptions.CenterAndExpand;
                    this.FrameStack.VerticalOptions = LayoutOptions.CenterAndExpand;
                    this.FrameStack.HorizontalOptions = LayoutOptions.CenterAndExpand;
                }
                else
                {
                    HeadStack.Orientation = StackOrientation.Vertical;
                    this.LogoStack.VerticalOptions = LayoutOptions.EndAndExpand;
                    this.LogoStack.HorizontalOptions = LayoutOptions.FillAndExpand;
                    this.LogoImage.VerticalOptions = LayoutOptions.EndAndExpand;
                    this.LogoImage.HorizontalOptions = LayoutOptions.CenterAndExpand;
                    this.FrameStack.VerticalOptions = LayoutOptions.StartAndExpand;
                    this.FrameStack.HorizontalOptions = LayoutOptions.FillAndExpand;
                }
            }

        }
    }
}