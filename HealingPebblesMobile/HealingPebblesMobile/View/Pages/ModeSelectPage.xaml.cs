﻿using HealingPebblesMobile.ViewModel;
using HealingPebblesMobile.Model.BluetoothPart;
using HealingPebblesMobile.Model.BluetoothPart.CustomEventArgsClasses;
using HealingPebblesMobile.Model.ServerPart;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace HealingPebblesMobile.View.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ModeSelectPage : ContentPage
    {
        FireFly FireFly;
        
        public ModeSelectPage()
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
            this.IconImageSource = "ecgIcon.png";
            this.ECGMonitoringMode.WidthRequest = App.ScreenWidth / 2;
            this.BindingContext = new ModeSelectViewModel() { Navigation = this.Navigation };
        }
    }
}