﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace HealingPebblesMobile.View.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MonitoringTabbedPage : TabbedPage
    {
        public MonitoringTabbedPage()
        {
            NavigationPage.SetHasNavigationBar(this, false);
            NavigationPage.SetHasBackButton(this, true);
            InitializeComponent();
            Children.Add(new SettingsPage());
            Children.Add(new ModeSelectPage());
        }
    }
}