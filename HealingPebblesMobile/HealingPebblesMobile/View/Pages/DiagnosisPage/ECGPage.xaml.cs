﻿using HealingPebblesMobile.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace HealingPebblesMobile.View.Pages.DiagnosisPage
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ECGPage : ContentPage
    {
        HolterECGViewModel HolterECGViewModel = null;
        public ECGPage(HolterECGViewModel holterECGViewModel)
        {
            InitializeComponent();
            Title = "ЭКГ";
            HolterECGViewModel = holterECGViewModel;
            HolterECGViewModel.SetCanvas(this.GeneralCanvasView);
        }
    }
}