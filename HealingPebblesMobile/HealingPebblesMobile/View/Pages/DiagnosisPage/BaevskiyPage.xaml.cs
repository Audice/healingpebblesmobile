﻿using HealingPebblesMobile.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace HealingPebblesMobile.View.Pages.DiagnosisPage
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class BaevskiyPage : ContentPage
    {
        HolterECGViewModel HolterECGViewModel = null;
        public BaevskiyPage(HolterECGViewModel holterECGViewModel)
        {
            InitializeComponent();
            Title = "Оценка стресса";
            HolterECGViewModel = holterECGViewModel;
            this.BindingContext = HolterECGViewModel;
        }
    }
}