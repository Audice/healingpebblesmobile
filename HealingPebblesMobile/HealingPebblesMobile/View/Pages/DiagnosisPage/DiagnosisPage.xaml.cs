﻿using HealingPebblesMobile.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace HealingPebblesMobile.View.Pages.DiagnosisPage
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class DiagnosisPage : ContentPage
    {
        HolterECGViewModel HolterECGViewModel = null;
        public DiagnosisPage(HolterECGViewModel holterECGViewModel)
        {
            InitializeComponent();
            Title = "Диагноз";
            HolterECGViewModel = holterECGViewModel;
            this.BindingContext = HolterECGViewModel;
        }

        protected override void OnSizeAllocated(double width, double height)
        {
            base.OnSizeAllocated(width, height);
            if (this.HolterECGViewModel != null)
            {
                uint actualHeight = (uint)(height - 100);
                this.HolterECGViewModel.UdateFramesSize(width, actualHeight);
            }
        }
    }
}