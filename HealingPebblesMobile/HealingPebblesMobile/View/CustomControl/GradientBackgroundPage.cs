﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace HealingPebblesMobile.View.CustomControl
{
    public class GradientBackgroundPage : StackLayout
    {
        public Xamarin.Forms.Color StartColor { get; set; }
        public Xamarin.Forms.Color EndColor { get; set; }
    }
}
