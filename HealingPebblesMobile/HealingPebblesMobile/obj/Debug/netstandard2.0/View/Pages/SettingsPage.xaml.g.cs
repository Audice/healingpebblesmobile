//------------------------------------------------------------------------------
// <auto-generated>
//     Этот код создан программой.
//     Исполняемая версия:4.0.30319.42000
//
//     Изменения в этом файле могут привести к неправильной работе и будут потеряны в случае
//     повторной генерации кода.
// </auto-generated>
//------------------------------------------------------------------------------

[assembly: global::Xamarin.Forms.Xaml.XamlResourceIdAttribute("HealingPebblesMobile.View.Pages.SettingsPage.xaml", "View/Pages/SettingsPage.xaml", typeof(global::HealingPebblesMobile.View.Pages.SettingsPage))]

namespace HealingPebblesMobile.View.Pages {
    
    
    [global::Xamarin.Forms.Xaml.XamlFilePathAttribute("View\\Pages\\SettingsPage.xaml")]
    public partial class SettingsPage : global::Xamarin.Forms.ContentPage {
        
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Xamarin.Forms.Build.Tasks.XamlG", "2.0.0.0")]
        private global::HealingPebblesMobile.View.CustomControl.ElypsedEntry UserSurnameEntry;
        
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Xamarin.Forms.Build.Tasks.XamlG", "2.0.0.0")]
        private global::HealingPebblesMobile.View.CustomControl.ElypsedEntry UserNameEntry;
        
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Xamarin.Forms.Build.Tasks.XamlG", "2.0.0.0")]
        private global::HealingPebblesMobile.View.CustomControl.ElypsedEntry UserPatronomycEntry;
        
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Xamarin.Forms.Build.Tasks.XamlG", "2.0.0.0")]
        private global::HealingPebblesMobile.View.CustomControl.ElypsedEntry UserGrowthEntry;
        
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Xamarin.Forms.Build.Tasks.XamlG", "2.0.0.0")]
        private global::HealingPebblesMobile.View.CustomControl.ElypsedEntry UserWeightEntry;
        
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Xamarin.Forms.Build.Tasks.XamlG", "2.0.0.0")]
        private global::HealingPebblesMobile.View.CustomControl.ElypsedPicker UserGender;
        
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Xamarin.Forms.Build.Tasks.XamlG", "2.0.0.0")]
        private global::HealingPebblesMobile.View.CustomControl.ElypsedDatePicker UserDateBorn;
        
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Xamarin.Forms.Build.Tasks.XamlG", "2.0.0.0")]
        private global::HealingPebblesMobile.View.CustomControl.ElypsedButton RegistrationButton;
        
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Xamarin.Forms.Build.Tasks.XamlG", "2.0.0.0")]
        private void InitializeComponent() {
            global::Xamarin.Forms.Xaml.Extensions.LoadFromXaml(this, typeof(SettingsPage));
            UserSurnameEntry = global::Xamarin.Forms.NameScopeExtensions.FindByName<global::HealingPebblesMobile.View.CustomControl.ElypsedEntry>(this, "UserSurnameEntry");
            UserNameEntry = global::Xamarin.Forms.NameScopeExtensions.FindByName<global::HealingPebblesMobile.View.CustomControl.ElypsedEntry>(this, "UserNameEntry");
            UserPatronomycEntry = global::Xamarin.Forms.NameScopeExtensions.FindByName<global::HealingPebblesMobile.View.CustomControl.ElypsedEntry>(this, "UserPatronomycEntry");
            UserGrowthEntry = global::Xamarin.Forms.NameScopeExtensions.FindByName<global::HealingPebblesMobile.View.CustomControl.ElypsedEntry>(this, "UserGrowthEntry");
            UserWeightEntry = global::Xamarin.Forms.NameScopeExtensions.FindByName<global::HealingPebblesMobile.View.CustomControl.ElypsedEntry>(this, "UserWeightEntry");
            UserGender = global::Xamarin.Forms.NameScopeExtensions.FindByName<global::HealingPebblesMobile.View.CustomControl.ElypsedPicker>(this, "UserGender");
            UserDateBorn = global::Xamarin.Forms.NameScopeExtensions.FindByName<global::HealingPebblesMobile.View.CustomControl.ElypsedDatePicker>(this, "UserDateBorn");
            RegistrationButton = global::Xamarin.Forms.NameScopeExtensions.FindByName<global::HealingPebblesMobile.View.CustomControl.ElypsedButton>(this, "RegistrationButton");
        }
    }
}
