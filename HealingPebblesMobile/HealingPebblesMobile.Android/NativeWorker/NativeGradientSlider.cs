﻿using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using HealingPebblesMobile.Droid.NativeWorker;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xamarin.Forms;
using HealingPebblesMobile.View.CustomControl;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(GradientSlider), typeof(NativeGradientSlider))]
namespace HealingPebblesMobile.Droid.NativeWorker
{
    [Obsolete]
    public class NativeGradientSlider : SliderRenderer
    {
        protected override void
                 OnElementChanged(ElementChangedEventArgs<Slider> e)
        {
            base.OnElementChanged(e);
            if (e.NewElement != null)
            {
                // Привязка многоуровнего стиля градиента
                Control.SetProgressDrawableTiled(
                Resources.GetDrawable(
                Resource.Drawable.custom_progressbar_style,
                (this.Context).Theme));

                Control.SplitTrack = false;
                // Привязка стиля курсора
                Control.SetThumb(Resources.GetDrawable(Resource.Drawable.custom_seekbar_thumb));
                // Отключаем действия над градиентным слайдером
                Control.SetOnTouchListener(new MyTouchListener());
            }
        }
    }

    public class MyTouchListener : Java.Lang.Object, Android.Views.View.IOnTouchListener
    {
        // Слушатель заглушка
        public bool OnTouch(Android.Views.View v, MotionEvent e)
        {
            return true;
        }
    }
}