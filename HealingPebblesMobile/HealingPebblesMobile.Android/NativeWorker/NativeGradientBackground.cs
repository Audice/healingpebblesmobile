﻿using HealingPebblesMobile.Droid;
using HealingPebblesMobile.View.CustomControl;
using System;

using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(GradientBackgroundPage), typeof(GradientBackgroundPageRenderer))]
namespace HealingPebblesMobile.Droid
{
#pragma warning disable CS0618 // Тип или член устарел
    public class GradientBackgroundPageRenderer : VisualElementRenderer<StackLayout>
    {
        private Color StartColor { get; set; }
        private Color EndColor { get; set; }

        protected override void DispatchDraw(global::Android.Graphics.Canvas canvas)
        {
            #region for Vertical Gradient
            
            var gradient = new Android.Graphics.LinearGradient(0, 0, 0, Height,
            #endregion

                   this.StartColor.ToAndroid(),
                   this.EndColor.ToAndroid(),
                   Android.Graphics.Shader.TileMode.Mirror);

            var paint = new Android.Graphics.Paint()
            {
                Dither = true,
            };



            var paint2 = new Android.Graphics.Paint()
            {
                Color = Color.Black.ToAndroid(),
                Dither = true,
                StrokeWidth = 10
            };


            paint.SetShader(gradient);
            canvas.DrawPaint(paint);
             //Придумать отрисовку ЭКГ
            //canvas.DrawLine(0, 0, Width, Height, paint2);

            base.DispatchDraw(canvas);
        }

        protected override void OnElementChanged(ElementChangedEventArgs<StackLayout> e)
        {
            base.OnElementChanged(e);

            if (e.OldElement != null || Element == null)
            {
                return;
            }
            try
            {
                var stack = e.NewElement as GradientBackgroundPage;
                this.StartColor = stack.StartColor;
                this.EndColor = stack.EndColor;
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(@"ERROR:", ex.Message);
            }
        }
    }
#pragma warning restore CS0618 // Тип или член устарел
}