﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Java.Util;
using Android.App;
using Android.Bluetooth;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Xamarin.Forms;
using System.Threading.Tasks;
using System.Threading;
using HealingPebblesMobile.Model.BluetoothPart;

[assembly: Dependency(typeof(HealingPebblesMobile.Droid.NativeWorker.BluetoothController))]
namespace HealingPebblesMobile.Droid.NativeWorker
{
    public class BluetoothController : IBluetoothController
    {
        //Будем хранить локальный единственный экземпляр сокета
        private BluetoothSocket CurrentBluetoothSocket = null;
        /// <summary>
        /// Массив байт, характеризующий правильность взаимодействия с устройством
        /// </summary>
        readonly byte[] RightResponse = new byte[] { 79, 75, 13, 10 };

        //Размер буффера для чтения
        private const int ResponseSize = 512;
        private const int ResponseArraySize = 2048;

        

        private bool stateBluetoothConnect = false;

        public event ChangeStateBluetoothConnectHandler ChangeStateBluetoothConnectEvent;

        public bool StateBluetoothConnect
        {
            get { return stateBluetoothConnect; }
            private set
            {
                stateBluetoothConnect = value;
                ChangeStateBluetoothConnectEvent?.Invoke();
            }
        }


        public bool BreakBluetoothConnect()
        {
            CurrentBluetoothSocket.Close();
            this.StateBluetoothConnect = false;
            return !this.StateBluetoothConnect;
        }

        public bool CloseCurrentBluetoothConnect()
        {
            if (CurrentBluetoothSocket != null)
            {
                try
                {
                    this.CurrentBluetoothSocket.Close();
                    //this.CurrentBluetoothSocket.Dispose();
                    this.CurrentBluetoothSocket = null;
                    this.StateBluetoothConnect = false;
                    return true;
                }
                catch (Exception ex)
                {
                    string ExceptionMessage = ex.Message;
                    this.CurrentBluetoothSocket = null;
                    this.StateBluetoothConnect = false;
                    return false;
                }
            }
            else
            {
                this.StateBluetoothConnect = false;
                return true;
            }
        }

        public bool GetBluetoothConnectState()
        {
            return this.StateBluetoothConnect;
        }

        public bool CheckBluetoothModule()
        {
            if (BluetoothAdapter.DefaultAdapter != null) return true;
            return false;
        }

        public List<Tuple<string, string>> GetListDevices(string deviceName)
        {
            List<Tuple<string, string>> resultDevicesList = new List<Tuple<string, string>>();
            try
            {
                //List<BluetoothDevice> bindedDevices = new List<BluetoothDevice>(BluetoothAdapter.DefaultAdapter.BondedDevices);
                foreach (BluetoothDevice device in BluetoothAdapter.DefaultAdapter.BondedDevices)
                    if (deviceName == device.Name)
                        resultDevicesList.Add(new Tuple<string, string>(device.Name, device.Address.ToString()));
            }
            catch (Exception exception)
            {
                string ExceptionMessage = exception.Message;
            }
            return resultDevicesList;
        }
        
        public async Task<int> ConnectBluetooth(string MAC)
        {
            try
            {
                BluetoothDevice currentDevice = GetBindedDivece(MAC);
                if (currentDevice != null)
                {
                    if (CurrentBluetoothSocket == null)
                    {
                        //При отсутствии экземпляра сокета, создаём его
                        UUID uuidForFireFlySPP = UUID.FromString("00001101-0000-1000-8000-00805F9B34FB");
                        try
                        {
                            CurrentBluetoothSocket = currentDevice.CreateRfcommSocketToServiceRecord(uuidForFireFlySPP);
                        }
                        catch (Exception exeption)
                        {
                            CurrentBluetoothSocket.Dispose();
                            CurrentBluetoothSocket = null;
                            StateBluetoothConnect = false;
                            string exceptionMessage = exeption.Message;
                        }
                        if (CurrentBluetoothSocket == null) return -2; //Невозможно создать Bluetooth Socket
                        try
                        {
                            //await CurrentBluetoothSocket.ConnectAsync();
                            CurrentBluetoothSocket.Connect();
                            await Task.Delay(2);
                        }
                        catch (Exception exception)
                        {
                            StateBluetoothConnect = false;
                            string exceptionMessage = exception.Message;
                        }
                        if (!CurrentBluetoothSocket.IsConnected)
                        {
                            CloseBluetoothConnection();
                            return -3; //Неудачное соединение с устройством
                        }

                        if (CurrentBluetoothSocket.InputStream == null || CurrentBluetoothSocket.OutputStream == null)
                        {
                            CloseBluetoothConnection();
                            StateBluetoothConnect = false;
                            return -4; //Не проинициализированы потоки ввода - вывода
                        }
                        StateBluetoothConnect = true;
                        return 0;
                    }
                    else
                    {
                        //Если же сокет существует, делаем реконнект
                        if (!StateBluetoothConnect)
                        {
                            //Если пропало соединение - восстанавливаем его
                            try
                            {
                                //await CurrentBluetoothSocket.ConnectAsync();
                                CurrentBluetoothSocket.Connect();
                                await Task.Delay(2);
                            }
                            catch (Exception exception)
                            {
                                StateBluetoothConnect = false;
                                string exceptionMessage = exception.Message;
                            }
                            if (!CurrentBluetoothSocket.IsConnected)
                            {
                                CloseBluetoothConnection();
                                return -3; //Неудачное соединение с устройством
                            }

                            if (CurrentBluetoothSocket.InputStream == null || CurrentBluetoothSocket.OutputStream == null)
                            {
                                CloseBluetoothConnection();
                                StateBluetoothConnect = false;
                                return -4; //Не проинициализированы потоки ввода - вывода
                            }
                            StateBluetoothConnect = true;
                            return 0;
                        }
                        else
                        {
                            return 1; //Переподключение не требуется
                        }
                    }
                }
                else
                {
                    return -1; // Устройство не забиндено к смартфону
                }
            }
            catch (Exception ex)
            {
                CloseBluetoothConnection();
                StateBluetoothConnect = false;
                string ExceptionMessage = ex.Message;
                string ExceptionStackTrace = ex.StackTrace;
                return -10; //Неизвестная ошибка
            }
        }

        private BluetoothDevice GetBindedDivece(string MAC)
        {
            List<BluetoothDevice> bindedDevices = new List<BluetoothDevice>(BluetoothAdapter.DefaultAdapter.BondedDevices);
            BluetoothDevice currentDevice = bindedDevices.Find(x => x.Address == MAC);
            return currentDevice;
        }

        public async Task<int> SendCommand(byte[] command)
        {
            if (CurrentBluetoothSocket != null && CurrentBluetoothSocket.IsConnected
                && CurrentBluetoothSocket.InputStream != null && CurrentBluetoothSocket.OutputStream != null)
            {
                await Task.Delay(10);
                try
                {
                    CurrentBluetoothSocket.OutputStream.Flush();
                    //OutputStream предназначен для отправки сообщений
                    CurrentBluetoothSocket.OutputStream.Write(command, 0, command.Length);
                    return 0;
                }
                catch (Exception ex)
                {
                    string ExceptionMessage = ex.Message;
                    CloseBluetoothConnection();
                    return -2; //Ошибка отправки, возможно произошло разъединение
                }
            }
            else
            {
                CloseBluetoothConnection();
                return -3; //Соединение разорвано
            }
        }

        public async Task<int> SendCheckableCommand(byte[] command)
        {
            if (CurrentBluetoothSocket != null && CurrentBluetoothSocket.IsConnected
                && CurrentBluetoothSocket.InputStream != null && CurrentBluetoothSocket.OutputStream != null)
            {
                await Task.Delay(10);
                try
                {
                    CurrentBluetoothSocket.InputStream.Flush();
                    CurrentBluetoothSocket.OutputStream.Flush();

                    //OutputStream предназначен для отправки сообщений
                    CurrentBluetoothSocket.OutputStream.Write(command, 0, command.Length);
                    //await CurrentBluetoothSocket.OutputStream.WriteAsync(command, 0, command.Length);
                    byte[] bluetoothResponse = new byte[ResponseSize];

                    //Запустим таймер, использую CancelationToken
                    TimeSpan timeout = TimeSpan.FromSeconds(5);
                    int receivedCount = -1;
                    using var cts = new CancellationTokenSource(timeout); //C# 8 syntax
                    using (cts.Token.Register(() => CurrentBluetoothSocket.InputStream.Close()))
                    {
                        try
                        {
                            receivedCount = await CurrentBluetoothSocket.InputStream.ReadAsync(bluetoothResponse, 0, bluetoothResponse.Length, cts.Token).ConfigureAwait(false);
                        }
                        catch (TimeoutException)
                        {
                            receivedCount = -1;
                        }
                    }
                    //int countByteResponse = await CurrentBluetoothSocket.InputStream.ReadAsync(bluetoothResponse, 0, bluetoothResponse.Length);

                    if (receivedCount > 0 && GetResponseCorrect(bluetoothResponse, receivedCount) > 0)
                        return 0; //Ответ устройства корректен

                    //Ответ устройства не корректен
                    CloseBluetoothConnection();
                    return -1; 
                }
                catch (Exception ex)
                {
                    string ExceptionMessage = ex.Message;
                    CloseBluetoothConnection();
                    return -2; //Ошибка отправки, возможно произошло разъединение
                }
            }
            else
            {
                CloseBluetoothConnection();
                return -3; //Соединение разорвано
            }
        }

        private int GetResponseCorrect(byte[] responseArray, int realSize)
        {
            if (responseArray.Length >= RightResponse.Length && realSize >= RightResponse.Length)
            {
                for (int i = 0; i <= realSize - 4; i++)
                {
                    if (responseArray[i] == RightResponse[0] && responseArray[i + 1] == RightResponse[1]
                        && responseArray[i + 2] == RightResponse[2] && responseArray[i + 3] == RightResponse[3])
                    {
                        //Ответ OK получен
                        return 1;
                    }
                }
                //Ответ не корректен
                return -2;
            }
            else
            {
                //Несовпадение размеров эталона и ответа устройства 
                return -1;
            }
        }

        //В случае получения null мы проверяем состояние коннекта
        public async Task<byte[]> ReadData()
        {
            if (CurrentBluetoothSocket != null
                && CurrentBluetoothSocket.IsConnected
                && CurrentBluetoothSocket.InputStream != null
                && CurrentBluetoothSocket.OutputStream != null)
            {
                try
                {
                    byte[] bluetoothResponse = new byte[ResponseArraySize];

                    int receivedCount = await CurrentBluetoothSocket.InputStream.ReadAsync(bluetoothResponse, 0, bluetoothResponse.Length);

                    if (receivedCount > 0)
                    {
                        byte[] smallBluetoothResponse = new byte[receivedCount];
                        Array.Copy(bluetoothResponse, 0, smallBluetoothResponse, 0, receivedCount);
                        //Массив с данными
                        bluetoothResponse = null;
                        return smallBluetoothResponse;
                    }
                    //Наличие соединения и пришедший ответ говорит о исправности соединения.
                    // Что-то не так с устройством.
                    bluetoothResponse = null;
                    return null;
                }
                catch (Exception ex)
                {
                    //Проблемы с соединением. Поэтому разрываем его или пересоздаём
                    string ExceptionMessage = ex.Message;
                    CloseBluetoothConnection();
                    return null;
                }
            }
            else
            {
                //Скорее всего что-то с соединением. Поэтому разрываем его или пересоздаём
                return null;
            }
        }

        private void CloseBluetoothConnection()
        {
            if (this.CurrentBluetoothSocket != null)
            {
                this.CurrentBluetoothSocket.Dispose();
                this.CurrentBluetoothSocket = null;
            }
            this.StateBluetoothConnect = false;
        }
    }
}