﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Timers;
using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using HealingPebblesMobile.Droid;

namespace HealingPebblesMobile.Droid
{
    //Запускаем splashscreen как главное окно приложения 
    [Activity(Theme = "@style/Theme.Splash", MainLauncher = true, NoHistory = true)]
    public class SplashScreen : Activity
    {
        //Блокируем Freez экран и выводим логотип
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
            Thread.Sleep(2000); // Simulate a long loading process on app startup.
            StartActivity(typeof(MainActivity)); // Тут указать нашу главную Activity
        }
    }
}